(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-bloco_notas-bloco_notas-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bloco_notas/bloco_notas.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bloco_notas/bloco_notas.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n  <ion-buttons slot=\"start\" size=\"x-small\">\r\n    <ion-back-button color=\"light\" defaultHref=\"contato\" text=\"\"></ion-back-button>\r\n  </ion-buttons>\r\n</ion-toolbar>\r\n\r\n<ion-content>\r\n  <div class=\"Container\">\r\n      <div class=\"Header\">\r\n       \r\n      </div>\r\n      <div class=\"ProfileButton\"></div>\r\n    \r\n      <div class=\"Text\"><b>Anotações</b></div><br>\r\n         <ion-item style=\"padding-right: 10px; padding-left: 10px;\">\r\n          <ion-label position=\"floating\">Escreva aqui suas anotações</ion-label>\r\n          <ion-textarea [(ngModel)]=\"anotacao\" style=\"width: 100%\" no-padding></ion-textarea>\r\n        </ion-item>\r\n      \r\n      <button class=\"Button\" (click)=\"anotacoes()\"><p class=\"ButtonText\">Salvar</p></button>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/bloco_notas/bloco_notas-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/bloco_notas/bloco_notas-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: BlocoNotasPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlocoNotasPageRoutingModule", function() { return BlocoNotasPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _bloco_notas_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./bloco_notas.page */ "./src/app/pages/bloco_notas/bloco_notas.page.ts");




const routes = [
    {
        path: '',
        component: _bloco_notas_page__WEBPACK_IMPORTED_MODULE_3__["BlocoNotasPage"],
    }
];
let BlocoNotasPageRoutingModule = class BlocoNotasPageRoutingModule {
};
BlocoNotasPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], BlocoNotasPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/bloco_notas/bloco_notas.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/bloco_notas/bloco_notas.module.ts ***!
  \*********************************************************/
/*! exports provided: BlocoNotasPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlocoNotasPageModule", function() { return BlocoNotasPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _bloco_notas_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bloco_notas.page */ "./src/app/pages/bloco_notas/bloco_notas.page.ts");
/* harmony import */ var _bloco_notas_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./bloco_notas-routing.module */ "./src/app/pages/bloco_notas/bloco_notas-routing.module.ts");







let BlocoNotasPageModule = class BlocoNotasPageModule {
};
BlocoNotasPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _bloco_notas_routing_module__WEBPACK_IMPORTED_MODULE_6__["BlocoNotasPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_bloco_notas_page__WEBPACK_IMPORTED_MODULE_5__["BlocoNotasPage"]]
    })
], BlocoNotasPageModule);



/***/ }),

/***/ "./src/app/pages/bloco_notas/bloco_notas.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/bloco_notas/bloco_notas.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #fff;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n}\n:host .Button {\n  width: 70%;\n  height: 50px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 15px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 13px;\n}\n:host .Input {\n  width: 90%;\n  height: 45px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 6px;\n  margin-bottom: 8px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .Container {\n  flex: 1;\n  background-color: #fff;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .HeaderTitle {\n  color: #f4ede9;\n  font-size: 20px;\n  line-height: 28px;\n}\n:host .UserName {\n  font-family: \"Lato-Bold\";\n  font-size: 22px;\n  color: #f4ede9;\n  margin-bottom: -15px;\n}\n:host .UserAvatar {\n  width: 56px;\n  height: 56px;\n  border-color: #fff;\n  border-radius: 28px;\n}\n:host .Text {\n  font-family: \"Lato-Bold\";\n  font-size: 24px;\n  color: #126DE8;\n  text-align: center;\n  margin-top: 20px;\n}\n:host .List {\n  padding-top: 50;\n  padding-left: 20;\n  padding-right: 20;\n}\n:host .ListContainer {\n  background: #126DE8;\n  border-radius: 15px;\n  padding: 15px;\n  margin-bottom: 10px;\n  margin-left: 6.5px;\n  margin-right: 6.5px;\n  flex-direction: row;\n  align-items: center;\n}\n:host .ListAvatar {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  border-radius: 36px;\n}\n:host .ListInfo {\n  flex: 1;\n  margin-left: 60px;\n}\n:host .ListName {\n  font-family: \"Lato-Bold\";\n  font-size: 18px;\n  color: #fff;\n}\n:host .ListMeta {\n  flex-direction: row;\n  align-items: center;\n  margin-top: 10px;\n  margin-left: 20px;\n}\n:host .ListMetaText {\n  margin-left: 8px;\n  color: #999591;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYmxvY29fbm90YXMvYmxvY29fbm90YXMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0ksa0JBQUE7QUFEUjtBQUlJO0VBQ0kseUJBQUE7RUFDQSw2QkFBQTtBQUZSO0FBS0k7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUhSO0FBT0k7RUFDSSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBTFI7QUFRSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQU5SO0FBU0k7RUFDSSxPQUFBO0VBQ0Esc0JBQUE7QUFQUjtBQVVJO0VBR0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFWUjtBQWFJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQVhSO0FBZUk7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUFiUjtBQWtCSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQWhCUjtBQW1CSTtFQUNJLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBakJSO0FBb0JJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFsQlI7QUFxQkk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFuQlI7QUFzQkk7RUFDSSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7RUFDQSwyQkFBQTtFQUFBLHdCQUFBO0VBQUEsbUJBQUE7RUFDQSxtQkFBQTtBQXBCUjtBQXVCSTtFQUNJLE9BQUE7RUFDQSxpQkFBQTtBQXJCUjtBQXdCSTtFQUNJLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUF0QlI7QUF5Qkk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQXZCUjtBQTBCSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtBQXhCUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Jsb2NvX25vdGFzL2Jsb2NvX25vdGFzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuXHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi10b29sYmFyIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgIC0taW9uLWNvbG9yLWJhc2U6IHRyYW5zcGFyZW50O1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuQnV0dG9uIHtcclxuICAgICAgICB3aWR0aDogNzAlO1xyXG4gICAgICAgIGhlaWdodDogNTBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2RDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIH1cclxuICBcclxuICAgIFxyXG4gICAgLkJ1dHRvblRleHQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcclxuICAgIH1cclxuICBcclxuICAgIC5JbnB1dCB7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICBoZWlnaHQ6IDQ1cHg7XHJcbiAgICAgICAgcGFkZGluZzogMCAxNnB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNnB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDhweDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuQ29udGFpbmVyIHtcclxuICAgICAgICBmbGV4OiAxO1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XHJcbiAgICB9XHJcblxyXG4gICAgLkhlYWRlciB7XHJcbiAgICAgICAgLy9wYWRkaW5nOiAzMHB4O1xyXG4gICAgICAgIC8vcGFkZGluZy10b3A6IDMwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkRFODtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5IZWFkZXJUaXRsZSB7XHJcbiAgICAgICAgY29sb3I6ICNmNGVkZTk7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyOHB4O1xyXG4gICAgICAgLy8gbWFyZ2luLXRvcDogMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAuVXNlck5hbWUge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgICAgY29sb3I6ICNmNGVkZTk7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTE1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLy8uUHJvZmlsZUJ1dHRvbiB7fVxyXG5cclxuICAgIC5Vc2VyQXZhdGFyIHtcclxuICAgICAgICB3aWR0aDogNTZweDtcclxuICAgICAgICBoZWlnaHQ6IDU2cHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI4cHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5UZXh0IHtcclxuICAgICAgICBmb250LWZhbWlseTogJ0xhdG8tQm9sZCcgO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICBjb2xvcjogIzEyNkRFODtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdCB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6NTA7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OjIwO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6MjA7XHJcbiAgICB9XHJcblxyXG4gICAgLkxpc3RDb250YWluZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDYuNXB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogNi41cHg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuTGlzdEF2YXRhciB7XHJcbiAgICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xyXG4gICAgICAgIGhlaWdodDogZml0LWNvbnRlbnQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMzZweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdEluZm8ge1xyXG4gICAgICAgIGZsZXg6MTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdE5hbWUge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICB9XHJcblxyXG4gICAgLkxpc3RNZXRhIHtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdE1ldGFUZXh0IHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gICAgICAgIGNvbG9yOiAjOTk5NTkxO1xyXG4gICAgfVxyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/pages/bloco_notas/bloco_notas.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/bloco_notas/bloco_notas.page.ts ***!
  \*******************************************************/
/*! exports provided: BlocoNotasPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlocoNotasPage", function() { return BlocoNotasPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _bloco_notasService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./bloco_notasService */ "./src/app/pages/bloco_notas/bloco_notasService.ts");






let BlocoNotasPage = class BlocoNotasPage {
    constructor(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, router, userService, toastCtrl, plt, route, anotacaoService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.userService = userService;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.route = route;
        this.anotacaoService = anotacaoService;
        this.user$ = userService.getUser();
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.sus = parametros['sus'];
            console.log('parametro dentro de info : ' + parametros['sus']);
        });
    }
    anotacoes() {
        this.paciente_sus = this.sus;
        console.log(this.anotacao);
        console.log('anotaçoes' + this.sus);
        this.anotacaoService.signup(this.anotacao, this.sus).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(this.sus);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Paciente cadastrado com sucesso ' + this.sus + '!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            var sus = this.sus;
            this.router.navigate(['/contato/' + sus + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['/home']);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Houve um erro, por favor contate o adm do app',
                duration: 8000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
BlocoNotasPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _bloco_notasService__WEBPACK_IMPORTED_MODULE_5__["BlocoNotasService"] }
];
BlocoNotasPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-bloco-notas',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./bloco_notas.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bloco_notas/bloco_notas.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./bloco_notas.page.scss */ "./src/app/pages/bloco_notas/bloco_notas.page.scss")).default]
    })
], BlocoNotasPage);



/***/ }),

/***/ "./src/app/pages/bloco_notas/bloco_notasService.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/bloco_notas/bloco_notasService.ts ***!
  \*********************************************************/
/*! exports provided: BlocoNotasService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BlocoNotasService", function() { return BlocoNotasService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let BlocoNotasService = class BlocoNotasService {
    constructor(http) {
        this.http = http;
    }
    signup(anotacao, paciente_sus) {
        return this.http.post(API_URL + '/anotacao/signup', { anotacao, paciente_sus });
    }
};
BlocoNotasService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
BlocoNotasService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], BlocoNotasService);



/***/ })

}]);
//# sourceMappingURL=pages-bloco_notas-bloco_notas-module-es2015.js.map