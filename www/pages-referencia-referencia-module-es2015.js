(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-referencia-referencia-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/referencia/referencia.page.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/referencia/referencia.page.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n  <ion-buttons slot=\"start\" size=\"x-small\">\r\n    <ion-back-button color=\"light\" defaultHref=\"agenda\" text=\"\"></ion-back-button>\r\n  </ion-buttons>\r\n</ion-toolbar>\r\n<ion-content>\r\n  <div class=\"Container\">\r\n      <div class=\"Title\">Referências Bibliográficas </div>\r\n        <div style=\"text-align: inherit;\">\r\n         <p> Arai H, Wakabayashi H, Yoshimura Y, Yamada M, Hunkyung Kim H, Harada A. Treatment of Sarcopenia. Geriatr Gerontol Int 2018; 18 (Suppl. 1): 28–44. doi: 10.1111/ggi.13322.\r\n        </p><br/>\r\n        <p>  Dodds R, Sayer AA. Sarcopenia. Arq Bras Endocrinol Metab. 2014, vol.58, n.5, pp.464-469. ISSN 1677-9487, doi 10.1590/0004-273000000.\r\n          </p><br/>\r\n          <p>  Jesus ITM, Orlandi AAS, Grazziano ES, Zazzetta MS. Fragilidade de Idosos em Vulnerabilidade Social. Acta Paul Enferm.2017; 30(6):614-20, doi 10.1590/1982- 0194201700088.\r\n          </p><br/>\r\n          <p> Peral JAR, Josa MSG. Ejercicios de Resistencia en el Tratamiento y Prevención de la Sarcopenia en Ancianos. Revisión sistemática. Gerokomos. 2018; 29(3):133-137.\r\n          </p> <br/>\r\n          <p> Cardoso RM, Neto JRC, Freitas LPR, Ferreira MPP. Exercício Resistido frente à Sarcopenia: Uma Alternativa Eficaz para a Qualidade de Vida do Idoso. Universidade Presidente Antônio Carlos, UNIPAC. EFDeportes.com, Revista Digital. Buenos Aires - Año 17 - Nº 169 - Junio de 2012.\r\n          </p><br/>          \r\n          <p> Chan DC, Chang CB,  Han DS, Hong CH, Hwang JS, Tsai KS, Yang RS. Effects of Exercise Improves Muscle Strength and fat Mass in Patients With High Fracture Risk: A randomized Control Trial. Journal of the Formosan Medical Association (2018) 117, 572e582.\r\n          </p><br/>\r\n          <p> Chica A, González-guirval F, Reigal RE, Carranque G, Hernández-mendo A. Efectos de un programa de danza española en mujeres con fibromialgia. Cuadernos de Psicología del Deporte, Vol 19(2) 2019, 52-69.\r\n          </p><br/>\r\n          <p> Paula JÁ, Wamser EL, Gomes ARS, Valderramas SR, Cardoso Neto J, Schieferdecke RMEM. Análise de Métodos para Detectar Sarcopenia em Idosas Independentes da Comunidade. Rev. Bras. Geriatr. Gerontol., Rio de Janeiro, 2016, doi10.1590/1809-98232016019.140233.\r\n          </p><br/>\r\n          <p>Conwright CMD, Courneya KS, Wahnefried WD, Sami N, Lee K, Buchanan TA, Spice DV. Tripathy.D.,Bernstein.L.,Mortimer.J.E. Effects of Aerobic and Resistance Exercise on Metabolic Syndrome, Sarcopenic Obesity, and Circulating Biomarkers in Overweight or Obese Survivors of Breast Cancer: A Randomized Controlled Trial. Journal of clinical oncology, volume 36, number 9, march 20, 2018,doi 10.1200/JCO.2017. 75.7526.\r\n          </p><br/>\r\n          <p>Cusumano AM. Sarcopenia en Pacientes con y sin Insuficiencia Renal Crónica: Diagnóstico, Evaluación y Tratamiento. Instituto de Nefrología Pergamino. Nefrología, Diálisis y Trasplante 2015; 35 (1) Pág. 32 a 43.\r\n          </p><br/>\r\n          <p>Damanti S, Azzolino D, Roncaglione C, Arosio B, Rossi P, Cesari M. Efficacy of Nutritional Interventions as Stand-Alone or Synergistic Treatments with Exercise for the Management of Sarcopenia. Nutrients 2019, 11, 1991; doi: 10.3390/nu11091991.\r\n          </p><br/>\r\n          <p>Distefano G, Goodpaster BH. Effects of Exercise and Aging on Skeletal Muscle.  Cold Spring Harb Perspect Med 2018; doi: 10.1101/cshperspect.a029785.\r\n          </p><br/>\r\n          <p>Teixeira VON, Filippin LI, Xavier RM. Mecanismos de Perda Muscular da Sarcopenia. Rev Bras Reumatol 2012;52(2):247-259.\r\n          </p><br/>\r\n          <p>Raman M, Loza AJM, Eslamparast T, Tandon P. Sarcopenic Obesity in Cirrhosis; The Confluence of 2 Prognostic Titans. Liver International. 2018;38:1706–1717. DOI: 10.1111/liv.13876.\r\n          </p><br/>\r\n          <p>Yamada M, Kimura Y, Ishiyama D, Nishio N, Otobe Y, Tanaka T, Ohji S,  Koyama S, Sato A, Suzuki M, Ogawa H, Ichikawa T, Arai H. Synergistic Effect of Bodyweight Resistance Exercise and Protein Supplementation on Skeletal Muscle in Sarcopenic or Dynapenic Older Adults. Epidemiology, clinical practice and health 2019.\r\n          </p><br/>\r\n          <p>Ruiz MER, Lans VG, Torres IP, Soto ME. Mechanisms Underlying Metabolic Syndrome-Related Sarcopenia and Possible Therapeutic Measures. Int. J. Mol. Sci. 2019, 20, 647; doi:10.3390/ijms20030647.\r\n          </p><br/>\r\n          <p>Torre AM. El Músculo, Elemento Clave para la Supervivencia en el Enfermo Neoplásico Muscle Wasting as a key Predictor of Survival in Cancer Patients. Nutr Hosp 2016;33(Supl. 1):11-16.\r\n          </p><br/>\r\n          \r\n          <p>Montoro MVP, Montilla JAP, Aguilera ELA, Checa MA. Intervención en la Sarcopenia con Entrenamiento de Resistencia Progresiva y Suplementos Nutricionales Protéicos. Nutr Hosp. 2015;31(4):1481-1490 ISSN 0212-1611, coden nuhoeq S.V.R. 31.\r\n          </p><br/>\r\n          <p>Trouwborst I, Verreijen A, Memelink R, Massanet P, Boirie Y, Weijs P, Tieland M. Exercise and Nutrition Strategies to Counteract Sarcopenic Obesity. Nutrients 2018, 10, 605; doi:10.3390/nu10050605\r\n          </p><br/>\r\n          <p>Izquierdo M. Prescripción de ejercicio físico. El Programa Vivifrail como modelo Multicomponent Physical Exercise Program: Vivifrail. Nutr Hosp 2019;36(N.º Extra 2):50-56 DOI: 10.20960/nh.02680.\r\n          </p><br/>\r\n          \r\n          <p>Kellyo J. Gilman JC, Boschiero D, Ilich, JZ.  Osteosarcopenic Obesity: Current Knowledge, Revised Identification Criteria and Treatment Principles. Nutrients 2019, 11, 747; doi: 10.3390/nu11040747\r\n          </p><br/>\r\n          <p>Beaudart C, Zaaria M, Pasleau F, Reginster JY, Bruyère O. Health Outcomes of Sarcopenia: A Systematic Review and Meta-Analysis. PLOS ONE journal.pone.0169548 January 17, 2017 DOI:10.1371.\r\n          </p><br/>\r\n          <p>Becerro JFM. El entrenamiento de fuerza en los deportistas mayores. Arch Med Deporte 2016; 33(5): 332-337.\r\n          </p><br/>\r\n          <p>Câmara LC, bastos CC, Volpe EFT. Exercício resistido em idosos frágeis: uma revisão da literatura. Fisioter Mov. 2012 abr/jun; 25(2):435-43.\r\n          </p><br/>\r\n          <p>Beaudart C, Zaaria M, Pasleau F, Reginster JY, Bruyère O. Health Outcomes of Sarcopenia: A Systematic Review and Meta-Analysis. PLOS ONE journal.pone.0169548 January 17, 2017 DOI:10.1371.\r\n          </p><br/>\r\n          <p>Duarte RA, Margain AR, Loza AJM, Rodrıguez RUM, Ferrando A., Kim WR. Exercise and Physical Activity for Patients With End-Stage Liver Disease: Improving Functional Status and Sarcopenia While on the Transplant Waiting List. Liver transplantation, Vol. 24, No. 1, 2018. DOI 10.1002/lt.24958.\r\n          </p><br/>\r\n          <p>Kelly OJ, Gilman JC, Boschiero D, Ilich JZ. Osteosarcopenic Obesity: Current Knowledge, Revised Identification Criteria and Treatment Principles. Nutrients 2019, 11, 747; doi:10.3390/nu11040747.\r\n          </p><br/>\r\n          <p>Becerro JFM. A Equipe Multiprofissional em Gerontologia e a Produção do Cuidado: um estudo de caso. KAIRÓS. 2014;17(2):205-222.\r\n          </p><br/>\r\n        \r\n        \r\n        </div>\r\n   \r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/referencia/referencia-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/referencia/referencia-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: ReferenciaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferenciaPageRoutingModule", function() { return ReferenciaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _referencia_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./referencia.page */ "./src/app/pages/referencia/referencia.page.ts");




const routes = [
    {
        path: '',
        component: _referencia_page__WEBPACK_IMPORTED_MODULE_3__["ReferenciaPage"],
    }
];
let ReferenciaPageRoutingModule = class ReferenciaPageRoutingModule {
};
ReferenciaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], ReferenciaPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/referencia/referencia.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/referencia/referencia.module.ts ***!
  \*******************************************************/
/*! exports provided: ReferenciaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferenciaPageModule", function() { return ReferenciaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _referencia_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./referencia.page */ "./src/app/pages/referencia/referencia.page.ts");
/* harmony import */ var _referencia_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./referencia-routing.module */ "./src/app/pages/referencia/referencia-routing.module.ts");







let ReferenciaPageModule = class ReferenciaPageModule {
};
ReferenciaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _referencia_routing_module__WEBPACK_IMPORTED_MODULE_6__["ReferenciaPageRoutingModule"]
        ],
        declarations: [_referencia_page__WEBPACK_IMPORTED_MODULE_5__["ReferenciaPage"]]
    })
], ReferenciaPageModule);



/***/ }),

/***/ "./src/app/pages/referencia/referencia.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/referencia/referencia.page.scss ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: white;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Container {\n  flex: 1;\n  background: #fff;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px;\n}\n:host .Title {\n  font-size: 24px;\n  text-align: center;\n  color: #000;\n  font-family: \"Lato-Regular\";\n  margin: 20px 0 30px;\n}\n:host .Conteudo {\n  font-size: 15px;\n  text-align: center;\n  color: #000;\n  font-family: \"Lato-Regular\";\n  margin: 20px 0 30px;\n}\n:host .BackToSignIn {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 calc(10 + getBottomSpace()px);\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVmZXJlbmNpYS9yZWZlcmVuY2lhLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUNJLG1CQUFBO0FBRFI7QUFJSTtFQUNJLHlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FBRlI7QUFLSTtFQUdJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FBTFI7QUFRSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFOUjtBQVVJO0VBQ0ksd0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQVJSO0FBV0k7RUFDSSxPQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtBQVRSO0FBYUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQVhSO0FBY0k7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsMkJBQUE7RUFDQSxtQkFBQTtBQVpSO0FBZ0JJO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSw2Q0FBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQWRSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVmZXJlbmNpYS9yZWZlcmVuY2lhLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuXHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB3aGl0ZTtcclxuICAgIH1cclxuXHJcbiAgICBpb24tdG9vbGJhciB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAtLWlvbi1jb2xvci1iYXNlOiB0cmFuc3BhcmVudDsgXHJcbiAgICAgICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAgICAgLS1ib3JkZXI6IDAgbm9uZTsgXHJcbiAgICAgICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgICAgICAtLW91dGxpbmU6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLkhlYWRlciB7XHJcbiAgICAgICAgLy9wYWRkaW5nOiAzMHB4O1xyXG4gICAgICAgIC8vcGFkZGluZy10b3A6IDMwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkRFODtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5CdXR0b24ge1xyXG4gICAgICAgIHdpZHRoOiA2MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA2MHB4OyAvLzUwXHJcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7IC8vMTVcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgXHJcbiAgICAuQnV0dG9uVGV4dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNnB4OyAvLzEzXHJcbiAgICB9XHJcblxyXG4gICAgLkNvbnRhaW5lcntcclxuICAgICAgICBmbGV4OiAxO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAwIDMwcHg7XHJcbiAgICAgICAgLy9tYXJnaW4tdG9wOiAxNTBweDtcclxuICAgIH1cclxuXHJcbiAgICAuVGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6ICMwMDA7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tUmVndWxhcic7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4IDAgMzBweDtcclxuICAgIH1cclxuXHJcbiAgICAuQ29udGV1ZG8ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6ICMwMDA7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tUmVndWxhcic7XHJcbiAgICAgICAgbWFyZ2luOiAyMHB4IDAgMzBweDtcclxuICAgIH1cclxuXHJcblxyXG4gICAgLkJhY2tUb1NpZ25JbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHJpZ2h0OjA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItdG9wLXdpZHRoOiAxcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjMjMyMTI5O1xyXG4gICAgICAgIHBhZGRpbmc6IDE2cHggMCBjYWxjKDEwICsgZ2V0Qm90dG9tU3BhY2UoKXB4KTtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICB9XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/referencia/referencia.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/referencia/referencia.page.ts ***!
  \*****************************************************/
/*! exports provided: ReferenciaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ReferenciaPage", function() { return ReferenciaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let ReferenciaPage = class ReferenciaPage {
    constructor(router, route) {
        this.router = router;
        this.route = route;
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.sus = parametros['sus'];
            console.log('parametro dentro de info : ' + parametros['sus']);
        });
    }
    teste() {
        console.log('enviando para teste');
        this.router.navigate(['/teste/' + this.sus + '']);
    }
};
ReferenciaPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] }
];
ReferenciaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-referencia',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./referencia.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/referencia/referencia.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./referencia.page.scss */ "./src/app/pages/referencia/referencia.page.scss")).default]
    })
], ReferenciaPage);



/***/ })

}]);
//# sourceMappingURL=pages-referencia-referencia-module-es2015.js.map