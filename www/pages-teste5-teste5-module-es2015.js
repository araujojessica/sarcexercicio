(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-teste5-teste5-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste5/teste5.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste5/teste5.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div>\n    <div class=\"Title\">Teste de Caminhada <br> de 6 minutos</div>\n    <div class=\"Conteudo\">Em um solo plano com extensão entre 20 e 30 metros, solicita-se que o indivíduo \n                          caminhe durante seis minutos, em ritmo próprio, com o objetivo de cobrir a maior \n                          distância possível, dentro do tempo proposto. Antes e imediatamente depois da conclusão\n                          do teste, são aferidas a freqüência cardíaca, a pressão arterial, a oximetria e a \n                          intensidade do cansaço, esta última por meio de um questionário padronizado – a escala de \n                          Bor Distancia Percorrida: 401,185 –(2,402 x Idade anos) + (43,247 x Gênero  \n                          (masc. = 1; fem. = 0) + 1,757 x estatura cm). Escore médio:410,5 metros para os homens e de\n                           371,0 metros para as mulheres.</div>\n      \n      <form [formGroup]=\"caminhadaForm\"> \n      <ion-item class=\"Input\" style=\"padding: 0px;\">\n        <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n          <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n        </svg>&nbsp;\n        <input autocomplete=\"off\" name=\"name\" type=\"text\" formControlName=\"caminhada_resultado\" placeholder=\"Resposta\" class=\"InputStyle\"/>\n      </ion-item>\n        </form>\n      <button class=\"Button\" (click)=\"agenda()\"><p class=\"ButtonText\">ENVIAR</p></button>\n    </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/teste5/caminhadaService.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/teste5/caminhadaService.ts ***!
  \**************************************************/
/*! exports provided: CaminhadaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CaminhadaService", function() { return CaminhadaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let CaminhadaService = class CaminhadaService {
    constructor(http) {
        this.http = http;
    }
    signup(newCaminhada) {
        console.log(newCaminhada);
        return this.http.post(API_URL + '/caminhada/signup', newCaminhada);
    }
};
CaminhadaService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
CaminhadaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], CaminhadaService);



/***/ }),

/***/ "./src/app/pages/teste5/teste5-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/teste5/teste5-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: Teste5PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste5PageRoutingModule", function() { return Teste5PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _teste5_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teste5.page */ "./src/app/pages/teste5/teste5.page.ts");




const routes = [
    {
        path: '',
        component: _teste5_page__WEBPACK_IMPORTED_MODULE_3__["Teste5Page"],
    }
];
let Teste5PageRoutingModule = class Teste5PageRoutingModule {
};
Teste5PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Teste5PageRoutingModule);



/***/ }),

/***/ "./src/app/pages/teste5/teste5.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/teste5/teste5.module.ts ***!
  \***********************************************/
/*! exports provided: Teste5PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste5PageModule", function() { return Teste5PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _teste5_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./teste5.page */ "./src/app/pages/teste5/teste5.page.ts");
/* harmony import */ var _teste5_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./teste5-routing.module */ "./src/app/pages/teste5/teste5-routing.module.ts");







let Teste5PageModule = class Teste5PageModule {
};
Teste5PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _teste5_routing_module__WEBPACK_IMPORTED_MODULE_6__["Teste5PageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_teste5_page__WEBPACK_IMPORTED_MODULE_5__["Teste5Page"]]
    })
], Teste5PageModule);



/***/ }),

/***/ "./src/app/pages/teste5/teste5.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/teste5/teste5.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .InputText {\n  flex: 1;\n  color: #126DE8;\n  font-size: 16px;\n  font-family: \"lato-Regular\";\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 40px 70px;\n  margin-top: 70px;\n}\n:host .Title {\n  text-align: center;\n  font-size: 24px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 100px 0 20px;\n}\n:host .Conteudo {\n  text-align: center;\n  font-size: 18px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 1px 0 20px;\n  padding: 0 10px 0px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGVzdGU1L3Rlc3RlNS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxxQkFBQTtBQURSO0FBSUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRlI7QUFNSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFKUjtBQU9JO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBTFI7QUFRSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQU5SO0FBU0k7RUFDSSxPQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtBQVBSO0FBVUk7RUFDSSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUFSUjtBQVdJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0Esb0JBQUE7QUFUUjtBQVlJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQVZSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGVzdGU1L3Rlc3RlNS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XG5cbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogIzEyNkRFODtcbiAgICB9XG4gIFxuICAgIC5CdXR0b24ge1xuICAgICAgICB3aWR0aDogNjAlO1xuICAgICAgICBoZWlnaHQ6IDYwcHg7IC8vNTBcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDhweDsgLy8xNVxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB9XG4gIFxuICAgIFxuICAgIC5CdXR0b25UZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7IC8vMTNcbiAgICB9XG4gIFxuICAgIC5JbnB1dCB7XG4gICAgICAgIHdpZHRoOiA5MCU7IC8vOTBcbiAgICAgICAgaGVpZ2h0OiAzOHB4OyAvLzQ1XG4gICAgICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4OyAvLzZcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgfVxuXG4gICAgLklucHV0U3R5bGUge1xuICAgICAgICBmb250LXNpemU6IGxhcmdlOyBcbiAgICAgICAgYm9yZGVyOiAwIG5vbmU7IFxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMCAwOyBcbiAgICAgICAgb3V0bGluZTogMDtcbiAgICB9XG4gIFxuICAgIC5JbnB1dFRleHQge1xuICAgICAgICBmbGV4OiAxO1xuICAgICAgICBjb2xvcjogIzEyNkRFODtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBmb250LWZhbWlseTogJ2xhdG8tUmVndWxhcic7XG4gICAgfVxuXG4gICAgLkNvbnRhaW5lciB7XG4gICAgICAgIGZsZXg6IDE7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAwIDQwcHggNzBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNzBweDtcbiAgICB9XG5cbiAgICAuVGl0bGUge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xuICAgICAgICBtYXJnaW46IDEwMHB4IDAgMjBweDtcbiAgICB9XG5cbiAgICAuQ29udGV1ZG8ge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xuICAgICAgICBtYXJnaW46IDFweCAwIDIwcHg7XG4gICAgICAgIHBhZGRpbmc6IDAgMTBweCAwcHg7XG4gICAgfVxuXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/teste5/teste5.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/teste5/teste5.page.ts ***!
  \*********************************************/
/*! exports provided: Teste5Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste5Page", function() { return Teste5Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _caminhadaService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./caminhadaService */ "./src/app/pages/teste5/caminhadaService.ts");






let Teste5Page = class Teste5Page {
    constructor(router, route, toastCtrl, caminhadaService, formBuilder) {
        this.router = router;
        this.route = route;
        this.toastCtrl = toastCtrl;
        this.caminhadaService = caminhadaService;
        this.formBuilder = formBuilder;
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.sus = parametros['sus'];
            console.log('parametro dentro de timeup : ' + parametros['sus']);
        });
        this.caminhadaForm = this.formBuilder.group({
            'caminhada_resultado': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
    }
    agenda() {
        const newCaminhada = this.caminhadaForm.getRawValue();
        console.log(newCaminhada);
        newCaminhada.paciente_sus = this.sus;
        this.caminhadaService.signup(newCaminhada).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Caminhada cadastrado com sucesso!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            this.router.navigate(['/contato/' + this.sus + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Houve um erro, por favor avise o administrador do app!',
                duration: 4000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
Teste5Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _caminhadaService__WEBPACK_IMPORTED_MODULE_5__["CaminhadaService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
Teste5Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./teste5.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste5/teste5.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./teste5.page.scss */ "./src/app/pages/teste5/teste5.page.scss")).default]
    })
], Teste5Page);



/***/ })

}]);
//# sourceMappingURL=pages-teste5-teste5-module-es2015.js.map