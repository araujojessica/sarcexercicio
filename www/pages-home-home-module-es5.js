(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html":
    /*!*********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
      \*********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesHomeHomePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\r\n  <div class = \"container\">\r\n    <img src=\"/sarcex/assets/img/logo.png\" style=\"display: block; margin-left: auto; margin-right: auto; margin-top: 15px;\"/>\r\n    <div class=\"Title\">Faça seu login</div>\r\n\r\n    <form [formGroup]=\"onLoginForm\">\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n        <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-envelope\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\r\n            <path fill-rule=\"evenodd\" d=\"M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z\"/>\r\n        </svg>&nbsp;\r\n        <input name=\"email\" formControlName=\"userName\" type=\"e-mail\" placeholder=\"E-mail\" class=\"InputStyle\">\r\n      </ion-item>\r\n\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n        <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-lock\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -7px;\">\r\n          <path fill-rule=\"evenodd\" d=\"M11.5 8h-7a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1zm-7-1a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-7zm0-3a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z\"/>\r\n        </svg>&nbsp;\r\n        <input  name=\"password\" formControlName=\"password\" [type]=\"tipo ? 'text' : 'password'\" placeholder=\"Senha\" class=\"InputStyle\"/>\r\n    </ion-item>\r\n    <div style=\"margin-top: 5px; text-align: right; padding-right: 18px;\" class=\"ForgotPasswordText\" (click)=\"exibirOcultar()\"><span>Exibir senha</span></div>\r\n\r\n  </form>\r\n    \r\n    <button class=\"Button\" (click)=\"agenda()\"><div class=\"ButtonText\" ><p>ENTRAR</p></div></button>\r\n    <div style=\"margin-top: 25px;\" class=\"ForgotPasswordText\" (click)=\"forgotPassword()\"><span>Esqueci minha senha</span></div>\r\n\r\n    <br>\r\n    <div text-center class=\"logo-img\">\r\n      <img width=\"180px\" src=\"/sarcex/assets/img/logo_Mestrado.png\" > \r\n    </div>\r\n  </div>\r\n  <div class=\"CreateAccountButton\" (click)=\"singup()\">\r\n    <div class=\"CreateAccountButtonText\">\r\n      <svg width=\"1.1em\" height=\"1.1em\" viewBox=\"0 0 16 16\" class=\"bi bi-box-arrow-in-right\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-bottom: -3px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z\"/>\r\n        <path fill-rule=\"evenodd\" d=\"M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z\"/>\r\n      </svg>&nbsp;\r\n      Cadastro para profissionais\r\n    </div>\r\n  </div>\r\n</ion-content>\r\n\r\n";
      /***/
    },

    /***/
    "./src/app/core/auth/auth.service.ts":
    /*!*******************************************!*\
      !*** ./src/app/core/auth/auth.service.ts ***!
      \*******************************************/

    /*! exports provided: AuthService */

    /***/
    function srcAppCoreAuthAuthServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "AuthService", function () {
        return AuthService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");
      /* harmony import */


      var _user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! ../user/user.service */
      "./src/app/core/user/user.service.ts"); //const API_URL = 'http://localhost:3000';


      var API_URL = 'http://159.203.181.9:3000';

      var AuthService = /*#__PURE__*/function () {
        function AuthService(http, userService) {
          _classCallCheck(this, AuthService);

          this.http = http;
          this.userService = userService;
        }

        _createClass(AuthService, [{
          key: "authenticate",
          value: function authenticate(userName, password) {
            var _this = this;

            return this.http.post("".concat(API_URL, "/user/login"), {
              userName: userName,
              password: password
            }, {
              observe: 'response'
            }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["tap"])(function (res) {
              var authToken = res.headers.get('x-access-token');

              _this.userService.setToken(authToken);

              console.log("User ".concat(userName, " authenticated with token ").concat(authToken));
            }));
          }
        }]);

        return AuthService;
      }();

      AuthService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }, {
          type: _user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"]
        }];
      };

      AuthService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], AuthService);
      /***/
    },

    /***/
    "./src/app/core/plataform-dector/plataform-dector.service.ts":
    /*!*******************************************************************!*\
      !*** ./src/app/core/plataform-dector/plataform-dector.service.ts ***!
      \*******************************************************************/

    /*! exports provided: PlatformDectorService */

    /***/
    function srcAppCorePlataformDectorPlataformDectorServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "PlatformDectorService", function () {
        return PlatformDectorService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");

      var PlatformDectorService = /*#__PURE__*/function () {
        function PlatformDectorService(platformId) {
          _classCallCheck(this, PlatformDectorService);

          this.platformId = platformId;
        }

        _createClass(PlatformDectorService, [{
          key: "isPlatformBrowser",
          value: function isPlatformBrowser() {
            return Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.platformId);
          }
        }]);

        return PlatformDectorService;
      }();

      PlatformDectorService.ctorParameters = function () {
        return [{
          type: String,
          decorators: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
            args: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"]]
          }]
        }];
      };

      PlatformDectorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], PlatformDectorService);
      /***/
    },

    /***/
    "./src/app/pages/home/home.module.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/home/home.module.ts ***!
      \*******************************************/

    /*! exports provided: HomePageModule */

    /***/
    function srcAppPagesHomeHomeModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePageModule", function () {
        return HomePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./home.page */
      "./src/app/pages/home/home.page.ts");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var routes = [{
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]
      }];

      var HomePageModule = function HomePageModule() {
        _classCallCheck(this, HomePageModule);
      };

      HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterModule"].forChild(routes)],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
      })], HomePageModule);
      /***/
    },

    /***/
    "./src/app/pages/home/home.page.scss":
    /*!*******************************************!*\
      !*** ./src/app/pages/home/home.page.scss ***!
      \*******************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesHomeHomePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n  float: inline-end;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding-left: 5px;\n  padding-bottom: none;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .InputText {\n  flex: 1;\n  color: #126DE8;\n  font-size: 16px;\n  font-family: \"lato-Regular\";\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px 70px;\n  width: 70%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n}\n:host .Title {\n  font-size: 20px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Regular\";\n  margin: 5px 0 24px;\n}\n:host .ForgotPassword {\n  margin-top: 30px;\n}\n:host .ForgotPasswordText {\n  color: #f4ede8;\n  font-size: 16px;\n  font-family: \"Lato-Regular\";\n  text-align: center;\n}\n:host .CreateAccountButton {\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n  background: #fff;\n  padding: 16px 0 25px;\n}\n:host .CreateAccountButtonText {\n  color: #126DE8;\n  font-size: 18px;\n  font-family: \"Lato-Bold\";\n  margin-left: 10px;\n  align-items: center;\n  text-align: center;\n}\n:host .logo-img {\n  display: block;\n  align-self: center;\n  align-items: center;\n  justify-content: center;\n  padding: 0 62px;\n  margin-top: 20%;\n  margin-bottom: 10%;\n  text-align: center;\n  margin: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUNJLHFCQUFBO0FBRFI7QUFJSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQUZSO0FBTUk7RUFDSSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBSlI7QUFPSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBRUEsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQU5SO0FBVUk7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFSUjtBQVdJO0VBQ0ksT0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsMkJBQUE7QUFUUjtBQVlJO0VBQ0ksT0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxvQkFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUFWUjtBQWFJO0VBQ0ksZUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7QUFYUjtBQWNJO0VBQ0ksZ0JBQUE7QUFaUjtBQWVJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtFQUNBLGtCQUFBO0FBYlI7QUFnQkk7RUFDSSxrQkFBQTtFQUNBLFdBQUE7RUFFQSxTQUFBO0VBR0EsZ0JBQUE7RUFHQSxvQkFBQTtBQW5CUjtBQTBCSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUF4QlI7QUE0Qkk7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQTFCUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcblxyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogIzEyNkRFODtcclxuICAgIH1cclxuXHJcbiAgICAuQnV0dG9uIHtcclxuICAgICAgICB3aWR0aDogNjAlO1xyXG4gICAgICAgIGhlaWdodDogNjBweDsgLy81MFxyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZEO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogOHB4OyAvLzE1XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgICAgICBmbG9hdDogaW5saW5lLWVuZDtcclxuICAgIH1cclxuICBcclxuICAgIFxyXG4gICAgLkJ1dHRvblRleHQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDsgLy8xM1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLklucHV0IHtcclxuICAgICAgICB3aWR0aDogOTAlOyAvLzkwXHJcbiAgICAgICAgaGVpZ2h0OiAzOHB4OyAvLzQ1XHJcbiAgICAgICAgLy9wYWRkaW5nLXRvcDogNXB4O1xyXG4gICAgICAgIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gICAgICAgIHBhZGRpbmctYm90dG9tOiBub25lO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4OyAvLzZcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC5JbnB1dFN0eWxlIHtcclxuICAgICAgICBmb250LXNpemU6IGxhcmdlOyBcclxuICAgICAgICBib3JkZXI6IDAgbm9uZTsgXHJcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAgICAgb3V0bGluZTogMDtcclxuICAgIH1cclxuICBcclxuICAgIC5JbnB1dFRleHQge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgY29sb3I6ICMxMjZERTg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnbGF0by1SZWd1bGFyJztcclxuICAgIH1cclxuICBcclxuICAgIC5Db250YWluZXIge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAwIDMwcHggNzBweDtcclxuICAgICAgICB3aWR0aDogNzAlO1xyXG4gICAgICAgIGhlaWdodDogNjBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2RDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDhweDtcclxuICAgIH1cclxuICBcclxuICAgIC5UaXRsZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4OyAvLzE3XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1SZWd1bGFyJztcclxuICAgICAgICBtYXJnaW46IDVweCAwIDI0cHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuRm9yZ290UGFzc3dvcmQge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuRm9yZ290UGFzc3dvcmRUZXh0IHtcclxuICAgICAgICBjb2xvcjogI2Y0ZWRlODtcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7IC8vMTMuNVxyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1SZWd1bGFyJztcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuQ3JlYXRlQWNjb3VudEJ1dHRvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgLy8gbGVmdDogMDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgIC8vICByaWdodDogMDtcclxuICAgICAgLy8gIG1hcmdpbi10b3A6MTAlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAvLyBib3JkZXItdG9wLXdpZHRoOiAxcHg7XHJcbiAgICAgIC8vICBib3JkZXItY29sb3I6ICMyMzIxMjk7XHJcbiAgICAgICAgcGFkZGluZzogMTZweCAwIDI1cHg7XHJcbiAgICAgIC8vICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgLy8gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIC8vICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgIC8vICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuQ3JlYXRlQWNjb3VudEJ1dHRvblRleHQge1xyXG4gICAgICAgIGNvbG9yOiAjMTI2REU4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDsgLy8xNlxyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgXHJcbiAgICAubG9nby1pbWcge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGFsaWduLXNlbGY6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgNjJweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMCU7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTAlO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW46IGF1dG87XHJcbiAgICB9XHJcblxyXG4gICBcclxuICAgXHJcbiAgfSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/home/home.page.ts":
    /*!*****************************************!*\
      !*** ./src/app/pages/home/home.page.ts ***!
      \*****************************************/

    /*! exports provided: HomePage */

    /***/
    function srcAppPagesHomeHomePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "HomePage", function () {
        return HomePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/core/auth/auth.service */
      "./src/app/core/auth/auth.service.ts");
      /* harmony import */


      var src_app_core_plataform_dector_plataform_dector_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! src/app/core/plataform-dector/plataform-dector.service */
      "./src/app/core/plataform-dector/plataform-dector.service.ts");
      /* harmony import */


      var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
      /*! src/app/core/user/user.service */
      "./src/app/core/user/user.service.ts");
      /* harmony import */


      var _recuperar_senha__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
      /*! ./recuperar_senha */
      "./src/app/pages/home/recuperar_senha.ts");

      var HomePage = /*#__PURE__*/function () {
        function HomePage(navCtrl, menuCtrl, toastCtrl, alertCtrl, loadingCtrl, router, formBuilder, authService, userService, recuperarSenha, platformDetectorService) {
          _classCallCheck(this, HomePage);

          this.navCtrl = navCtrl;
          this.menuCtrl = menuCtrl;
          this.toastCtrl = toastCtrl;
          this.alertCtrl = alertCtrl;
          this.loadingCtrl = loadingCtrl;
          this.router = router;
          this.formBuilder = formBuilder;
          this.authService = authService;
          this.userService = userService;
          this.recuperarSenha = recuperarSenha;
          this.platformDetectorService = platformDetectorService;
          this.users = [];
        }

        _createClass(HomePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.onLoginForm = this.formBuilder.group({
              'userName': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'password': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
            });
            this.platformDetectorService.isPlatformBrowser();
          }
        }, {
          key: "forgotPassword",
          value: function forgotPassword() {
            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee4() {
              var _this2 = this;

              var alert;
              return regeneratorRuntime.wrap(function _callee4$(_context4) {
                while (1) {
                  switch (_context4.prev = _context4.next) {
                    case 0:
                      _context4.next = 2;
                      return this.alertCtrl.create({
                        header: 'Esqueceu sua senha?',
                        message: 'Informe seu e-mail para enviarmos um novo link de recuperação.',
                        inputs: [{
                          name: 'email',
                          type: 'email',
                          placeholder: 'E-mail'
                        }],
                        buttons: [{
                          text: 'Cancelar',
                          role: 'cancel',
                          cssClass: 'secondary',
                          handler: function handler() {
                            console.log('Confirmar Cancelamento?');
                          }
                        }, {
                          text: 'Confirmar',
                          handler: function handler(data) {
                            return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee3() {
                              var _this3 = this;

                              return regeneratorRuntime.wrap(function _callee3$(_context3) {
                                while (1) {
                                  switch (_context3.prev = _context3.next) {
                                    case 0:
                                      console.log(data.email);
                                      this.recuperarSenha.sendemail(data.email).subscribe(function () {
                                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                                          var toast;
                                          return regeneratorRuntime.wrap(function _callee$(_context) {
                                            while (1) {
                                              switch (_context.prev = _context.next) {
                                                case 0:
                                                  this.navCtrl.navigateRoot('');
                                                  _context.next = 3;
                                                  return this.toastCtrl.create({
                                                    message: 'Email enviado com sucesso!',
                                                    duration: 4000,
                                                    position: 'top',
                                                    color: 'success'
                                                  });

                                                case 3:
                                                  toast = _context.sent;
                                                  toast.present();

                                                case 5:
                                                case "end":
                                                  return _context.stop();
                                              }
                                            }
                                          }, _callee, this);
                                        }));
                                      }, function (err) {
                                        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this3, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                                          var toast;
                                          return regeneratorRuntime.wrap(function _callee2$(_context2) {
                                            while (1) {
                                              switch (_context2.prev = _context2.next) {
                                                case 0:
                                                  _context2.next = 2;
                                                  return this.toastCtrl.create({
                                                    message: 'Usuário não cadastrado, por favor cadastre-se no APP!',
                                                    duration: 4000,
                                                    position: 'top',
                                                    color: 'danger'
                                                  });

                                                case 2:
                                                  toast = _context2.sent;
                                                  toast.present();

                                                case 4:
                                                case "end":
                                                  return _context2.stop();
                                              }
                                            }
                                          }, _callee2, this);
                                        }));
                                      });

                                    case 2:
                                    case "end":
                                      return _context3.stop();
                                  }
                                }
                              }, _callee3, this);
                            }));
                          }
                        }]
                      });

                    case 2:
                      alert = _context4.sent;
                      _context4.next = 5;
                      return alert.present();

                    case 5:
                    case "end":
                      return _context4.stop();
                  }
                }
              }, _callee4, this);
            }));
          }
        }, {
          key: "singup",
          value: function singup() {
            this.router.navigate(['/singup']);
          }
        }, {
          key: "agenda",
          value: function agenda() {
            var _this4 = this;

            var userName = this.onLoginForm.get('userName').value;
            var password = this.onLoginForm.get('password').value;
            this.authService.authenticate(userName, password).subscribe(function () {
              return _this4.navCtrl.navigateRoot('/agenda');
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this4, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee5() {
                var toast;
                return regeneratorRuntime.wrap(function _callee5$(_context5) {
                  while (1) {
                    switch (_context5.prev = _context5.next) {
                      case 0:
                        this.onLoginForm.reset();
                        this.platformDetectorService.isPlatformBrowser();
                        _context5.next = 4;
                        return this.toastCtrl.create({
                          message: 'Usuário ou senha incorreto, por favor cadastra-se, ou tente novamente!',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 4:
                        toast = _context5.sent;
                        this.router.navigate(['/singup']);
                        toast.present();

                      case 7:
                      case "end":
                        return _context5.stop();
                    }
                  }
                }, _callee5, this);
              }));
            }); //
          }
        }, {
          key: "exibirOcultar",
          value: function exibirOcultar() {
            this.tipo = !this.tipo;
          }
        }]);

        return HomePage;
      }();

      HomePage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: src_app_core_auth_auth_service__WEBPACK_IMPORTED_MODULE_5__["AuthService"]
        }, {
          type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_7__["UserService"]
        }, {
          type: _recuperar_senha__WEBPACK_IMPORTED_MODULE_8__["RecuperarSenhaService"]
        }, {
          type: src_app_core_plataform_dector_plataform_dector_service__WEBPACK_IMPORTED_MODULE_6__["PlatformDectorService"]
        }];
      };

      HomePage.propDecorators = {
        userNameInput: [{
          type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"],
          args: ['userNameInput']
        }]
      };
      HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./home.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./home.page.scss */
        "./src/app/pages/home/home.page.scss"))["default"]]
      })], HomePage);
      /***/
    },

    /***/
    "./src/app/pages/home/recuperar_senha.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/home/recuperar_senha.ts ***!
      \***********************************************/

    /*! exports provided: RecuperarSenhaService */

    /***/
    function srcAppPagesHomeRecuperar_senhaTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "RecuperarSenhaService", function () {
        return RecuperarSenhaService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");

      var API_URL = 'http://159.203.181.9:3000';

      var RecuperarSenhaService = /*#__PURE__*/function () {
        function RecuperarSenhaService(http) {
          _classCallCheck(this, RecuperarSenhaService);

          this.http = http;
        }

        _createClass(RecuperarSenhaService, [{
          key: "sendemail",
          value: function sendemail(email) {
            console.log(email);
            return this.http.post("".concat(API_URL, "/sendEmail"), {
              email: email
            });
          }
        }]);

        return RecuperarSenhaService;
      }();

      RecuperarSenhaService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]
        }];
      };

      RecuperarSenhaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
      })], RecuperarSenhaService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-home-home-module-es5.js.map