(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contato-contato-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contato/contato.page.html":
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contato/contato.page.html ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n  <ion-buttons slot=\"start\" size=\"x-small\">\r\n    <ion-back-button color=\"light\" defaultHref=\"agendamento_cliente\" text=\"\"></ion-back-button>\r\n  </ion-buttons>\r\n</ion-toolbar>\r\n<ion-content>\r\n  <div class= \"container\">\r\n    <div class=\"Title\">Configuração do Paciente</div>\r\n    <p *ngFor=\"let contato of contato\" ></p>\r\n<ion-card>\r\n  <ion-card-content>\r\n    <ion-card-title (click)=\"toggleMMSS();\">\r\n      <ion-item>\r\n        <ion-icon name=\"arrow-down-outline\" slot=\"end\"></ion-icon>\r\n        <ion-text>Exercícios MMSS</ion-text>\r\n      </ion-item>\r\n    </ion-card-title>\r\n  <ion-list id=\"MMSS\" name=\"MMSS\" style=\"display:none;\">\r\n    <ion-radio-group allow-empty-selection (ionChange)=\"exercicio($event)\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <div>\r\n            <ion-radio value = \"Deltoide / Romboide / Manguito Rotador / Trapézio - Desenvolvimento ou Extensor de Ombro / Elevação Lateral\"></ion-radio>&nbsp;\r\n            <ion-label id=\"radio-form\">Deltoide / Romboide / Manguito Rotador / Trapézio - Desenvolvimento ou Extensor de Ombro / Elevação Lateral</ion-label>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-radio-group>\r\n    <ion-radio-group allow-empty-selection (ionChange)=\"exercicio1($event)\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <div>\r\n            <ion-radio value = \"Peitoral - Abução e Abução Horizontal de Ombro / Flexão de Braço Adaptado / Supino\"></ion-radio>&nbsp;\r\n            <ion-label id=\"radio-form\">Peitoral - Abução e Abução Horizontal de Ombro / Flexão de Braço Adaptado / Supino</ion-label>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-radio-group>\r\n    <ion-radio-group allow-empty-selection (ionChange)=\"exercicio2($event)\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <div>\r\n            <ion-radio value = \"Bíceps - Rosca Direta / Rosca Alternada / Rosca Martelo\"></ion-radio>&nbsp;\r\n            <ion-label id=\"radio-form\">Bíceps - Rosca Direta / Rosca Alternada / Rosca Martelo</ion-label>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-radio-group>\r\n    <ion-radio-group allow-empty-selection (ionChange)=\"exercicio3($event)\">\r\n      <ion-row>\r\n        <ion-col>\r\n          <div>\r\n            <ion-radio value = \"Tríceps - Tríceps Testa / Supino fechado / Tríceps Francês / Tríceps Coice\"></ion-radio>&nbsp;\r\n            <ion-label id=\"radio-form\">Tríceps - Tríceps Testa / Supino fechado / Tríceps Francês / Tríceps Coice</ion-label>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-radio-group>\r\n      \r\n    </ion-list>\r\n  </ion-card-content>\r\n  </ion-card>\r\n\r\n  <p></p>\r\n\r\n  <ion-card>\r\n    <ion-card-content>\r\n      <ion-card-title (click)=\"toggleMMII();\">\r\n        <ion-item>\r\n          <ion-icon name=\"arrow-down-outline\" slot=\"end\"></ion-icon>\r\n          <ion-text>Exercícios MMII</ion-text>\r\n        </ion-item>\r\n      </ion-card-title>\r\n\r\n      <ion-list id=\"MMII\" name=\"MMII\" style=\"display:none;\">\r\n\r\n        <ion-radio-group allow-empty-selection (ionChange)=\"exercicioMMII($event)\">\r\n          <ion-row>\r\n            <ion-col>\r\n              <div>\r\n                <ion-radio value = \"Quadriceps Femoral - Agachamento (com apoio, senta e levanta) Afundo / Flexão de Joelhos / Extensão do joelho\"></ion-radio>&nbsp;\r\n                <ion-label id=\"radio-form\">Quadriceps Femoral - Agachamento (com apoio, senta e levanta) Afundo / Flexão de Joelhos / Extensão do joelho</ion-label>\r\n              </div>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-radio-group>\r\n        <ion-radio-group allow-empty-selection (ionChange)=\"exercicioMMII1($event)\">\r\n            <ion-row>\r\n              <ion-col>\r\n                <div>\r\n                  <ion-radio value = \"Glúteo e Posterior de Coxa - Abdução e Adução de Quadril (com variações deitado, em pé e sentado)\"></ion-radio>&nbsp;\r\n                  <ion-label id=\"radio-form\">Glúteo e Posterior de Coxa - Abdução e Adução de Quadril (com variações deitado, em pé e sentado)</ion-label>\r\n                </div>\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-radio-group>\r\n            <ion-radio-group allow-empty-selection (ionChange)=\"exercicioMMII2($event)\">\r\n              <ion-row>\r\n                <ion-col>\r\n                  <div>\r\n                    <ion-radio value = \"Panturilha - Panturrilha em pé / Panturrilha sentado / rotação Tornozelo / Flexão e extensão de pé\"></ion-radio>&nbsp;\r\n                    <ion-label id=\"radio-form\">Panturilha - Panturrilha em pé / Panturrilha sentado / rotação Tornozelo / Flexão e extensão de pé</ion-label>\r\n                  </div>\r\n                </ion-col>\r\n              </ion-row>\r\n            </ion-radio-group>\r\n         \r\n        </ion-list>\r\n    </ion-card-content>\r\n    </ion-card>\r\n\r\n    <p></p>\r\n\r\n    <ion-card>\r\n      <ion-card-content>\r\n        <ion-card-title (click)=\"toggleCore();\">\r\n          <ion-item>\r\n            <ion-icon name=\"arrow-down-outline\" slot=\"end\"></ion-icon>\r\n            <ion-text>Exercícios de Fortalecimento Core, Tronco, Abdômen</ion-text>\r\n          </ion-item>\r\n        </ion-card-title>\r\n      <ion-list id=\"Core\" name=\"Core\" style=\"display:none;\">\r\n\r\n        <ion-radio-group allow-empty-selection (ionChange)=\"exerciciocore($event)\">\r\n          <ion-row>\r\n            <ion-col>\r\n              <div>\r\n                <ion-radio value = \"Abdominal Supra / Infra / Obliquo Controle de Restrição\"></ion-radio>&nbsp;\r\n                <ion-label id=\"radio-form\">Abdominal Supra / Infra / Oblíquo Controle de Respiração</ion-label>\r\n              </div>\r\n            </ion-col>\r\n          </ion-row>\r\n          </ion-radio-group>\r\n        </ion-list>\r\n      </ion-card-content>\r\n      </ion-card>\r\n\r\n    <button class=\"Button\" (click)=\"salvarEX()\"><p class=\"ButtonText\">SALVAR EXERCÍCIOS</p></button>\r\n    <button class=\"Button\" (click)=\"blocoNotas()\"><p class=\"ButtonText\">BLOCO DE NOTAS</p></button>\r\n    <button class=\"Button\" (click)=\"agendamento()\"><p class=\"ButtonText\">AGENDAMENTO</p></button>\r\n    \r\n\r\n    <br><br><br>\r\n   \r\n   <div class=\"CreateAccountButton\" (click)=\"teste()\">\r\n      <div class=\"CreateAccountButtonText\">\r\n        <svg width=\"1.1em\" height=\"1.1em\" viewBox=\"0 0 16 16\" class=\"bi bi-box-arrow-in-right\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-bottom: -3px;\">\r\n          <path fill-rule=\"evenodd\" d=\"M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z\"/>\r\n          <path fill-rule=\"evenodd\" d=\"M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z\"/>\r\n        </svg>&nbsp;\r\n        Contato Paciente\r\n      </div>\r\n    </div>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/contato/contato-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/contato/contato-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: ContatoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContatoPageRoutingModule", function() { return ContatoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _contato_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contato.page */ "./src/app/pages/contato/contato.page.ts");




const routes = [
    {
        path: '',
        component: _contato_page__WEBPACK_IMPORTED_MODULE_3__["ContatoPage"],
    }
];
let ContatoPageRoutingModule = class ContatoPageRoutingModule {
};
ContatoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], ContatoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/contato/contato.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/contato/contato.module.ts ***!
  \*************************************************/
/*! exports provided: ContatoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContatoPageModule", function() { return ContatoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _contato_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contato.page */ "./src/app/pages/contato/contato.page.ts");
/* harmony import */ var _contato_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contato-routing.module */ "./src/app/pages/contato/contato-routing.module.ts");







let ContatoPageModule = class ContatoPageModule {
};
ContatoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _contato_routing_module__WEBPACK_IMPORTED_MODULE_6__["ContatoPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_contato_page__WEBPACK_IMPORTED_MODULE_5__["ContatoPage"]]
    })
], ContatoPageModule);



/***/ }),

/***/ "./src/app/pages/contato/contato.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/contato/contato.page.scss ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #126DE8;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n:host ion-item {\n  --ion-item-background: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n  font-size: small;\n}\n:host ion-card {\n  width: 100%;\n  display: block;\n  margin: auto;\n}\n:host ion-card-content {\n  padding-left: 5px;\n  padding-right: 5px;\n  padding-top: 3px;\n  padding-bottom: 3px;\n}\n:host ion-icon {\n  color: #126DE8;\n  width: 15px;\n  height: 15px;\n}\n:host ion-checkbox {\n  width: 20px;\n  height: 20px;\n}\n:host p {\n  color: #fff;\n  text-align: center;\n}\n:host .choose-paciente {\n  width: 100%;\n  padding: 9px;\n  margin-bottom: 10px;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 12px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 10px;\n}\n:host .Title {\n  font-size: 24px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Regular\";\n  margin: 5px 0 24px;\n}\n:host .Conteudo {\n  font-size: 20px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin-bottom: 5px;\n  text-align: center;\n}\n:host .BackToSignIn {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n:host .CreateAccountButton {\n  position: fixed;\n  width: 100%;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n  display: block;\n}\n:host .CreateAccountButtonText {\n  color: #126DE8;\n  font-size: 18px;\n  font-family: \"Lato-Bold\";\n  margin-left: 10px;\n  align-items: center;\n  text-align: center;\n}\n:host img {\n  border-radius: 50%;\n  height: 40px;\n  margin-left: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGF0by9jb250YXRvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUNJLHFCQUFBO0FBRFI7QUFLSTtFQUNJLHlCQUFBO0VBQ0EsNkJBQUE7RUFDQSxtQ0FBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0FBSFI7QUFNSTtFQUNJLGtDQUFBO0VBQ0EsbUNBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsWUFBQTtFQUVELGdCQUFBO0FBTFA7QUFRSTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQU5OO0FBU0k7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQVBOO0FBVUk7RUFDRSxjQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFSTjtBQVdJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7QUFUUjtBQVlJO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0FBVlI7QUFhSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7QUFYUjtBQWNJO0VBR0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFkUjtBQWlCSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFmUjtBQW1CSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFqQlI7QUFvQkk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFsQlI7QUFxQkk7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFuQlI7QUFzQkk7RUFDSSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7QUFwQlI7QUF3Qkk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQXRCUjtBQTBCSTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0Esd0JBQUE7RUFFQSxrQkFBQTtFQUNBLGtCQUFBO0FBekJSO0FBNkJJO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQTNCUjtBQThCSTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBNUJSO0FBK0JJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSx3QkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQTdCUjtBQWdDSTtFQUNJLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBOUJSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvY29udGF0by9jb250YXRvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0e1xyXG5cclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICAgICAgIFxyXG4gICAgfVxyXG4gIFxyXG4gICAgaW9uLXRvb2xiYXIge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgLS1pb24tY29sb3ItYmFzZTogdHJhbnNwYXJlbnQ7IFxyXG4gICAgICAgIC0tYm9yZGVyLXN0eWxlOiB2YXIoLS1ib3JkZXItc3R5bGUpO1xyXG4gICAgICAgIC0tYm9yZGVyOiAwIG5vbmU7IFxyXG4gICAgICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAgICAgLS1vdXRsaW5lOiAwO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgaW9uLWl0ZW0ge1xyXG4gICAgICAgIC0taW9uLWl0ZW0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgLS1ib3JkZXItc3R5bGU6IHZhcigtLWJvcmRlci1zdHlsZSk7XHJcbiAgICAgICAgLS1ib3JkZXI6IDAgbm9uZTsgXHJcbiAgICAgICAgLS1ib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgICAgICAtLW91dGxpbmU6IDA7XHJcbiAgICAgICAvL3RleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICAgICBmb250LXNpemU6IHNtYWxsO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgaW9uLWNhcmQge1xyXG4gICAgICB3aWR0aDogMTAwJTsgXHJcbiAgICAgIGRpc3BsYXk6IGJsb2NrOyBcclxuICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgaW9uLWNhcmQtY29udGVudCB7XHJcbiAgICAgIHBhZGRpbmctbGVmdDogNXB4OyBcclxuICAgICAgcGFkZGluZy1yaWdodDogNXB4OyBcclxuICAgICAgcGFkZGluZy10b3A6IDNweDsgXHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAzcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICBpb24taWNvbiB7XHJcbiAgICAgIGNvbG9yOiAjMTI2REU4O1xyXG4gICAgICB3aWR0aDogMTVweDsgXHJcbiAgICAgIGhlaWdodDogMTVweDtcclxuICAgIH1cclxuICBcclxuICAgIGlvbi1jaGVja2JveCB7XHJcbiAgICAgICAgd2lkdGg6IDIwcHg7XHJcbiAgICAgICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgcCB7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLmNob29zZS1wYWNpZW50ZXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiA5cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIH1cclxuICBcclxuICAgIC5IZWFkZXIge1xyXG4gICAgICAgIC8vcGFkZGluZzogMzBweDtcclxuICAgICAgICAvL3BhZGRpbmctdG9wOiAzMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuICBcclxuICAgIC5CdXR0b24ge1xyXG4gICAgICAgIHdpZHRoOiA2MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA2MHB4OyAvLzUwXHJcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7IC8vMTVcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgXHJcbiAgICAuQnV0dG9uVGV4dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4OyAvLzEzXHJcbiAgICB9XHJcbiAgXHJcbiAgICAuSW5wdXQge1xyXG4gICAgICAgIHdpZHRoOiA5MCU7IC8vOTBcclxuICAgICAgICBoZWlnaHQ6IDM4cHg7IC8vNDVcclxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7IC8vNlxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIH1cclxuICBcclxuICAgIC5JbnB1dFN0eWxlIHtcclxuICAgICAgICBmb250LXNpemU6IGxhcmdlOyBcclxuICAgICAgICBib3JkZXI6IDAgbm9uZTsgXHJcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAgICAgb3V0bGluZTogMDtcclxuICAgIH1cclxuICBcclxuICAgIC5jb250YWluZXIge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAwIDEwcHg7XHJcbiAgICAgICAgLy9tYXJnaW4tdG9wOiA3MHB4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLlRpdGxlIHtcclxuICAgICAgICBmb250LXNpemU6IDI0cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiBcIkxhdG8tUmVndWxhclwiO1xyXG4gICAgICAgIG1hcmdpbjogNXB4IDAgMjRweDs7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbiAgXHJcbiAgICAuQ29udGV1ZG8ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBjb2xvcjogI2Y0ZWRlODtcclxuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcclxuICAgICAgICAvL21hcmdpbjogMTBweCAwIDIwcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICBcclxuICBcclxuICAgIC5CYWNrVG9TaWduSW4ge1xyXG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgICAgICBsZWZ0OiAwO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICByaWdodDowO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXRvcC13aWR0aDogMXB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogIzIzMjEyOTtcclxuICAgICAgICBwYWRkaW5nOiAxNnB4IDAgMjVweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuQ3JlYXRlQWNjb3VudEJ1dHRvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXRvcC13aWR0aDogMXB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogIzIzMjEyOTtcclxuICAgICAgICBwYWRkaW5nOiAxNnB4IDAgMjVweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuQ3JlYXRlQWNjb3VudEJ1dHRvblRleHQge1xyXG4gICAgICAgIGNvbG9yOiAjMTI2REU4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDsgLy8xNlxyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuICBcclxuICAgIGltZyAgICB7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIGhlaWdodDogNDBweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDoxMHB4O1xyXG4gICAgfVxyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/pages/contato/contato.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/contato/contato.page.ts ***!
  \***********************************************/
/*! exports provided: ContatoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContatoPage", function() { return ContatoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _consulta_consultaService__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../consulta/consultaService */ "./src/app/pages/consulta/consultaService.ts");
/* harmony import */ var _contatoService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contatoService */ "./src/app/pages/contato/contatoService.ts");





//import { ContatoPaciente } from '../consulta/consultaPaciente';

let ContatoPage = class ContatoPage {
    constructor(router, route, toastCtrl, contatoService, c) {
        this.router = router;
        this.route = route;
        this.toastCtrl = toastCtrl;
        this.contatoService = contatoService;
        this.c = c;
        this.customAlertOptions = {
            header: 'Exercícios MMSS',
            cssClass: 'checkbox',
            translucent: true
        };
        this.consulta = [];
        this.ex = '';
        this.ex1 = '';
        this.ex2 = '';
        this.ex3 = '';
        this.exMMII = '';
        this.exMMII1 = '';
        this.exMMII2 = '';
        this.excore = '';
    }
    ngOnInit() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.route.params.subscribe(parametros => {
                this.sus = parametros['sus'];
                console.log('parametro dentro de contato : ' + parametros['sus']);
            });
            const toast = yield (yield this.toastCtrl.create({
                message: '<b>AVISO!</b><br> É importante a realização de uma avaliação muscular criteriosa individualizada para poder prescrever o tratamento adequado obtendo assim sucesso na resposta às condutas prescritas. Sugestões de Exercícios Obs: De acordo com a avaliação do Profissional de Educação Física utilizar a melhor estrategia para seu paciente.',
                duration: 5000, position: 'middle',
                color: 'danger'
            })).present();
        });
    }
    toggleMMSS() {
        if (document.getElementById("MMSS").style.display == 'none') {
            document.getElementById("MMSS").style.display = "block";
        }
        else {
            document.getElementById("MMSS").style.display = "none";
        }
    }
    toggleMMII() {
        if (document.getElementById("MMII").style.display == 'none') {
            document.getElementById("MMII").style.display = "block";
        }
        else {
            document.getElementById("MMII").style.display = "none";
        }
    }
    toggleCore() {
        if (document.getElementById("Core").style.display == 'none') {
            document.getElementById("Core").style.display = "block";
        }
        else {
            document.getElementById("Core").style.display = "none";
        }
    }
    teste() {
        this.router.navigate(['/agenda']);
    }
    blocoNotas() {
        this.router.navigate(['/bloco_notas/' + this.sus + '']);
    }
    agendamento() {
        this.router.navigate(['/agendamento/' + this.sus + '']);
    }
    exercicio(event) {
        this.ex = event.target.value;
    }
    exercicio1(event) {
        this.ex1 = event.target.value;
    }
    exercicio2(event) {
        this.ex2 = event.target.value;
    }
    exercicio3(event) {
        this.ex3 = event.target.value;
    }
    exercicioMMII(event) {
        this.exMMII = event.target.value;
    }
    exercicioMMII1(event) {
        this.exMMII1 = event.target.value;
    }
    exercicioMMII2(event) {
        this.exMMII2 = event.target.value;
    }
    exerciciocore(event) {
        this.excore = event.target.value;
    }
    salvarEX() {
        let exercicio_MMSS = this.ex + '#' + this.ex1 + '#' + this.ex2 + '#' + this.ex3;
        let exercicio_MMII = this.exMMII + '#' + this.exMMII1 + '#' + this.exMMII2;
        let exercicio_core = this.excore;
        this.c.signup(exercicio_MMSS, exercicio_MMII, exercicio_core, this.sus).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Exercícios cadastrados com sucesso ' + this.sus + '!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            var sus = this.sus;
            this.router.navigate(['/contato/' + sus + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Houve um erro, por favor contate o adm do app',
                duration: 8000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
ContatoPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _consulta_consultaService__WEBPACK_IMPORTED_MODULE_4__["ConsultaService"] },
    { type: _contatoService__WEBPACK_IMPORTED_MODULE_5__["ContatoService"] }
];
ContatoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./contato.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contato/contato.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./contato.page.scss */ "./src/app/pages/contato/contato.page.scss")).default]
    })
], ContatoPage);



/***/ }),

/***/ "./src/app/pages/contato/contatoService.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/contato/contatoService.ts ***!
  \*************************************************/
/*! exports provided: ContatoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContatoService", function() { return ContatoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let ContatoService = class ContatoService {
    constructor(http) {
        this.http = http;
    }
    findPaciente(pacienteSus) {
        return this.http.get(API_URL + '/paciente/' + pacienteSus + '');
    }
    signup(exercicio_MMSS, exercicio_MMII, exercicio_core, paciente_sus) {
        return this.http.post(API_URL + '/exercicio/signup', { exercicio_MMSS, exercicio_MMII, exercicio_core, paciente_sus });
    }
};
ContatoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
ContatoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], ContatoService);



/***/ })

}]);
//# sourceMappingURL=pages-contato-contato-module-es2015.js.map