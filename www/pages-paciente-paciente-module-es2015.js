(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-paciente-paciente-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/paciente/paciente.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/paciente/paciente.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n  <ion-buttons slot=\"start\" size=\"x-small\">\r\n    <ion-back-button color=\"light\" defaultHref=\"agenda\" text=\"\" ></ion-back-button>\r\n  </ion-buttons>\r\n</ion-toolbar>\r\n<ion-content>\r\n  <div>\r\n    <img src=\"/sarcex/assets/img/logo.png\" style=\"display: block; margin-left: auto; margin-right: auto; margin-top: 20px;\"/>\r\n    <div class=\"Title\">Vamos cadastrar o paciente.</div>\r\n\r\n    <form [formGroup]=\"pacienteForm\">\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -3px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\r\n      </svg>&nbsp;\r\n      <input name=\"name\" autocomplete=\"off\" type=\"text\" formControlName=\"name\" placeholder=\"Nome Completo\" class=\"InputStyle\"/>\r\n      </ion-item>\r\n      <p ion-text class=\"text08\" *ngIf=\"pacienteForm.get('name').touched && pacienteForm.get('name').hasError('required')\">\r\n        <ion-text color=\"danger\">\r\n          Esse campo é obrigatório!\r\n        </ion-text>\r\n      </p>\r\n    \r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-telephone\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z\"/>\r\n      </svg>&nbsp;\r\n      <input name=\"phone\" autocomplete=\"off\" type=\"tel\" formControlName=\"telefone\" placeholder=\"Telefone\" onkeypress=\"$(this).mask('(00) 90000-0000')\" class=\"InputStyle\"/>\r\n    </ion-item>\r\n    <p ion-text class=\"text08\" *ngIf=\"pacienteForm.get('telefone').touched && pacienteForm.get('telefone').hasError('required')\">\r\n      <ion-text color=\"danger\">\r\n        Esse campo é obrigatório!\r\n      </ion-text>\r\n    </p>\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\" xmlns=\"http://www.w3.org/2000/svg\" fill=\"currentColor\" class=\"bi bi-card-text\" viewBox=\"0 0 16 16\" style=\"color: #126DE8; margin-top: -3px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M14.5 3h-13a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z\"/>\r\n        <path fill-rule=\"evenodd\" d=\"M3 5.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 8a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 8zm0 2.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z\"/>\r\n      </svg>&nbsp;\r\n      <input name=\"sus\" autocomplete=\"off\" type=\"text\" formControlName=\"sus\" placeholder=\"Carteirinha do SUS\" class=\"InputStyle\"/>\r\n    </ion-item>\r\n    <p ion-text class=\"text08\" *ngIf=\"pacienteForm.get('sus').touched && pacienteForm.get('sus').hasError('required')\">\r\n      <ion-text color=\"danger\">\r\n        Esse campo é obrigatório!\r\n      </ion-text>\r\n    </p>\r\n\r\n    <!--<div class= \"texto-email\">\r\n      <small *ngIf = \"pacienteForm.get('sus').errors?.pacienteNameTaken\" class= \"text-danger\">Paciente já cadastrado!</small>\r\n      <small *ngIf = \"pacienteForm.get('sus').valid\" class= \"text-success\" >Paciente não cadastrado!</small>\r\n    </div>-->\r\n\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\"xmlns=\"http://www.w3.org/2000/svg\"  fill=\"currentColor\" class=\"bi bi-calendar4-event\" viewBox=\"0 0 16 16\" style=\"color: #126DE8; margin-top: 1px;\">\r\n        <path d=\"M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1H2zm13 3H1v9a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V5z\"/>\r\n        <path d=\"M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z\"/>\r\n      </svg>&nbsp;\r\n      <input name=\"date\" autocomplete=\"off\" type=\"tel\" formControlName=\"nascimento\" placeholder=\"Data de nascimento\" onkeypress=\"$(this).mask('00/00/0000')\" class=\"InputStyle\"/>\r\n      </ion-item>\r\n      <p ion-text class=\"text08\"  *ngIf=\"pacienteForm.get('nascimento').touched && pacienteForm.get('nascimento').hasError('required')\">\r\n        <ion-text color=\"danger\">\r\n          Esse campo é obrigatório!\r\n        </ion-text>\r\n      </p>\r\n    </form>\r\n\r\n      <div class=\"Conteudo\"><p>Todo paciente Novo tem que passar pelo teste Sarcopenia.</p></div>\r\n      <button class=\"Button\" (click)=\"info()\"><p class=\"ButtonText\">Teste para diagnóstico</p></button>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/paciente/paciente-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/paciente/paciente-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: PacientePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacientePageRoutingModule", function() { return PacientePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _paciente_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./paciente.page */ "./src/app/pages/paciente/paciente.page.ts");




const routes = [
    {
        path: '',
        component: _paciente_page__WEBPACK_IMPORTED_MODULE_3__["PacientePage"],
    }
];
let PacientePageRoutingModule = class PacientePageRoutingModule {
};
PacientePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PacientePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/paciente/paciente.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/paciente/paciente.module.ts ***!
  \***************************************************/
/*! exports provided: PacientePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacientePageModule", function() { return PacientePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _paciente_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paciente.page */ "./src/app/pages/paciente/paciente.page.ts");
/* harmony import */ var _paciente_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./paciente-routing.module */ "./src/app/pages/paciente/paciente-routing.module.ts");







let PacientePageModule = class PacientePageModule {
};
PacientePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _paciente_routing_module__WEBPACK_IMPORTED_MODULE_6__["PacientePageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_paciente_page__WEBPACK_IMPORTED_MODULE_5__["PacientePage"]]
    })
], PacientePageModule);



/***/ }),

/***/ "./src/app/pages/paciente/paciente.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/paciente/paciente.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #126DE8;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n  --border-style: var(--border-style);\n  --border: 0 none;\n  --box-shadow: 0 0 0 0;\n  --outline: 0;\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px;\n}\n:host .Title {\n  font-size: 24px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Regular\";\n  margin: 10px 0 10px;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .Conteudo {\n  font-size: 18px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 10px 0 10px;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .InputStyleS {\n  margin-left: 10px;\n  width: 115%;\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host ion-text.ion-color.ion-color-danger.md.hydrated {\n  font-size: 13px;\n  margin-left: 5%;\n  background: #eb445a;\n  padding: 2%;\n  border-radius: 9px;\n  color: #fff;\n}\n:host small.text-success {\n  background: green;\n  padding: 2%;\n  margin-left: 5%;\n  border-radius: 9px;\n  color: #fff;\n}\n:host small.text-danger {\n  background: #eb445a;\n  padding: 2%;\n  margin-left: 5%;\n  border-radius: 9px;\n  color: #fff;\n}\n:host .texto-email {\n  margin-bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGFjaWVudGUvcGFjaWVudGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0kscUJBQUE7QUFEUjtBQUlJO0VBQ0kseUJBQUE7RUFDQSw2QkFBQTtFQUNBLG1DQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLFlBQUE7QUFGUjtBQUtJO0VBQ0ksT0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBSFI7QUFNSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBSlI7QUFPSTtFQUdJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FBUFI7QUFVSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSx3QkFBQTtFQUNBLG1CQUFBO0FBUlI7QUFXSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFUUjtBQWFJO0VBQ0ksd0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQVhSO0FBY0k7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFaUjtBQWVJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0FBYlI7QUFnQkk7RUFDSSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFkUjtBQWlCSTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBZlI7QUFrQkk7RUFDSSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBaEJSO0FBbUJJO0VBQ0ksbUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQWpCUjtBQW9CSTtFQUNJLG1CQUFBO0FBbEJSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGFjaWVudGUvcGFjaWVudGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG5cclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICB9XHJcblxyXG4gICAgaW9uLXRvb2xiYXIge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgLS1pb24tY29sb3ItYmFzZTogdHJhbnNwYXJlbnQ7IFxyXG4gICAgICAgIC0tYm9yZGVyLXN0eWxlOiB2YXIoLS1ib3JkZXItc3R5bGUpO1xyXG4gICAgICAgIC0tYm9yZGVyOiAwIG5vbmU7IFxyXG4gICAgICAgIC0tYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAgICAgLS1vdXRsaW5lOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIC5Db250YWluZXJ7XHJcbiAgICAgICAgZmxleDogMTtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMzBweDtcclxuICAgIH1cclxuXHJcbiAgICAuVGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tUmVndWxhcic7XHJcbiAgICAgICAgbWFyZ2luOiAxMHB4IDAgMTBweDtcclxuICAgIH1cclxuXHJcbiAgICAuSGVhZGVyIHtcclxuICAgICAgICAvL3BhZGRpbmc6IDMwcHg7XHJcbiAgICAgICAgLy9wYWRkaW5nLXRvcDogMzBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2REU4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLkNvbnRldWRvIHtcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xyXG4gICAgICAgIG1hcmdpbjogMTBweCAwIDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLkJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6IDYwJTtcclxuICAgICAgICBoZWlnaHQ6IDYwcHg7IC8vNTBcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2RDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDhweDsgLy8xNVxyXG4gICAgICAgIGp1c3RpZnktY29udGVudDpjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgXHJcbiAgICBcclxuICAgIC5CdXR0b25UZXh0IHtcclxuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7IC8vMTNcclxuICAgIH1cclxuICBcclxuICAgIC5JbnB1dCB7XHJcbiAgICAgICAgd2lkdGg6IDkwJTsgLy85MFxyXG4gICAgICAgIGhlaWdodDogMzhweDsgLy80NVxyXG4gICAgICAgIHBhZGRpbmc6IDAgMTZweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDsgLy82XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG5cclxuICAgIC5JbnB1dFN0eWxlIHtcclxuICAgICAgICBmb250LXNpemU6IGxhcmdlOyBcclxuICAgICAgICBib3JkZXI6IDAgbm9uZTsgXHJcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMDsgXHJcbiAgICAgICAgb3V0bGluZTogMDtcclxuICAgIH1cclxuXHJcbiAgICAuSW5wdXRTdHlsZVMge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIHdpZHRoOiAxMTUlO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7IFxyXG4gICAgICAgIGJvcmRlcjogMCBub25lOyBcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgICAgICBvdXRsaW5lOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi10ZXh0Lmlvbi1jb2xvci5pb24tY29sb3ItZGFuZ2VyLm1kLmh5ZHJhdGVkIHtcclxuICAgICAgICBmb250LXNpemU6IDEzcHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNlYjQ0NWE7XHJcbiAgICAgICAgcGFkZGluZzogMiU7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOXB4O1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgfVxyXG5cclxuICAgIHNtYWxsLnRleHQtc3VjY2VzcyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZ3JlZW47XHJcbiAgICAgICAgcGFkZGluZzogMiU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDlweDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuXHJcbiAgICBzbWFsbC50ZXh0LWRhbmdlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDojZWI0NDVhO1xyXG4gICAgICAgIHBhZGRpbmc6IDIlO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA5cHg7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICB9IFxyXG5cclxuICAgIC50ZXh0by1lbWFpbHtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/pages/paciente/paciente.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/paciente/paciente.page.ts ***!
  \*************************************************/
/*! exports provided: PacientePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacientePage", function() { return PacientePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _pacienteNotTakenValidatorService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./pacienteNotTakenValidatorService */ "./src/app/pages/paciente/pacienteNotTakenValidatorService.ts");
/* harmony import */ var _pacienteService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./pacienteService */ "./src/app/pages/paciente/pacienteService.ts");







let PacientePage = class PacientePage {
    constructor(navCtrl, menuCtrl, loadingCtrl, router, signupService, toastCtrl, formBuilder, pacienteNotTakenValidatorService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.router = router;
        this.signupService = signupService;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.pacienteNotTakenValidatorService = pacienteNotTakenValidatorService;
    }
    ngOnInit() {
        this.pacienteForm = this.formBuilder.group({
            telefone: ['',
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(15)
                ]
            ],
            name: ['',
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)
                ]
            ],
            sus: ['',
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)
                ] //,this.pacienteNotTakenValidatorService.checkUserNameTaken()
            ],
            nascimento: ['',
                [
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required,
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2),
                    _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)
                ]
            ]
        });
    }
    info() {
        const newPaciente = this.pacienteForm.getRawValue();
        this.signupService.signup(newPaciente).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(newPaciente.sus);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Paciente cadastrado com sucesso ' + newPaciente.sus + '!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            var sus = newPaciente.sus;
            this.router.navigate(['/info/' + sus + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['/home']);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Paciente já cadastrado. Por favor faça seu login e siga os procedimentos necessários!',
                duration: 8000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
PacientePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _pacienteService__WEBPACK_IMPORTED_MODULE_6__["PacienteService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _pacienteNotTakenValidatorService__WEBPACK_IMPORTED_MODULE_5__["PacienteNotTakenValidatorService"] }
];
PacientePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./paciente.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/paciente/paciente.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./paciente.page.scss */ "./src/app/pages/paciente/paciente.page.scss")).default]
    })
], PacientePage);



/***/ }),

/***/ "./src/app/pages/paciente/pacienteNotTakenValidatorService.ts":
/*!********************************************************************!*\
  !*** ./src/app/pages/paciente/pacienteNotTakenValidatorService.ts ***!
  \********************************************************************/
/*! exports provided: PacienteNotTakenValidatorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacienteNotTakenValidatorService", function() { return PacienteNotTakenValidatorService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _pacienteService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pacienteService */ "./src/app/pages/paciente/pacienteService.ts");




let PacienteNotTakenValidatorService = class PacienteNotTakenValidatorService {
    constructor(signUpService) {
        this.signUpService = signUpService;
    }
    checkUserNameTaken() {
        return (control) => {
            console.log('entrou no taken' + control.valueChanges);
            return control.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["debounceTime"])(300)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["switchMap"])(userName => this.signUpService.checkUserNameTaken(userName))).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(isTaken => isTaken ? { pacienteNameTaken: true } : null)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["first"])());
        };
    }
};
PacienteNotTakenValidatorService.ctorParameters = () => [
    { type: _pacienteService__WEBPACK_IMPORTED_MODULE_3__["PacienteService"] }
];
PacienteNotTakenValidatorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], PacienteNotTakenValidatorService);



/***/ }),

/***/ "./src/app/pages/paciente/pacienteService.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/paciente/pacienteService.ts ***!
  \***************************************************/
/*! exports provided: PacienteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacienteService", function() { return PacienteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let PacienteService = class PacienteService {
    constructor(http) {
        this.http = http;
    }
    checkUserNameTaken(pacienteSus) {
        console.log('dentro do taken: ' + pacienteSus);
        return this.http.get(API_URL + '/paciente/exists/' + pacienteSus);
    }
    signup(newPaciente) {
        return this.http.post(API_URL + '/paciente/signup', newPaciente);
    }
    getAll() {
        return this.http.get(`/pacientes`);
    }
    getById(id) {
        return this.http.get(`/paciente/` + id);
    }
    register(paciente) {
        return this.http.post(`/paciente/register`, paciente);
    }
    update(paciente) {
        return this.http.put(`/paciente/` + paciente.id, paciente);
    }
    delete(id) {
        return this.http.delete(`/paciente/` + id);
    }
};
PacienteService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
PacienteService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], PacienteService);



/***/ })

}]);
//# sourceMappingURL=pages-paciente-paciente-module-es2015.js.map