(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-singup-singup-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/singup/singup.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/singup/singup.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesSingupSingupPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\r\n  <div>\r\n    <img src=\"/sarcex/assets/img/logo.png\" style=\"display: block; margin-left: auto; margin-right: auto; margin-top: 20px;\"/>\r\n    <div class=\"Title\">Crie sua conta</div>\r\n\r\n    <form [formGroup]=\"onRegisterForm\" >\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -3px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\r\n      </svg>&nbsp;\r\n      <input name=\"name\" type=\"text\" autocomplete=\"off\" placeholder=\"Nome Completo\" formControlName=\"fullName\" class=\"InputStyle\"/>\r\n    </ion-item>\r\n    <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('fullName').touched && onRegisterForm.get('fullName').hasError('required')\">\r\n      <ion-text color=\"danger\">\r\n        Esse campo é obrigatório!\r\n      </ion-text>\r\n    </p>\r\n\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-telephone\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z\"/>\r\n      </svg>&nbsp;\r\n      <input name=\"phone\" type=\"tel\" autocomplete=\"off\" placeholder=\"Telefone\" formControlName=\"telefone\" onkeypress=\"$(this).mask('(00) 90000-0000')\" class=\"InputStyle\"/>\r\n    </ion-item>\r\n    <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('telefone').touched && onRegisterForm.get('telefone').hasError('required')\">\r\n      <ion-text color=\"danger\">\r\n        Esse campo é obrigatório!\r\n      </ion-text>\r\n    </p>\r\n\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-envelope\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z\"/>\r\n      </svg>&nbsp;\r\n      <input name=\"email\" autocomplete=\"off\" type=\"email\" placeholder=\"E-mail\" formControlName=\"userName\" class=\"InputStyle\"/>\r\n    </ion-item>\r\n    <p ion-text class=\"text08\" *ngIf=\"onRegisterForm.get('userName').touched && onRegisterForm.get('userName').hasError('required')\">\r\n      <ion-text color=\"danger\">\r\n        Esse campo é obrigatório!\r\n      </ion-text>\r\n    </p>\r\n    <div class= \"texto-email\">\r\n      <small *ngIf = \"onRegisterForm.get('userName').errors?.userNameTaken\" class= \"text-danger\" >Email Existente !</small>\r\n      <small *ngIf = \"onRegisterForm.get('userName').valid\" class= \"text-success\" >Email Válido !</small>\r\n    </div>\r\n\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-lock\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -7px;\">\r\n        <path fill-rule=\"evenodd\" d=\"M11.5 8h-7a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1zm-7-1a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-7zm0-3a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z\"/>\r\n      </svg>&nbsp;\r\n      <input  name=\"password\" autocomplete=\"off\" formControlName=\"password\" [type]=\"tipo ? 'text' : 'password'\" placeholder=\"Senha\" class=\"InputStyle\"/>\r\n    </ion-item>\r\n    <div style=\"margin-top: 5px; text-align: right; padding-right: 18px;\" class=\"ForgotPasswordText\" (click)=\"exibirOcultar()\"><span>Exibir senha</span></div>\r\n    <p ion-text color=\"warning\" class=\"text08\" *ngIf=\"onRegisterForm.get('password').touched && onRegisterForm.get('password').hasError('required')\">\r\n      <ion-text color=\"danger\">\r\n        Esse campo é obrigatório!\r\n      </ion-text>\r\n    </p>\r\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n        <input formControlName = \"file\"  type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\">\r\n    </ion-item>\r\n  </form>\r\n\r\n    <button class=\"Button\" (click)=\"paciente()\"><div class=\"ButtonText\"><p>ENVIAR</p></div></button>\r\n  </div>\r\n  <div class=\"BackToSignIn\" (click)=\"login()\">\r\n      <div class=\"BackToSignInText\">\r\n        <svg width=\"1.7em\" height=\"1.7em\" viewBox=\"0 0 16 16\" class=\"bi bi-arrow-left-short\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-bottom: -8px;\">\r\n          <path fill-rule=\"evenodd\" d=\"M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z\"/>\r\n        </svg>&nbsp;\r\n        Voltar para Login\r\n      </div>\r\n    </div>\r\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/singup/singup-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/singup/singup-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: SingUpPageRoutingModule */

    /***/
    function srcAppPagesSingupSingupRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SingUpPageRoutingModule", function () {
        return SingUpPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _singup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./singup.page */
      "./src/app/pages/singup/singup.page.ts");

      var routes = [{
        path: '',
        component: _singup_page__WEBPACK_IMPORTED_MODULE_3__["SingUpPage"]
      }];

      var SingUpPageRoutingModule = function SingUpPageRoutingModule() {
        _classCallCheck(this, SingUpPageRoutingModule);
      };

      SingUpPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SingUpPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/singup/singup.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/singup/singup.module.ts ***!
      \***********************************************/

    /*! exports provided: SingUpPageModule */

    /***/
    function srcAppPagesSingupSingupModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SingUpPageModule", function () {
        return SingUpPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _singup_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./singup.page */
      "./src/app/pages/singup/singup.page.ts");
      /* harmony import */


      var _singup_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./singup-routing.module */
      "./src/app/pages/singup/singup-routing.module.ts");

      var SingUpPageModule = function SingUpPageModule() {
        _classCallCheck(this, SingUpPageModule);
      };

      SingUpPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _singup_routing_module__WEBPACK_IMPORTED_MODULE_6__["SingUpPageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]],
        declarations: [_singup_page__WEBPACK_IMPORTED_MODULE_5__["SingUpPage"]]
      })], SingUpPageModule);
      /***/
    },

    /***/
    "./src/app/pages/singup/singup.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/pages/singup/singup.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesSingupSingupPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px 70px;\n}\n:host .Title {\n  font-size: 24px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Regular\";\n  margin: 5px 0 24px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  padding-left: 5px;\n  padding-bottom: none;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .BackToSignIn {\n  position: absolute;\n  bottom: 0;\n  background: #fff;\n  padding: 16px 0 25px;\n}\n:host .ForgotPasswordText {\n  color: #f4ede8;\n  font-size: 16px;\n  font-family: \"Lato-Regular\";\n  text-align: center;\n}\n:host .BackToSignInText {\n  color: #126DE8;\n  font-size: 18px;\n  font-family: \"Lato-Bold\";\n  align-items: center;\n  text-align: center;\n}\n:host ion-text.ion-color.ion-color-danger.md.hydrated {\n  font-size: 13px;\n  margin-left: 5%;\n  background: #eb445a;\n  padding: 2%;\n  border-radius: 9px;\n  color: #fff;\n}\n:host small.text-success {\n  background: green;\n  padding: 2%;\n  margin-left: 5%;\n  border-radius: 9px;\n  color: #fff;\n}\n:host small.text-danger {\n  background: #eb445a;\n  padding: 2%;\n  margin-left: 5%;\n  border-radius: 9px;\n  color: #fff;\n}\n:host .texto-email {\n  margin-bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2luZ3VwL3Npbmd1cC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxxQkFBQTtBQURSO0FBSUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRlI7QUFNSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFKUjtBQU9JO0VBQ0ksT0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxvQkFBQTtBQUxSO0FBUUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQU5SO0FBU0k7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBUFI7QUFVSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQVJSO0FBV0k7RUFDSSxrQkFBQTtFQUVBLFNBQUE7RUFHQSxnQkFBQTtFQUdBLG9CQUFBO0FBZFI7QUFvQkk7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLDJCQUFBO0VBQ0Esa0JBQUE7QUFsQlI7QUFxQkk7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLHdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQW5CUjtBQXFCSTtFQUNJLGVBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FBbkJSO0FBcUJJO0VBQ0ksaUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtBQW5CUjtBQXFCSTtFQUNJLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUFuQlI7QUFxQkk7RUFDSSxtQkFBQTtBQW5CUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Npbmd1cC9zaW5ndXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG5cclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICB9XHJcblxyXG4gICAgLkJ1dHRvbiB7XHJcbiAgICAgICAgd2lkdGg6IDYwJTtcclxuICAgICAgICBoZWlnaHQ6IDYwcHg7IC8vNTBcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2RDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDhweDsgLy8xNVxyXG4gICAgICAgIGp1c3RpZnktY29udGVudDpjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgXHJcbiAgICBcclxuICAgIC5CdXR0b25UZXh0IHtcclxuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICBmb250LXNpemU6IDE2cHg7IC8vMTNcclxuICAgIH1cclxuXHJcbiAgICAuQ29udGFpbmVye1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAwIDMwcHggNzBweDtcclxuICAgIH1cclxuXHJcbiAgICAuVGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLVJlZ3VsYXInO1xyXG4gICAgICAgIG1hcmdpbjogNXB4IDAgMjRweDtcclxuICAgIH1cclxuXHJcbiAgICAuSW5wdXQge1xyXG4gICAgICAgIHdpZHRoOiA5MCU7IC8vOTBcclxuICAgICAgICBoZWlnaHQ6IDM4cHg7IC8vNDVcclxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XHJcbiAgICAgICAgcGFkZGluZy1ib3R0b206IG5vbmU7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7IC8vNlxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICAuSW5wdXRTdHlsZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZTsgXHJcbiAgICAgICAgYm9yZGVyOiAwIG5vbmU7IFxyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLkJhY2tUb1NpZ25JbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAvLyAgbGVmdDogMDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgIC8vICByaWdodDowO1xyXG4gICAgICAvLyAgbWFyZ2luLXRvcDogMjAlO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgLy8gICBib3JkZXItdG9wLXdpZHRoOiAxcHg7XHJcbiAgICAgIC8vICBib3JkZXItY29sb3I6ICMyMzIxMjk7XHJcbiAgICAgICAgcGFkZGluZzogMTZweCAwIDI1cHg7XHJcbiAgICAgIC8vICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgLy8gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgIC8vICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgfVxyXG5cclxuICAgIC5Gb3Jnb3RQYXNzd29yZFRleHQge1xyXG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDsgLy8xMy41XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLVJlZ3VsYXInO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuQmFja1RvU2lnbkluVGV4dCB7IFxyXG4gICAgICAgIGNvbG9yOiAjMTI2REU4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBmb250LWZhbWlseTogJ0xhdG8tQm9sZCc7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBpb24tdGV4dC5pb24tY29sb3IuaW9uLWNvbG9yLWRhbmdlci5tZC5oeWRyYXRlZCB7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1JTtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZWI0NDVhO1xyXG4gICAgICAgIHBhZGRpbmc6IDIlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDlweDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuICAgIHNtYWxsLnRleHQtc3VjY2VzcyB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogZ3JlZW47XHJcbiAgICAgICAgcGFkZGluZzogMiU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDlweDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuICAgIHNtYWxsLnRleHQtZGFuZ2VyIHtcclxuICAgICAgICBiYWNrZ3JvdW5kOiNlYjQ0NWE7XHJcbiAgICAgICAgcGFkZGluZzogMiU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDUlO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDlweDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH0gXHJcbiAgICAudGV4dG8tZW1haWx7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIH1cclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/singup/singup.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/singup/singup.page.ts ***!
      \*********************************************/

    /*! exports provided: SingUpPage */

    /***/
    function srcAppPagesSingupSingupPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SingUpPage", function () {
        return SingUpPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _register_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./register.service */
      "./src/app/pages/singup/register.service.ts");
      /* harmony import */


      var _user_not_taken_validator_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./user-not-taken.validator.service */
      "./src/app/pages/singup/user-not-taken.validator.service.ts");

      var SingUpPage = /*#__PURE__*/function () {
        function SingUpPage(navCtrl, menuCtrl, loadingCtrl, router, userNotTakenValidatorService, signupService, toastCtrl, formBuilder) {
          _classCallCheck(this, SingUpPage);

          this.navCtrl = navCtrl;
          this.menuCtrl = menuCtrl;
          this.loadingCtrl = loadingCtrl;
          this.router = router;
          this.userNotTakenValidatorService = userNotTakenValidatorService;
          this.signupService = signupService;
          this.toastCtrl = toastCtrl;
          this.formBuilder = formBuilder;
        }

        _createClass(SingUpPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            this.onRegisterForm = this.formBuilder.group({
              fullName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(2), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(40)]],
              userName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email], this.userNotTakenValidatorService.checkUserNameTaken()],
              telefone: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]],
              password: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].minLength(1), _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].maxLength(14)]],
              file: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
          }
        }, {
          key: "paciente",
          value: function paciente() {
            var _this = this;

            var newUser = this.onRegisterForm.getRawValue();
            newUser.email = newUser.userName;
            console.log('novo usuario : ' + newUser.fullName);
            this.signupService.signup(newUser).subscribe(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var _this2 = this;

                var description, allowComments, toast;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        description = newUser.email;
                        allowComments = true;
                        this.signupService.upload(description, allowComments, this.file).subscribe(function () {
                          return _this2.router.navigate(['paciente']);
                        });
                        _context.next = 5;
                        return this.toastCtrl.create({
                          message: 'Usuário cadastrado com sucesso! Agora é só fazer o login!',
                          duration: 4000,
                          position: 'top',
                          color: 'success'
                        });

                      case 5:
                        _context.next = 7;
                        return _context.sent.present();

                      case 7:
                        toast = _context.sent;
                        this.router.navigate(['/paciente']);

                      case 9:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var toast;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        console.log(err);
                        _context2.next = 3;
                        return this.toastCtrl.create({
                          message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! SarcEx agradece!',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 3:
                        _context2.next = 5;
                        return _context2.sent.present();

                      case 5:
                        toast = _context2.sent;

                      case 6:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          }
        }, {
          key: "login",
          value: function login() {
            this.router.navigate(['/home']);
          }
        }, {
          key: "exibirOcultar",
          value: function exibirOcultar() {
            this.tipo = !this.tipo;
          }
        }]);

        return SingUpPage;
      }();

      SingUpPage.ctorParameters = function () {
        return [{
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: _user_not_taken_validator_service__WEBPACK_IMPORTED_MODULE_6__["UserNotTakenValidatorService"]
        }, {
          type: _register_service__WEBPACK_IMPORTED_MODULE_5__["RegisterService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }];
      };

      SingUpPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./singup.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/singup/singup.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./singup.page.scss */
        "./src/app/pages/singup/singup.page.scss"))["default"]]
      })], SingUpPage);
      /***/
    },

    /***/
    "./src/app/pages/singup/user-not-taken.validator.service.ts":
    /*!******************************************************************!*\
      !*** ./src/app/pages/singup/user-not-taken.validator.service.ts ***!
      \******************************************************************/

    /*! exports provided: UserNotTakenValidatorService */

    /***/
    function srcAppPagesSingupUserNotTakenValidatorServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "UserNotTakenValidatorService", function () {
        return UserNotTakenValidatorService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _register_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! ./register.service */
      "./src/app/pages/singup/register.service.ts");
      /* harmony import */


      var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! rxjs/operators */
      "./node_modules/rxjs/_esm2015/operators/index.js");

      var UserNotTakenValidatorService = /*#__PURE__*/function () {
        function UserNotTakenValidatorService(signUpService) {
          _classCallCheck(this, UserNotTakenValidatorService);

          this.signUpService = signUpService;
        }

        _createClass(UserNotTakenValidatorService, [{
          key: "checkUserNameTaken",
          value: function checkUserNameTaken() {
            var _this3 = this;

            return function (control) {
              console.log('entrou no taken' + control.valueChanges);
              return control.valueChanges.pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["debounceTime"])(300)).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["switchMap"])(function (userName) {
                return _this3.signUpService.checkUserNameTaken(userName);
              })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])(function (isTaken) {
                return isTaken ? {
                  userNameTaken: true
                } : null;
              })).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["first"])());
            };
          }
        }]);

        return UserNotTakenValidatorService;
      }();

      UserNotTakenValidatorService.ctorParameters = function () {
        return [{
          type: _register_service__WEBPACK_IMPORTED_MODULE_2__["RegisterService"]
        }];
      };

      UserNotTakenValidatorService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], UserNotTakenValidatorService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-singup-singup-module-es5.js.map