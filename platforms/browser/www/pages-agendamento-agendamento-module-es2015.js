(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-agendamento-agendamento-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agendamento/agendamento.page.html":
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agendamento/agendamento.page.html ***!
  \***********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n  <ion-buttons slot=\"start\" size=\"x-small\">\r\n    <ion-back-button color=\"light\" defaultHref=\"contato\" text=\"\"></ion-back-button>\r\n  </ion-buttons>\r\n</ion-toolbar>\r\n\r\n<ion-content>\r\n  <div class=\"Header\">\r\n       \r\n  </div>\r\n  <div class=\"Container\">\r\n      \r\n      <div class=\"ProfileButton\"></div>\r\n    \r\n      <div class=\"Text\"><b>Agendamento</b></div><br>\r\n         \r\n         <ion-item>\r\n          <ion-label>Data da consulta</ion-label>\r\n          <ion-datetime displayFormat=\"DD/MM/YYYY\" [(ngModel)]=\"data\" ></ion-datetime>\r\n        </ion-item>\r\n        <ion-item>\r\n          <ion-label>Horário da consulta</ion-label>\r\n          <ion-datetime display-format=\"HH:mm\" picker-format=\"HH:mm\" [(ngModel)]=\"hora\"></ion-datetime>\r\n        </ion-item>\r\n       <br>\r\n         <select class=\"custom-select\" [(ngModel)]=\"profissional\">\r\n          <option [ngValue]=\"undefined\" hidden selected>Profissional</option>\r\n          <option>Noeli</option>\r\n          <option>Jessica</option>\r\n        </select>\r\n      \r\n      <button class=\"Button\" (click)=\"agendamento()\"><p class=\"ButtonText\">Salvar</p></button>\r\n  </div>\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/agendamento/agendamento-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/agendamento/agendamento-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: AgendamentoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoPageRoutingModule", function() { return AgendamentoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _agendamento_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./agendamento.page */ "./src/app/pages/agendamento/agendamento.page.ts");




const routes = [
    {
        path: '',
        component: _agendamento_page__WEBPACK_IMPORTED_MODULE_3__["AgendamentoPage"],
    }
];
let AgendamentoPageRoutingModule = class AgendamentoPageRoutingModule {
};
AgendamentoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AgendamentoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/agendamento/agendamento.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/agendamento/agendamento.module.ts ***!
  \*********************************************************/
/*! exports provided: AgendamentoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoPageModule", function() { return AgendamentoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _agendamento_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agendamento-routing.module */ "./src/app/pages/agendamento/agendamento-routing.module.ts");
/* harmony import */ var _agendamento_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./agendamento.page */ "./src/app/pages/agendamento/agendamento.page.ts");







let AgendamentoPageModule = class AgendamentoPageModule {
};
AgendamentoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _agendamento_routing_module__WEBPACK_IMPORTED_MODULE_5__["AgendamentoPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_agendamento_page__WEBPACK_IMPORTED_MODULE_6__["AgendamentoPage"]]
    })
], AgendamentoPageModule);



/***/ }),

/***/ "./src/app/pages/agendamento/agendamento.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/agendamento/agendamento.page.scss ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #fff;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n}\n:host .custom-select {\n  display: block;\n  width: 95%;\n  height: calc(1.5em + .75rem + 2px);\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-size: 1rem;\n  font-weight: 400;\n  line-height: 1.5;\n  color: #495057;\n  border: 1px solid #126DE8;\n  border-radius: 0.75rem;\n  -webkit-appearance: auto;\n  -moz-appearance: auto;\n  appearance: auto;\n  text-align: center;\n  margin: auto;\n}\n:host .Button {\n  width: 70%;\n  height: 50px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 15px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 13px;\n}\n:host .Input {\n  width: 90%;\n  height: 45px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 6px;\n  margin-bottom: 8px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .Container {\n  flex: 1;\n  background-color: #fff;\n  align-items: center;\n  justify-content: center;\n  padding: 0 19px;\n  margin-top: 70px;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .HeaderTitle {\n  color: #f4ede9;\n  font-size: 20px;\n  line-height: 28px;\n  margin-top: 10px;\n}\n:host .UserName {\n  font-family: \"Lato-Bold\";\n  font-size: 22px;\n  color: #f4ede9;\n  margin-bottom: -15px;\n}\n:host .UserAvatar {\n  width: 56px;\n  height: 56px;\n  border-color: #fff;\n  border-radius: 28px;\n}\n:host .Text {\n  font-family: \"Lato-Bold\";\n  font-size: 24px;\n  color: #126DE8;\n  text-align: center;\n  margin-top: 20px;\n}\n:host .List {\n  padding-top: 50;\n  padding-left: 20;\n  padding-right: 20;\n}\n:host .ListContainer {\n  background: #126DE8;\n  border-radius: 15px;\n  padding: 15px;\n  margin-bottom: 10px;\n  margin-left: 6.5px;\n  margin-right: 6.5px;\n  flex-direction: row;\n  align-items: center;\n}\n:host .ListAvatar {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  border-radius: 36px;\n}\n:host .ListInfo {\n  flex: 1;\n  margin-left: 60px;\n}\n:host .ListName {\n  font-family: \"Lato-Bold\";\n  font-size: 18px;\n  color: #fff;\n}\n:host .ListMeta {\n  flex-direction: row;\n  align-items: center;\n  margin-top: 10px;\n  margin-left: 20px;\n}\n:host .ListMetaText {\n  margin-left: 8px;\n  color: #999591;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWdlbmRhbWVudG8vYWdlbmRhbWVudG8ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0ksa0JBQUE7QUFEUjtBQUlJO0VBQ0kseUJBQUE7RUFDQSw2QkFBQTtBQUZSO0FBS0k7RUFDSSxjQUFBO0VBQ0EsVUFBQTtFQUNBLGtDQUFBO0VBQ0EsMENBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFFQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esd0JBQUE7RUFDQSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBSlI7QUFPSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBTFI7QUFTSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFQUjtBQVVJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBUlI7QUFXSTtFQUNJLE9BQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFUUjtBQVlJO0VBR0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFaUjtBQWVJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FBYlI7QUFnQkk7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUFkUjtBQW1CSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQWpCUjtBQW9CSTtFQUNJLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBbEJSO0FBcUJJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFuQlI7QUFzQkk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFwQlI7QUF1Qkk7RUFDSSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7RUFDQSwyQkFBQTtFQUFBLHdCQUFBO0VBQUEsbUJBQUE7RUFDQSxtQkFBQTtBQXJCUjtBQXdCSTtFQUNJLE9BQUE7RUFDQSxpQkFBQTtBQXRCUjtBQXlCSTtFQUNJLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUF2QlI7QUEwQkk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQXhCUjtBQTJCSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtBQXpCUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FnZW5kYW1lbnRvL2FnZW5kYW1lbnRvLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcclxuXHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgfVxyXG5cclxuICAgIGlvbi10b29sYmFyIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgIC0taW9uLWNvbG9yLWJhc2U6IHRyYW5zcGFyZW50O1xyXG4gICAgfVxyXG5cclxuICAgIC5jdXN0b20tc2VsZWN0IHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICB3aWR0aDogOTUlO1xyXG4gICAgICAgIGhlaWdodDogY2FsYygxLjVlbSArIC43NXJlbSArIDJweCk7XHJcbiAgICAgICAgcGFkZGluZzogLjM3NXJlbSAxLjc1cmVtIC4zNzVyZW0gLjc1cmVtO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgICAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjU7XHJcbiAgICAgICAgY29sb3I6ICM0OTUwNTc7XHJcbiAgICAgICAgLy92ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMxMjZERTg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogLjc1cmVtO1xyXG4gICAgICAgIC13ZWJraXQtYXBwZWFyYW5jZTogYXV0bztcclxuICAgICAgICAtbW96LWFwcGVhcmFuY2U6IGF1dG87XHJcbiAgICAgICAgYXBwZWFyYW5jZTogYXV0bztcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgfVxyXG5cclxuICAgIC5CdXR0b24ge1xyXG4gICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZEO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgXHJcbiAgICAuQnV0dG9uVGV4dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLklucHV0IHtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGhlaWdodDogNDVweDtcclxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5Db250YWluZXIge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTlweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA3MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5IZWFkZXIge1xyXG4gICAgICAgIC8vcGFkZGluZzogMzBweDtcclxuICAgICAgICAvL3BhZGRpbmctdG9wOiAzMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuSGVhZGVyVGl0bGUge1xyXG4gICAgICAgIGNvbG9yOiAjZjRlZGU5O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMjhweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5Vc2VyTmFtZSB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLUJvbGQnO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICBjb2xvcjogI2Y0ZWRlOTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcclxuICAgIH1cclxuXHJcbiAgICAvLy5Qcm9maWxlQnV0dG9uIHt9XHJcblxyXG4gICAgLlVzZXJBdmF0YXIge1xyXG4gICAgICAgIHdpZHRoOiA1NnB4O1xyXG4gICAgICAgIGhlaWdodDogNTZweDtcclxuICAgICAgICBib3JkZXItY29sb3I6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjhweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLlRleHQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJyA7XHJcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xyXG4gICAgICAgIGNvbG9yOiAjMTI2REU4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0IHtcclxuICAgICAgICBwYWRkaW5nLXRvcDo1MDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6MjA7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDoyMDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdENvbnRhaW5lciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkRFODtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNi41cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA2LjVweDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5MaXN0QXZhdGFyIHtcclxuICAgICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbiAgICAgICAgaGVpZ2h0OiBmaXQtY29udGVudDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAzNnB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0SW5mbyB7XHJcbiAgICAgICAgZmxleDoxO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA2MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0TmFtZSB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLUJvbGQnO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdE1ldGEge1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0TWV0YVRleHQge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XHJcbiAgICAgICAgY29sb3I6ICM5OTk1OTE7XHJcbiAgICB9XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/agendamento/agendamento.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/agendamento/agendamento.page.ts ***!
  \*******************************************************/
/*! exports provided: AgendamentoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoPage", function() { return AgendamentoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _agendamentoService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agendamentoService */ "./src/app/pages/agendamento/agendamentoService.ts");






let AgendamentoPage = class AgendamentoPage {
    constructor(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, router, userService, toastCtrl, plt, route, agendamentoService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.userService = userService;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.route = route;
        this.agendamentoService = agendamentoService;
        this.user$ = userService.getUser();
        this.nome = window.localStorage.getItem('nome');
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.sus = parametros['sus'];
            console.log('parametro dentro de info : ' + parametros['sus']);
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        //this.menuCtrl.close();
    }
    agendamento() {
        this.paciente_sus = this.sus;
        var a = this.data.split('T');
        var b = this.hora.split('T');
        var c = b[1].split('.');
        this.data_agendamento = a[0] + 'T' + c[0];
        this.agendamentoService.signup(this.data_agendamento, this.sus, this.profissional).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(this.sus);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Agendamento cadastrado com sucesso ' + this.sus + '!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            var sus = this.sus;
            this.router.navigate(['/contato/' + sus + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            this.router.navigate(['/home']);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Houve um erro, por favor contate o adm do app',
                duration: 8000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
AgendamentoPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _agendamentoService__WEBPACK_IMPORTED_MODULE_5__["AgendamentoService"] }
];
AgendamentoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-Agendamento',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./agendamento.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agendamento/agendamento.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./agendamento.page.scss */ "./src/app/pages/agendamento/agendamento.page.scss")).default]
    })
], AgendamentoPage);



/***/ }),

/***/ "./src/app/pages/agendamento/agendamentoService.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/agendamento/agendamentoService.ts ***!
  \*********************************************************/
/*! exports provided: AgendamentoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoService", function() { return AgendamentoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let AgendamentoService = class AgendamentoService {
    constructor(http) {
        this.http = http;
    }
    signup(anotacao, paciente_sus, profissional) {
        console.log('bbb' + anotacao);
        console.log('aaa' + paciente_sus);
        console.log('ccc' + profissional);
        return this.http.post(API_URL + '/agendamento/signup', { anotacao, paciente_sus, profissional });
    }
};
AgendamentoService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AgendamentoService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], AgendamentoService);



/***/ })

}]);
//# sourceMappingURL=pages-agendamento-agendamento-module-es2015.js.map