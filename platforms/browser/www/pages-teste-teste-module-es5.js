(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-teste-teste-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste/teste.page.html":
    /*!***********************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste/teste.page.html ***!
      \***********************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTesteTestePageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div>\n    <div class=\"Title\">Questionário Clínico <br> Dados Clínicos</div>\n    <br>\n    <form [formGroup]=\"testeForm\">\n       \n    <div class=\"Conteudo\">Doenças?</div>\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n      </svg>&nbsp;\n      <input autocomplete=\"off\" formControlName=\"doencas\" name=\"name\" type=\"text\" placeholder=\"Quais?\" class=\"InputStyle\"/>\n      </ion-item>\n      <br>\n    <div class=\"Conteudo\">Medicamentos de uso contínuo?</div>\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n      </svg>&nbsp;\n      <input autocomplete=\"off\" formControlName=\"medicamentos\" name=\"name\" type=\"text\" placeholder=\"Quais?\" class=\"InputStyle\"/>\n      </ion-item>\n      <br>\n    <div class=\"Conteudo\">Tabagismo?</div>\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n      </svg>&nbsp;\n      <input autocomplete=\"off\" formControlName=\"tabagismo\" name=\"name\" type=\"text\" placeholder=\"Sim ou Não\" class=\"InputStyle\"/>\n      </ion-item>\n      <br>\n    <div class=\"Conteudo\">Pratica atividade física?</div>\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n      </svg>&nbsp;\n      <input autocomplete=\"off\" formControlName=\"atividade\" name=\"name\" type=\"text\" placeholder=\"Sim ou Não\" class=\"InputStyle\"/>\n      </ion-item>\n    </form>\n\n    <button class=\"Button\" (click)=\"teste2()\"><p class=\"ButtonText\">Avançar</p></button>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/teste/teste-routing.module.ts":
    /*!*****************************************************!*\
      !*** ./src/app/pages/teste/teste-routing.module.ts ***!
      \*****************************************************/

    /*! exports provided: TestePageRoutingModule */

    /***/
    function srcAppPagesTesteTesteRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TestePageRoutingModule", function () {
        return TestePageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _teste_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./teste.page */
      "./src/app/pages/teste/teste.page.ts");

      var routes = [{
        path: '',
        component: _teste_page__WEBPACK_IMPORTED_MODULE_3__["TestePage"]
      }];

      var TestePageRoutingModule = function TestePageRoutingModule() {
        _classCallCheck(this, TestePageRoutingModule);
      };

      TestePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], TestePageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/teste/teste.module.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/teste/teste.module.ts ***!
      \*********************************************/

    /*! exports provided: TestePageModule */

    /***/
    function srcAppPagesTesteTesteModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TestePageModule", function () {
        return TestePageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _teste_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./teste.page */
      "./src/app/pages/teste/teste.page.ts");
      /* harmony import */


      var _teste_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./teste-routing.module */
      "./src/app/pages/teste/teste-routing.module.ts");

      var TestePageModule = function TestePageModule() {
        _classCallCheck(this, TestePageModule);
      };

      TestePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _teste_routing_module__WEBPACK_IMPORTED_MODULE_6__["TestePageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]],
        declarations: [_teste_page__WEBPACK_IMPORTED_MODULE_5__["TestePage"]]
      })], TestePageModule);
      /***/
    },

    /***/
    "./src/app/pages/teste/teste.page.scss":
    /*!*********************************************!*\
      !*** ./src/app/pages/teste/teste.page.scss ***!
      \*********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTesteTestePageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px;\n  margin-top: 70px;\n}\n:host .Title {\n  margin-top: 70px;\n  font-size: 24px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  text-align: center;\n}\n:host .Conteudo {\n  font-size: 20px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin-bottom: 5px;\n  text-align: center;\n}\n:host .BackToSignIn {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGVzdGUvdGVzdGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0kscUJBQUE7QUFEUjtBQUlJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUZSO0FBTUk7RUFDSSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBSlI7QUFPSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUxSO0FBUUk7RUFDSSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFOUjtBQVNJO0VBQ0ksT0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFQUjtBQVVJO0VBQ0ksZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7QUFSUjtBQVdJO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSx3QkFBQTtFQUVBLGtCQUFBO0VBQ0Esa0JBQUE7QUFWUjtBQWNJO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQVpSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdGVzdGUvdGVzdGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3R7XG5cbiAgICBpb24tY29udGVudCB7XG4gICAgICAgIC0tYmFja2dyb3VuZDogIzEyNkRFODtcbiAgICB9XG4gIFxuICAgIC5CdXR0b24ge1xuICAgICAgICB3aWR0aDogNjAlO1xuICAgICAgICBoZWlnaHQ6IDYwcHg7IC8vNTBcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkQ7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDhweDsgLy8xNVxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB9XG4gIFxuICAgIFxuICAgIC5CdXR0b25UZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgICAgICBmb250LXNpemU6IDE2cHg7IC8vMTNcbiAgICB9XG4gIFxuICAgIC5JbnB1dCB7XG4gICAgICAgIHdpZHRoOiA5MCU7IC8vOTBcbiAgICAgICAgaGVpZ2h0OiAzOHB4OyAvLzQ1XG4gICAgICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4OyAvLzZcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgfVxuXG4gICAgLklucHV0U3R5bGUge1xuICAgICAgICBmb250LXNpemU6IGxhcmdlOyBcbiAgICAgICAgYm9yZGVyOiAwIG5vbmU7IFxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMCAwOyBcbiAgICAgICAgb3V0bGluZTogMDtcbiAgICB9XG5cbiAgICAuQ29udGFpbmVyIHtcbiAgICAgICAgZmxleDogMTtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDAgMzBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNzBweDtcbiAgICB9XG5cbiAgICAuVGl0bGUge1xuICAgICAgICBtYXJnaW4tdG9wOiA3MHB4O1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuICAgIC5Db250ZXVkbyB7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xuICAgICAgICAvL21hcmdpbjogMTBweCAwIDIwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIH1cblxuXG4gICAgLkJhY2tUb1NpZ25JbiB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICByaWdodDowO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICBib3JkZXItdG9wLXdpZHRoOiAxcHg7XG4gICAgICAgIGJvcmRlci1jb2xvcjogIzIzMjEyOTtcbiAgICAgICAgcGFkZGluZzogMTZweCAwIDI1cHg7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgIH1cbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/teste/teste.page.ts":
    /*!*******************************************!*\
      !*** ./src/app/pages/teste/teste.page.ts ***!
      \*******************************************/

    /*! exports provided: TestePage */

    /***/
    function srcAppPagesTesteTestePageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TestePage", function () {
        return TestePage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/core/user/user.service */
      "./src/app/core/user/user.service.ts");
      /* harmony import */


      var _testeService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./testeService */
      "./src/app/pages/teste/testeService.ts");

      var TestePage = /*#__PURE__*/function () {
        function TestePage(router, userService, formBuilder, testeService, toastCtrl, route) {
          _classCallCheck(this, TestePage);

          this.router = router;
          this.userService = userService;
          this.formBuilder = formBuilder;
          this.testeService = testeService;
          this.toastCtrl = toastCtrl;
          this.route = route;
          this.user$ = userService.getUser();
        }

        _createClass(TestePage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.route.params.subscribe(function (parametros) {
              _this.sus = parametros['sus'];
              console.log('parametro dentro de info : ' + parametros['sus']);
            });
            this.testeForm = this.formBuilder.group({
              'doencas': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'medicamentos': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'tabagismo': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'atividade': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
            });
          }
        }, {
          key: "teste2",
          value: function teste2() {
            var _this2 = this;

            var newTeste = this.testeForm.getRawValue();
            newTeste.paciente_id = this.sus;
            this.testeService.signup(newTeste).subscribe(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var toast;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return this.toastCtrl.create({
                          message: 'Teste cadastrado com sucesso!',
                          duration: 4000,
                          position: 'top',
                          color: 'success'
                        });

                      case 2:
                        _context.next = 4;
                        return _context.sent.present();

                      case 4:
                        toast = _context.sent;
                        this.router.navigate(['/teste2/' + this.sus + '']);

                      case 6:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var toast;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        console.log(err);
                        _context2.next = 3;
                        return this.toastCtrl.create({
                          message: 'Houve um erro, por favor avise o administrador do app!',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 3:
                        _context2.next = 5;
                        return _context2.sent.present();

                      case 5:
                        toast = _context2.sent;

                      case 6:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          }
        }]);

        return TestePage;
      }();

      TestePage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: _testeService__WEBPACK_IMPORTED_MODULE_6__["TesteService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }];
      };

      TestePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./teste.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste/teste.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./teste.page.scss */
        "./src/app/pages/teste/teste.page.scss"))["default"]]
      })], TestePage);
      /***/
    },

    /***/
    "./src/app/pages/teste/testeService.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/teste/testeService.ts ***!
      \*********************************************/

    /*! exports provided: TesteService */

    /***/
    function srcAppPagesTesteTesteServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "TesteService", function () {
        return TesteService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js"); //const API_URL = "http://localhost:3000"; 


      var API_URL = 'http://159.203.181.9:3000';

      var TesteService = /*#__PURE__*/function () {
        function TesteService(http) {
          _classCallCheck(this, TesteService);

          this.http = http;
        }

        _createClass(TesteService, [{
          key: "signup",
          value: function signup(newTeste) {
            console.log(newTeste);
            return this.http.post(API_URL + '/questionario/signup', newTeste);
          }
        }]);

        return TesteService;
      }();

      TesteService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      TesteService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], TesteService);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-teste-teste-module-es5.js.map