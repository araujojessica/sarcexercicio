(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-teste4-teste4-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste4/teste4.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste4/teste4.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div>\n    <div class=\"Title\">Teste Time up and GO (TUG)</div>\n    <div class=\"Conteudo\">Teste consiste em levantar-se de uma cadeira sem a ajuda dos braços e andar em ritmo \n                          confortável e seguro a uma distância de três metros, dar a volta, retornar e sentar \n                          novamente, cronometrado em segundos. Para análise do desempenho o escore: > 8,1 s \n                          Indicativo para Sarcopenia.</div>\n\n    <form [formGroup]=\"timeupForm\">\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n      </svg>&nbsp;\n      <input autocomplete=\"off\" name=\"name\" type=\"number\" formControlName=\"timeup_resultado\" placeholder=\"Resposta\" class=\"InputStyle\">\n      </ion-item>\n    </form>\n    <button class=\"Button\" (click)=\"teste5()\"><p class=\"ButtonText\">Avançar</p></button>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/teste4/teste4-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/teste4/teste4-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: Teste4PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste4PageRoutingModule", function() { return Teste4PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _teste4_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teste4.page */ "./src/app/pages/teste4/teste4.page.ts");




const routes = [
    {
        path: '',
        component: _teste4_page__WEBPACK_IMPORTED_MODULE_3__["Teste4Page"],
    }
];
let Teste4PageRoutingModule = class Teste4PageRoutingModule {
};
Teste4PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Teste4PageRoutingModule);



/***/ }),

/***/ "./src/app/pages/teste4/teste4.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/teste4/teste4.module.ts ***!
  \***********************************************/
/*! exports provided: Teste4PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste4PageModule", function() { return Teste4PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _teste4_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./teste4.page */ "./src/app/pages/teste4/teste4.page.ts");
/* harmony import */ var _teste4_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./teste4-routing.module */ "./src/app/pages/teste4/teste4-routing.module.ts");







let Teste4PageModule = class Teste4PageModule {
};
Teste4PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _teste4_routing_module__WEBPACK_IMPORTED_MODULE_6__["Teste4PageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_teste4_page__WEBPACK_IMPORTED_MODULE_5__["Teste4Page"]]
    })
], Teste4PageModule);



/***/ }),

/***/ "./src/app/pages/teste4/teste4.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/teste4/teste4.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .InputText {\n  flex: 1;\n  color: #126DE8;\n  font-size: 16px;\n  font-family: \"lato-Regular\";\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 40px 90px;\n  margin-top: 70px;\n}\n:host .Title {\n  text-align: center;\n  font-size: 24px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 100px 0 20px;\n}\n:host .Conteudo {\n  text-align: center;\n  font-size: 20px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 1px 0 20px;\n  padding: 0 10px 50px;\n}\n:host .BackToSignIn {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 calc(10 + getBottomSpace()px);\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGVzdGU0L3Rlc3RlNC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxxQkFBQTtBQURSO0FBSUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRlI7QUFNSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFKUjtBQU9JO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBTFI7QUFRSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQU5SO0FBU0k7RUFDSSxPQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtBQVBSO0FBV0k7RUFDSSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUFUUjtBQVlJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0Esb0JBQUE7QUFWUjtBQWFJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxvQkFBQTtBQVhSO0FBY0k7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLDZDQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBWlIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy90ZXN0ZTQvdGVzdGU0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0IHtcblxuICAgIGlvbi1jb250ZW50IHtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjMTI2REU4O1xuICAgIH1cbiAgXG4gICAgLkJ1dHRvbiB7XG4gICAgICAgIHdpZHRoOiA2MCU7XG4gICAgICAgIGhlaWdodDogNjBweDsgLy81MFxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2RDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogOHB4OyAvLzE1XG4gICAgICAgIGp1c3RpZnktY29udGVudDpjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIH1cbiAgXG4gICAgXG4gICAgLkJ1dHRvblRleHQge1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDsgLy8xM1xuICAgIH1cbiAgXG4gICAgLklucHV0IHtcbiAgICAgICAgd2lkdGg6IDkwJTsgLy85MFxuICAgICAgICBoZWlnaHQ6IDM4cHg7IC8vNDVcbiAgICAgICAgcGFkZGluZzogMCAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7IC8vNlxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB9XG5cbiAgICAuSW5wdXRTdHlsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7IFxuICAgICAgICBib3JkZXI6IDAgbm9uZTsgXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDA7IFxuICAgICAgICBvdXRsaW5lOiAwO1xuICAgIH1cbiAgXG4gICAgLklucHV0VGV4dCB7XG4gICAgICAgIGZsZXg6IDE7XG4gICAgICAgIGNvbG9yOiAjMTI2REU4O1xuICAgICAgICBmb250LXNpemU6IDE2cHg7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnbGF0by1SZWd1bGFyJztcbiAgICB9XG4gIFxuXG4gICAgLkNvbnRhaW5lciB7XG4gICAgICAgIGZsZXg6IDE7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAwIDQwcHggOTBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogNzBweDtcbiAgICB9XG5cbiAgICAuVGl0bGUge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xuICAgICAgICBtYXJnaW46IDEwMHB4IDAgMjBweDtcbiAgICB9XG5cbiAgICAuQ29udGV1ZG8ge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xuICAgICAgICBtYXJnaW46IDFweCAwIDIwcHg7XG4gICAgICAgIHBhZGRpbmc6IDAgMTBweCA1MHB4O1xuICAgIH1cblxuICAgIC5CYWNrVG9TaWduSW4ge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgcmlnaHQ6MDtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgYm9yZGVyLXRvcC13aWR0aDogMXB4O1xuICAgICAgICBib3JkZXItY29sb3I6ICMyMzIxMjk7XG4gICAgICAgIHBhZGRpbmc6IDE2cHggMCBjYWxjKDEwICsgZ2V0Qm90dG9tU3BhY2UoKXB4KTtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/teste4/teste4.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/teste4/teste4.page.ts ***!
  \*********************************************/
/*! exports provided: Teste4Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste4Page", function() { return Teste4Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _timeupService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./timeupService */ "./src/app/pages/teste4/timeupService.ts");






let Teste4Page = class Teste4Page {
    constructor(router, route, toastCtrl, timeupService, formBuilder) {
        this.router = router;
        this.route = route;
        this.toastCtrl = toastCtrl;
        this.timeupService = timeupService;
        this.formBuilder = formBuilder;
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.sus = parametros['sus'];
            console.log('parametro dentro de timeup : ' + parametros['sus']);
        });
        this.timeupForm = this.formBuilder.group({
            'timeup_resultado': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
    }
    teste5() {
        const newtimeup = this.timeupForm.getRawValue();
        console.log(newtimeup);
        newtimeup.paciente_sus = this.sus;
        this.timeupService.signup(newtimeup).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Timeup cadastrado com sucesso!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            this.router.navigate(['/teste5/' + this.sus + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Houve um erro, por favor avise o administrador do app!',
                duration: 4000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
Teste4Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _timeupService__WEBPACK_IMPORTED_MODULE_5__["TimeupService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
Teste4Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./teste4.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste4/teste4.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./teste4.page.scss */ "./src/app/pages/teste4/teste4.page.scss")).default]
    })
], Teste4Page);



/***/ }),

/***/ "./src/app/pages/teste4/timeupService.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/teste4/timeupService.ts ***!
  \***********************************************/
/*! exports provided: TimeupService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimeupService", function() { return TimeupService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let TimeupService = class TimeupService {
    constructor(http) {
        this.http = http;
    }
    signup(newTimeup) {
        console.log(newTimeup);
        return this.http.post(API_URL + '/timeup/signup', newTimeup);
    }
};
TimeupService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
TimeupService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], TimeupService);



/***/ })

}]);
//# sourceMappingURL=pages-teste4-teste4-module-es2015.js.map