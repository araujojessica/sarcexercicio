(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-teste3-teste3-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste3/teste3.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste3/teste3.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div>\n    <div class=\"Title\">Avaliação Antropométrica (peso, altura, circunferência panturrilha)</div>\n    <div class=\"Conteudo\">IMC: Peso corporal em quilogramas e a altura em metros elevada ao quadrado (IMC= peso/altura²) baixo peso(maior 18,5 kg/m2), peso normal (18,5-24,9 kg/m2), sobrepeso (25-29,9kg/m2)  obesos (> 30 kg/m2).13,14\n                          Circunferência Panturrilha mensurar a maior porção da região da panturrilha sem comprimi-la.\n                          Um valor inferior a 31 centímetro1s indicado depleção de massa muscular.</div>\n\n      <form [formGroup]=\"antropometricaForm\">    \n      <ion-item class=\"Input\" style=\"padding: 0px;\">\n        <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n          <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n        </svg>&nbsp;\n        <input name=\"name\" autocomplete=\"off\" type=\"number\"  formControlName=\"antropometrica_resultado\" placeholder=\"% Resultado\" class=\"InputStyle\"/>\n        </ion-item>\n      </form> \n\n    <button class=\"Button\" (click)=\"teste4()\"><p class=\"ButtonText\">Avançar</p></button>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/teste3/antropometricaService.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/teste3/antropometricaService.ts ***!
  \*******************************************************/
/*! exports provided: AntropometricaService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AntropometricaService", function() { return AntropometricaService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let AntropometricaService = class AntropometricaService {
    constructor(http) {
        this.http = http;
    }
    signup(newAntropometrica) {
        console.log(newAntropometrica);
        return this.http.post(API_URL + '/antropometrica/signup', newAntropometrica);
    }
};
AntropometricaService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AntropometricaService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], AntropometricaService);



/***/ }),

/***/ "./src/app/pages/teste3/teste3-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/teste3/teste3-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: Teste3PageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste3PageRoutingModule", function() { return Teste3PageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _teste3_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./teste3.page */ "./src/app/pages/teste3/teste3.page.ts");




const routes = [
    {
        path: '',
        component: _teste3_page__WEBPACK_IMPORTED_MODULE_3__["Teste3Page"],
    }
];
let Teste3PageRoutingModule = class Teste3PageRoutingModule {
};
Teste3PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], Teste3PageRoutingModule);



/***/ }),

/***/ "./src/app/pages/teste3/teste3.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/teste3/teste3.module.ts ***!
  \***********************************************/
/*! exports provided: Teste3PageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste3PageModule", function() { return Teste3PageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _teste3_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./teste3.page */ "./src/app/pages/teste3/teste3.page.ts");
/* harmony import */ var _teste3_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./teste3-routing.module */ "./src/app/pages/teste3/teste3-routing.module.ts");







let Teste3PageModule = class Teste3PageModule {
};
Teste3PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _teste3_routing_module__WEBPACK_IMPORTED_MODULE_6__["Teste3PageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_teste3_page__WEBPACK_IMPORTED_MODULE_5__["Teste3Page"]]
    })
], Teste3PageModule);



/***/ }),

/***/ "./src/app/pages/teste3/teste3.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/teste3/teste3.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .InputText {\n  flex: 1;\n  color: #126DE8;\n  font-size: 16px;\n  font-family: \"lato-Regular\";\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px 70px;\n}\n:host .Title {\n  text-align: center;\n  font-size: 24px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 100px 0 20px;\n}\n:host .Conteudo {\n  text-align: center;\n  font-size: 18px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 10px 0 30px;\n  padding: 0 20px 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGVzdGUzL3Rlc3RlMy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxxQkFBQTtBQURSO0FBSUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRlI7QUFNSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFKUjtBQU9JO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBTFI7QUFRSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQU5SO0FBU0k7RUFDSSxPQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtBQVBSO0FBVUk7RUFDSSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0FBUlI7QUFXSTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSx3QkFBQTtFQUNBLG9CQUFBO0FBVFI7QUFZSTtFQUNJLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSx3QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7QUFWUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Rlc3RlMy90ZXN0ZTMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICAtLWJhY2tncm91bmQ6ICMxMjZERTg7XG4gICAgfVxuICBcbiAgICAuQnV0dG9uIHtcbiAgICAgICAgd2lkdGg6IDYwJTtcbiAgICAgICAgaGVpZ2h0OiA2MHB4OyAvLzUwXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZEO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7IC8vMTVcbiAgICAgICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgfVxuICBcbiAgICBcbiAgICAuQnV0dG9uVGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4OyAvLzEzXG4gICAgfVxuICBcbiAgICAuSW5wdXQge1xuICAgICAgICB3aWR0aDogOTAlOyAvLzkwXG4gICAgICAgIGhlaWdodDogMzhweDsgLy80NVxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDsgLy82XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIH1cblxuICAgIC5JbnB1dFN0eWxlIHtcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZTsgXG4gICAgICAgIGJvcmRlcjogMCBub25lOyBcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMDsgXG4gICAgICAgIG91dGxpbmU6IDA7XG4gICAgfVxuICBcbiAgICAuSW5wdXRUZXh0IHtcbiAgICAgICAgZmxleDogMTtcbiAgICAgICAgY29sb3I6ICMxMjZERTg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdsYXRvLVJlZ3VsYXInO1xuICAgIH1cblxuICAgIC5Db250YWluZXIge1xuICAgICAgICBmbGV4OiAxO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgcGFkZGluZzogMCAzMHB4IDcwcHg7XG4gICAgfVxuXG4gICAgLlRpdGxlIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgbWFyZ2luOiAxMDBweCAwIDIwcHg7XG4gICAgfVxuXG4gICAgLkNvbnRldWRvIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgbWFyZ2luOiAxMHB4IDAgMzBweDtcbiAgICAgICAgcGFkZGluZzogMCAyMHB4IDA7XG4gICAgfVxufSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/teste3/teste3.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/teste3/teste3.page.ts ***!
  \*********************************************/
/*! exports provided: Teste3Page */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Teste3Page", function() { return Teste3Page; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _antropometricaService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./antropometricaService */ "./src/app/pages/teste3/antropometricaService.ts");






let Teste3Page = class Teste3Page {
    constructor(router, route, formBuilder, antropometricaService, toastCtrl) {
        this.router = router;
        this.route = route;
        this.formBuilder = formBuilder;
        this.antropometricaService = antropometricaService;
        this.toastCtrl = toastCtrl;
    }
    ngOnInit() {
        this.route.params.subscribe(parametros => {
            this.sus = parametros['sus'];
            console.log('parametro dentro de antro : ' + parametros['sus']);
        });
        this.antropometricaForm = this.formBuilder.group({
            'antropometrica_resultado': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
    }
    teste4() {
        const newAntropometrica = this.antropometricaForm.getRawValue();
        console.log(newAntropometrica.antropometrica_resultado);
        newAntropometrica.paciente_sus = this.sus;
        this.antropometricaService.signup(newAntropometrica).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Bioimpedância cadastrado com sucesso!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            this.router.navigate(['/teste4/' + this.sus + '']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            console.log(err);
            const toast = yield (yield this.toastCtrl.create({
                message: 'Houve um erro, por favor avise o administrador do app!',
                duration: 4000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
};
Teste3Page.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _antropometricaService__WEBPACK_IMPORTED_MODULE_5__["AntropometricaService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] }
];
Teste3Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./teste3.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste3/teste3.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./teste3.page.scss */ "./src/app/pages/teste3/teste3.page.scss")).default]
    })
], Teste3Page);



/***/ })

}]);
//# sourceMappingURL=pages-teste3-teste3-module-es2015.js.map