(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-consulta-consulta-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/consulta/consulta.page.html":
    /*!*****************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/consulta/consulta.page.html ***!
      \*****************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesConsultaConsultaPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-toolbar class=\"Header\">\r\n    <ion-buttons slot=\"start\" size=\"x-small\">\r\n      <ion-back-button color=\"light\" defaultHref=\"agenda\" text=\"\"></ion-back-button>\r\n    </ion-buttons>\r\n</ion-toolbar>\r\n  \r\n  <ion-content>\r\n      <div class=\"Title\">Consulta do Paciente</div>\r\n      <div class=\"UserName\" *ngIf=\"consulta as consulta\" >Paciente {{consulta.paciente_name}}</div>\r\n\r\n      <p></p>\r\n    <ion-grid  style=\"background: aliceblue;\">\r\n        <ion-row><p><u>Último teste:</u></p></ion-row><br>\r\n        <ion-row>\r\n          <ion-col>Doenças</ion-col>\r\n          <ion-col>{{doenca}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Medicamentos</ion-col>\r\n          <ion-col >{{medicamentos}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Tabagismo</ion-col>\r\n          <ion-col>{{tabagismo}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Atividades Físicas</ion-col>\r\n          <ion-col>{{atividade}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Bioimpendância M. Magra</ion-col>\r\n          <ion-col>{{massa_magra}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Bioimpendância M. Gorda</ion-col>\r\n          <ion-col>{{gordura}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Antropométrica</ion-col>\r\n          <ion-col>{{antropometrica_resultado}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Time up and GO (TUG)</ion-col>\r\n          <ion-col>{{timeup_resultado}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Caminhada</ion-col>\r\n          <ion-col>{{caminhada_resultado}}</ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n\r\n      <p></p>\r\n\r\n      <ion-grid style=\"background: aliceblue;\">\r\n        <ion-row><p><u>Exercícios:</u></p></ion-row>\r\n        <ion-row>\r\n          <ion-col>MMSS</ion-col>\r\n          <ion-col>{{ex_01}}<br><hr>{{ex_02}}<br><hr>{{ex_03}}<br><hr>{{ex_04}}</ion-col>\r\n        </ion-row>\r\n\r\n        <ion-row>\r\n          <ion-col>MMII</ion-col>\r\n          <ion-col>{{ex_mmii}}<br><hr>{{ex_mmii_02}}<br><hr>{{ex_mmii_03}}</ion-col>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-col>Core</ion-col>\r\n          <ion-col>{{ex_core}}</ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n      <p>Anotações : </p>\r\n      <p>{{anotacoes}}</p>\r\n  </ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/consulta/consulta-routing.module.ts":
    /*!***********************************************************!*\
      !*** ./src/app/pages/consulta/consulta-routing.module.ts ***!
      \***********************************************************/

    /*! exports provided: ConsultaPageRoutingModule */

    /***/
    function srcAppPagesConsultaConsultaRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConsultaPageRoutingModule", function () {
        return ConsultaPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _consulta_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./consulta.page */
      "./src/app/pages/consulta/consulta.page.ts");

      var routes = [{
        path: '',
        component: _consulta_page__WEBPACK_IMPORTED_MODULE_3__["ConsultaPage"]
      }];

      var ConsultaPageRoutingModule = function ConsultaPageRoutingModule() {
        _classCallCheck(this, ConsultaPageRoutingModule);
      };

      ConsultaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ConsultaPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/consulta/consulta.module.ts":
    /*!***************************************************!*\
      !*** ./src/app/pages/consulta/consulta.module.ts ***!
      \***************************************************/

    /*! exports provided: ConsultaPageModule */

    /***/
    function srcAppPagesConsultaConsultaModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConsultaPageModule", function () {
        return ConsultaPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _consulta_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./consulta.page */
      "./src/app/pages/consulta/consulta.page.ts");
      /* harmony import */


      var _consulta_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./consulta-routing.module */
      "./src/app/pages/consulta/consulta-routing.module.ts");

      var ConsultaPageModule = function ConsultaPageModule() {
        _classCallCheck(this, ConsultaPageModule);
      };

      ConsultaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _consulta_routing_module__WEBPACK_IMPORTED_MODULE_6__["ConsultaPageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]],
        declarations: [_consulta_page__WEBPACK_IMPORTED_MODULE_5__["ConsultaPage"]]
      })], ConsultaPageModule);
      /***/
    },

    /***/
    "./src/app/pages/consulta/consulta.page.scss":
    /*!***************************************************!*\
      !*** ./src/app/pages/consulta/consulta.page.scss ***!
      \***************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesConsultaConsultaPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host ion-content {\n  --background: #fff;\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n}\n:host p {\n  color: #126DE8;\n  padding-left: 10px;\n}\n:host .choose-paciente {\n  width: 100%;\n  padding: 9px;\n  margin-bottom: 10px;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 12px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 10px;\n  background: purple;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .Title {\n  margin-top: 10px;\n  font-size: 18px;\n  text-align: center;\n  color: #126DE8;\n  font-family: \"Lato-Bold\";\n}\n:host .Conteudo {\n  font-size: 20px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin-bottom: 5px;\n  text-align: center;\n}\n:host .UserName {\n  font-family: \"Lato-Bold\";\n  font-size: 22px;\n  color: #126DE8;\n  text-align: center;\n}\n:host .BackToSignIn {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n:host .CreateAccountButton {\n  position: fixed;\n  width: 100%;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n  display: block;\n}\n:host .CreateAccountButtonText {\n  color: #126DE8;\n  font-size: 18px;\n  font-family: \"Lato-Bold\";\n  margin-left: 10px;\n  align-items: center;\n  text-align: center;\n}\n:host img {\n  border-radius: 50%;\n  height: 40px;\n  margin-left: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29uc3VsdGEvY29uc3VsdGEucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQURSO0FBT0k7RUFDSSx5QkFBQTtFQUNBLDZCQUFBO0FBTFI7QUFRSTtFQUNJLGNBQUE7RUFDQSxrQkFBQTtBQU5SO0FBU0k7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBUFI7QUFVSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFSUjtBQVlJO0VBQ0ksd0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQVZSO0FBYUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFYUjtBQWNJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0FBWlI7QUFlSTtFQUNJLE9BQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUVBLGtCQUFBO0FBZFI7QUFpQkk7RUFHSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQWpCUjtBQW9CSTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0FBbEJSO0FBcUJJO0VBQ0ksZUFBQTtFQUNBLGNBQUE7RUFDQSx3QkFBQTtFQUVBLGtCQUFBO0VBQ0Esa0JBQUE7QUFwQlI7QUF1Qkk7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFyQlI7QUF5Qkk7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBdkJSO0FBMEJJO0VBQ0ksZUFBQTtFQUNBLFdBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUF4QlI7QUEyQkk7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLHdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBekJSO0FBNEJJO0VBQ0ksa0JBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUExQlIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jb25zdWx0YS9jb25zdWx0YS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdHtcclxuXHJcbiAgICBpb24tY29udGVudCB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICAvL3BhZGRpbmc6IDAgMTBweDtcclxuICAgICAgICAvL21hcmdpbi10b3A6IDcwcHg7XHJcbiAgICAgICAgXHJcbiAgICB9XHJcblxyXG4gICAgaW9uLXRvb2xiYXIge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogdHJhbnNwYXJlbnQ7XHJcbiAgICAgICAgLS1pb24tY29sb3ItYmFzZTogdHJhbnNwYXJlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgcCB7XHJcbiAgICAgICAgY29sb3I6IzEyNkRFODsgXHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5jaG9vc2UtcGFjaWVudGV7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgcGFkZGluZzogOXB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuQnV0dG9uIHtcclxuICAgICAgICB3aWR0aDogNjAlO1xyXG4gICAgICAgIGhlaWdodDogNjBweDsgLy81MFxyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZEO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogOHB4OyAvLzE1XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIH1cclxuICBcclxuICAgIFxyXG4gICAgLkJ1dHRvblRleHQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xyXG4gICAgICAgIGNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTJweDsgLy8xM1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLklucHV0IHtcclxuICAgICAgICB3aWR0aDogOTAlOyAvLzkwXHJcbiAgICAgICAgaGVpZ2h0OiAzOHB4OyAvLzQ1XHJcbiAgICAgICAgcGFkZGluZzogMCAxNnB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogOHB4OyAvLzZcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcblxyXG4gICAgLklucHV0U3R5bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7IFxyXG4gICAgICAgIGJvcmRlcjogMCBub25lOyBcclxuICAgICAgICBib3gtc2hhZG93OiAwIDAgMCAwOyBcclxuICAgICAgICBvdXRsaW5lOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIC5jb250YWluZXIge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBwYWRkaW5nOiAwIDEwcHg7XHJcbiAgICAgICAgLy9tYXJnaW4tdG9wOiA3MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6IHB1cnBsZTtcclxuICAgIH1cclxuXHJcbiAgICAuSGVhZGVyIHtcclxuICAgICAgICAvL3BhZGRpbmc6IDMwcHg7XHJcbiAgICAgICAgLy9wYWRkaW5nLXRvcDogMzBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2REU4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLlRpdGxlIHtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgY29sb3I6ICMxMjZERTg7IC8vI2Y0ZWRlODtcclxuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcclxuICAgIH1cclxuXHJcbiAgICAuQ29udGV1ZG8ge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBjb2xvcjogI2Y0ZWRlODtcclxuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcclxuICAgICAgICAvL21hcmdpbjogMTBweCAwIDIwcHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuVXNlck5hbWUge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgICAgY29sb3I6ICMxMjZERTg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICAgIC8vbWFyZ2luLWJvdHRvbTogLTE1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLkJhY2tUb1NpZ25JbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHJpZ2h0OjA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItdG9wLXdpZHRoOiAxcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjMjMyMTI5O1xyXG4gICAgICAgIHBhZGRpbmc6IDE2cHggMCAyNXB4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIH1cclxuXHJcbiAgICAuQ3JlYXRlQWNjb3VudEJ1dHRvbiB7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHJpZ2h0OiAwO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXRvcC13aWR0aDogMXB4O1xyXG4gICAgICAgIGJvcmRlci1jb2xvcjogIzIzMjEyOTtcclxuICAgICAgICBwYWRkaW5nOiAxNnB4IDAgMjVweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbiAgXHJcbiAgICAuQ3JlYXRlQWNjb3VudEJ1dHRvblRleHQge1xyXG4gICAgICAgIGNvbG9yOiAjMTI2REU4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDsgLy8xNlxyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBtYXJnaW4tbGVmdDogMTBweDtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICBpbWcgICAge1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcclxuICAgICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6MTBweDtcclxuICAgIH1cclxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/consulta/consulta.page.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/consulta/consulta.page.ts ***!
      \*************************************************/

    /*! exports provided: ConsultaPage */

    /***/
    function srcAppPagesConsultaConsultaPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ConsultaPage", function () {
        return ConsultaPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _consultaService__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./consultaService */
      "./src/app/pages/consulta/consultaService.ts");

      var ConsultaPage = /*#__PURE__*/function () {
        function ConsultaPage(router, route, consultaService) {
          _classCallCheck(this, ConsultaPage);

          this.router = router;
          this.route = route;
          this.consultaService = consultaService;
        }

        _createClass(ConsultaPage, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.route.params.subscribe(function (parametros) {
              _this.sus = parametros['sus'];
              console.log('parametro dentro de contato : ' + parametros['sus']);
            });
            this.consultaService.findPaciente2(this.sus).subscribe(function (consulta) {
              _this.consulta = consulta;
              _this.doenca = consulta.doenca;
              _this.medicamentos = consulta.medicamentos;
              _this.tabagismo = consulta.tabagismo;
              _this.atividade = consulta.atividade;
              _this.massa_magra = consulta.massa_magra;
              _this.gordura = consulta.gordura;
              _this.antropometrica_resultado = consulta.antropometrica_resultado;
              _this.timeup_resultado = consulta.timeup_resultado;
              _this.caminhada_resultado = consulta.caminhada_resultado;
              _this.anotacoes = consulta.anotacao;
              var s = consulta.exercicio_MMSS.split('#');

              if (s[0] == 'undefined') {
                _this.ex_01 = '';
              } else {
                _this.ex_01 = '. ' + s[0];
              }

              if (s[1] == 'undefined') {
                _this.ex_02 = '';
              } else {
                _this.ex_02 = '. ' + s[1];
              }

              if (s[2] == 'undefined') {
                _this.ex_03 = '';
              } else {
                _this.ex_03 = '. ' + s[2];
              }

              if (s[3] == 'undefined') {
                _this.ex_04 = '';
              } else {
                _this.ex_04 = '. ' + s[3];
              }

              var n = consulta.exercicio_MMII.split('#');

              if (n[0] == 'undefined') {
                _this.ex_mmii = '';
              } else {
                _this.ex_mmii = '. ' + n[0];
              }

              if (n[1] == 'undefined') {
                _this.ex_mmii_02 = '';
              } else {
                _this.ex_mmii_02 = '. ' + n[1];
              }

              if (n[2] == 'undefined') {
                _this.ex_mmii_03 = '';
              } else {
                _this.ex_mmii_03 = '. ' + n[2];
              }

              if (consulta.exercicio_core == undefined) {
                _this.ex_core = '';
              } else {
                _this.ex_core = '. ' + consulta.exercicio_core;
              }
            });
          }
        }]);

        return ConsultaPage;
      }();

      ConsultaPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
        }, {
          type: _consultaService__WEBPACK_IMPORTED_MODULE_3__["ConsultaService"]
        }];
      };

      ConsultaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./consulta.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/consulta/consulta.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./consulta.page.scss */
        "./src/app/pages/consulta/consulta.page.scss"))["default"]]
      })], ConsultaPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-consulta-consulta-module-es5.js.map