(function () {
  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-teste2-teste2-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste2/teste2.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste2/teste2.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesTeste2Teste2PageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div>\n    <div class=\"Title\"><p>Avaliação Bioimpendância</p></div>\n    <div class=\"Conteudo\">Exame através de uma balança tecnológica, para análise completa do peso corporal, percentual de gordura, de água e de massa muscular.</div>\n\n    <form  [formGroup]=\"bioimpedanciaForm\">\n      <input formControlName=\"paciente_id\" name=\"paciente_id\" *ngIf=\"(user$ | async) as user\" type=\"hidden\" placeholder=\" {{user.id}}\" class=\"InputStyle\"/>\n      <ion-item class=\"Input\" style=\"padding: 0px;\">\n        <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n          <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n        </svg>&nbsp;\n        <input autocomplete=\"off\" name=\"name\" formControlName=\"gordura\" type=\"number\" placeholder=\"% Gordura\" class=\"InputStyle\"/>\n      </ion-item>\n      <div class=\"Conteudo\">Normal: Homem 18% a 24% <br> Mulher: 23% a 31%</div>\n\n      <ion-item class=\"Input\" style=\"padding: 0px;\">\n        <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n          <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n        </svg>&nbsp;\n        <input autocomplete=\"off\" name=\"name\" type=\"number\" formControlName=\"massa_magra\" placeholder=\"% Massa magra\" class=\"InputStyle\"/>\n      </ion-item>\n      <div class=\"Conteudo\">Homem: > 33% Mulher: > 25 %</div>\n    </form>\n    \n    <button class=\"Button\" (click)=\"teste3()\"><p class=\"ButtonText\">Avançar</p></button>\n  </div>\n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/teste2/bioimpedanciaService.ts":
    /*!******************************************************!*\
      !*** ./src/app/pages/teste2/bioimpedanciaService.ts ***!
      \******************************************************/

    /*! exports provided: BioimpedanciService */

    /***/
    function srcAppPagesTeste2BioimpedanciaServiceTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "BioimpedanciService", function () {
        return BioimpedanciService;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common/http */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js"); //const API_URL = "http://localhost:3000"; 


      var API_URL = 'http://159.203.181.9:3000';

      var BioimpedanciService = /*#__PURE__*/function () {
        function BioimpedanciService(http) {
          _classCallCheck(this, BioimpedanciService);

          this.http = http;
        }

        _createClass(BioimpedanciService, [{
          key: "signup",
          value: function signup(newBioimpedancia) {
            console.log(newBioimpedancia);
            return this.http.post(API_URL + '/bioimpedancia/signup', newBioimpedancia);
          }
        }]);

        return BioimpedanciService;
      }();

      BioimpedanciService.ctorParameters = function () {
        return [{
          type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
        }];
      };

      BioimpedanciService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
      })], BioimpedanciService);
      /***/
    },

    /***/
    "./src/app/pages/teste2/teste2-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/teste2/teste2-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: Teste2PageRoutingModule */

    /***/
    function srcAppPagesTeste2Teste2RoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Teste2PageRoutingModule", function () {
        return Teste2PageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _teste2_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./teste2.page */
      "./src/app/pages/teste2/teste2.page.ts");

      var routes = [{
        path: '',
        component: _teste2_page__WEBPACK_IMPORTED_MODULE_3__["Teste2Page"]
      }];

      var Teste2PageRoutingModule = function Teste2PageRoutingModule() {
        _classCallCheck(this, Teste2PageRoutingModule);
      };

      Teste2PageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], Teste2PageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/teste2/teste2.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/teste2/teste2.module.ts ***!
      \***********************************************/

    /*! exports provided: Teste2PageModule */

    /***/
    function srcAppPagesTeste2Teste2ModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Teste2PageModule", function () {
        return Teste2PageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _teste2_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./teste2.page */
      "./src/app/pages/teste2/teste2.page.ts");
      /* harmony import */


      var _teste2_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./teste2-routing.module */
      "./src/app/pages/teste2/teste2-routing.module.ts");

      var Teste2PageModule = function Teste2PageModule() {
        _classCallCheck(this, Teste2PageModule);
      };

      Teste2PageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _teste2_routing_module__WEBPACK_IMPORTED_MODULE_6__["Teste2PageRoutingModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]],
        declarations: [_teste2_page__WEBPACK_IMPORTED_MODULE_5__["Teste2Page"]]
      })], Teste2PageModule);
      /***/
    },

    /***/
    "./src/app/pages/teste2/teste2.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/pages/teste2/teste2.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesTeste2Teste2PageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .InputText {\n  flex: 1;\n  color: #126DE8;\n  font-size: 16px;\n  font-family: \"lato-Regular\";\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 40px 70px;\n  margin-top: 70px;\n}\n:host .Title {\n  text-align: center;\n  font-size: 24px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 100px 0 20px;\n}\n:host .Conteudo {\n  text-align: center;\n  font-size: 20px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 10px 0 20px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdGVzdGUyL3Rlc3RlMi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxxQkFBQTtBQURSO0FBSUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRlI7QUFNSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFKUjtBQU9JO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBTFI7QUFRSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQU5SO0FBU0k7RUFDSSxPQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSwyQkFBQTtBQVBSO0FBVUk7RUFDSSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7QUFSUjtBQVdJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0Esb0JBQUE7QUFUUjtBQVlJO0VBQ0ksa0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLHdCQUFBO0VBQ0EsbUJBQUE7QUFWUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Rlc3RlMi90ZXN0ZTIucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICAtLWJhY2tncm91bmQ6ICMxMjZERTg7XG4gICAgfVxuICBcbiAgICAuQnV0dG9uIHtcbiAgICAgICAgd2lkdGg6IDYwJTtcbiAgICAgICAgaGVpZ2h0OiA2MHB4OyAvLzUwXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZEO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7IC8vMTVcbiAgICAgICAganVzdGlmeS1jb250ZW50OmNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XG4gICAgfVxuICBcbiAgICBcbiAgICAuQnV0dG9uVGV4dCB7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xuICAgICAgICBjb2xvcjogI2ZmZjtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4OyAvLzEzXG4gICAgfVxuICBcbiAgICAuSW5wdXQge1xuICAgICAgICB3aWR0aDogOTAlOyAvLzkwXG4gICAgICAgIGhlaWdodDogMzhweDsgLy80NVxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDhweDsgLy82XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIH1cblxuICAgIC5JbnB1dFN0eWxlIHtcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZTsgXG4gICAgICAgIGJvcmRlcjogMCBub25lOyBcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMDsgXG4gICAgICAgIG91dGxpbmU6IDA7XG4gICAgfVxuICBcbiAgICAuSW5wdXRUZXh0IHtcbiAgICAgICAgZmxleDogMTtcbiAgICAgICAgY29sb3I6ICMxMjZERTg7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdsYXRvLVJlZ3VsYXInO1xuICAgIH1cblxuICAgIC5Db250YWluZXIge1xuICAgICAgICBmbGV4OiAxO1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgcGFkZGluZzogMCA0MHB4IDcwcHg7XG4gICAgICAgIG1hcmdpbi10b3A6IDcwcHg7XG4gICAgfVxuXG4gICAgLlRpdGxlIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgbWFyZ2luOiAxMDBweCAwIDIwcHg7XG4gICAgfVxuXG4gICAgLkNvbnRldWRvIHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgbWFyZ2luOiAxMHB4IDAgMjBweDtcbiAgICB9XG59Il19 */";
      /***/
    },

    /***/
    "./src/app/pages/teste2/teste2.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/teste2/teste2.page.ts ***!
      \*********************************************/

    /*! exports provided: Teste2Page */

    /***/
    function srcAppPagesTeste2Teste2PageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "Teste2Page", function () {
        return Teste2Page;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! src/app/core/user/user.service */
      "./src/app/core/user/user.service.ts");
      /* harmony import */


      var _bioimpedanciaService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./bioimpedanciaService */
      "./src/app/pages/teste2/bioimpedanciaService.ts");

      var Teste2Page = /*#__PURE__*/function () {
        function Teste2Page(router, userService, formBuilder, bioimpedanciaService, toastCtrl, route) {
          _classCallCheck(this, Teste2Page);

          this.router = router;
          this.userService = userService;
          this.formBuilder = formBuilder;
          this.bioimpedanciaService = bioimpedanciaService;
          this.toastCtrl = toastCtrl;
          this.route = route;
        }

        _createClass(Teste2Page, [{
          key: "ngOnInit",
          value: function ngOnInit() {
            var _this = this;

            this.route.params.subscribe(function (parametros) {
              _this.sus = parametros['sus'];
              console.log('parametro dentro de bioo : ' + parametros['sus']);
            });
            this.bioimpedanciaForm = this.formBuilder.group({
              'paciente_id': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'gordura': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])],
              'massa_magra': [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
            });
          }
        }, {
          key: "teste3",
          value: function teste3() {
            var _this2 = this;

            var newBio = this.bioimpedanciaForm.getRawValue();
            console.log(newBio);
            newBio.paciente_id = this.sus;
            this.bioimpedanciaService.signup(newBio).subscribe(function () {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee() {
                var toast;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                  while (1) {
                    switch (_context.prev = _context.next) {
                      case 0:
                        _context.next = 2;
                        return this.toastCtrl.create({
                          message: 'Bioimpedância cadastrado com sucesso!',
                          duration: 4000,
                          position: 'top',
                          color: 'success'
                        });

                      case 2:
                        _context.next = 4;
                        return _context.sent.present();

                      case 4:
                        toast = _context.sent;
                        this.router.navigate(['/teste3/' + this.sus + '']);

                      case 6:
                      case "end":
                        return _context.stop();
                    }
                  }
                }, _callee, this);
              }));
            }, function (err) {
              return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(_this2, void 0, void 0, /*#__PURE__*/regeneratorRuntime.mark(function _callee2() {
                var toast;
                return regeneratorRuntime.wrap(function _callee2$(_context2) {
                  while (1) {
                    switch (_context2.prev = _context2.next) {
                      case 0:
                        console.log(err);
                        _context2.next = 3;
                        return this.toastCtrl.create({
                          message: 'Houve um erro, por favor avise o administrador do app!',
                          duration: 4000,
                          position: 'top',
                          color: 'danger'
                        });

                      case 3:
                        _context2.next = 5;
                        return _context2.sent.present();

                      case 5:
                        toast = _context2.sent;

                      case 6:
                      case "end":
                        return _context2.stop();
                    }
                  }
                }, _callee2, this);
              }));
            });
          }
        }]);

        return Teste2Page;
      }();

      Teste2Page.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
        }, {
          type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"]
        }, {
          type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
        }, {
          type: _bioimpedanciaService__WEBPACK_IMPORTED_MODULE_6__["BioimpedanciService"]
        }, {
          type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"]
        }, {
          type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"]
        }];
      };

      Teste2Page = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./teste2.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/teste2/teste2.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./teste2.page.scss */
        "./src/app/pages/teste2/teste2.page.scss"))["default"]]
      })], Teste2Page);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-teste2-teste2-module-es5.js.map