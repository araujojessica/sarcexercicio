(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-agenda-agenda-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agenda/agenda.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agenda/agenda.page.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n    <ion-buttons slot=\"end\">\r\n      <ion-menu-button color=\"light\"><ion-icon name=\"ellipsis-vertical\"></ion-icon></ion-menu-button>\r\n    </ion-buttons>\r\n    <br><br>\r\n    <div class=\"circle\">\r\n      <img src=\"http://159.203.181.9:3000/imgs/{{urlFoto}}\">\r\n    </div>\r\n    <div class=\"HeaderTitle\"><span *ngIf=\"(user$ | async) as user;else login\" >Olá, <br>{{user.fullName}}  !</span>\r\n    </div>  \r\n  </ion-toolbar>\r\n\r\n<ion-content>\r\n  <div class=\"Container\">\r\n      <div class=\"ProfileButton\"></div>\r\n      <div class=\"Text\"><b>Atendimento do dia</b></div><br>\r\n      \r\n      <div class=\"List\">\r\n\r\n        <div class=\"ListContainer\" *ngFor=\"let agenda of agenda\" (click) = \"consulta(agenda.paciente_sus)\">\r\n          <div class=\"ListAvatar\">\r\n            <div class=\"ListMeta\">\r\n              <div class=\"ListInfo\" >\r\n                <div class=\"ListName\">{{agenda.nome}}</div>\r\n                <div class=\"ListName\">\r\n                  <svg width=\"0.8em\" height=\"0.8em\" viewBox=\"0 0 16 16\" class=\"bi bi-calendar-event\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #fff; margin-bottom: -1px;\">\r\n                    <path fill-rule=\"evenodd\" d=\"M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z\"/>\r\n                    <path d=\"M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z\"/>\r\n                  </svg>\r\n                  {{agenda.semana}}</div>\r\n                <div class=\"ListName\">\r\n                  <svg width=\"0.8em\" height=\"0.8em\" viewBox=\"0 0 16 16\" class=\"bi bi-clock\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #fff; margin-bottom: -1px;\">\r\n                    <path fill-rule=\"evenodd\" d=\"M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm8-7A8 8 0 1 1 0 8a8 8 0 0 1 16 0z\"/>\r\n                    <path fill-rule=\"evenodd\" d=\"M7.5 3a.5.5 0 0 1 .5.5v5.21l3.248 1.856a.5.5 0 0 1-.496.868l-3.5-2A.5.5 0 0 1 7 9V3.5a.5.5 0 0 1 .5-.5z\"/>\r\n                  </svg>\r\n                  {{agenda.hora}}</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n  </div>\r\n</ion-content>\r\n");

/***/ }),

/***/ "./src/app/pages/agenda/agenda-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/agenda/agenda-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: AgendaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaPageRoutingModule", function() { return AgendaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _agenda_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./agenda.page */ "./src/app/pages/agenda/agenda.page.ts");




const routes = [
    {
        path: '',
        component: _agenda_page__WEBPACK_IMPORTED_MODULE_3__["AgendaPage"],
    }
];
let AgendaPageRoutingModule = class AgendaPageRoutingModule {
};
AgendaPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AgendaPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/agenda/agenda.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/agenda/agenda.module.ts ***!
  \***********************************************/
/*! exports provided: AgendaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaPageModule", function() { return AgendaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _agenda_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agenda.page */ "./src/app/pages/agenda/agenda.page.ts");
/* harmony import */ var _agenda_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./agenda-routing.module */ "./src/app/pages/agenda/agenda-routing.module.ts");







let AgendaPageModule = class AgendaPageModule {
};
AgendaPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _agenda_routing_module__WEBPACK_IMPORTED_MODULE_6__["AgendaPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_agenda_page__WEBPACK_IMPORTED_MODULE_5__["AgendaPage"]]
    })
], AgendaPageModule);



/***/ }),

/***/ "./src/app/pages/agenda/agenda.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/agenda/agenda.page.scss ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #fff;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n}\n:host .Button {\n  width: 70%;\n  height: 50px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 15px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 13px;\n}\n:host .Input {\n  width: 90%;\n  height: 45px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 6px;\n  margin-bottom: 8px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .Container {\n  flex: 1;\n  background-color: #fff;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .HeaderTitle {\n  color: #f4ede9;\n  font-size: 20px;\n  line-height: 28px;\n  position: absolute;\n  top: 55%;\n  left: 45%;\n}\n:host .UserName {\n  font-family: \"Lato-Bold\";\n  font-size: 22px;\n  color: #f4ede9;\n  margin-bottom: -15px;\n}\n:host .UserAvatar {\n  width: 56px;\n  height: 56px;\n  border-color: #fff;\n  border-radius: 28px;\n}\n:host .Text {\n  font-family: \"Lato-Bold\";\n  font-size: 24px;\n  color: #126DE8;\n  text-align: center;\n  margin-top: 20px;\n}\n:host .List {\n  padding-top: 50;\n  padding-left: 20;\n  padding-right: 20;\n}\n:host .ListContainer {\n  background: #126DE8;\n  border-radius: 15px;\n  padding: 15px;\n  margin-bottom: 10px;\n  margin-left: 6.5px;\n  margin-right: 6.5px;\n  flex-direction: row;\n  align-items: center;\n}\n:host .ListAvatar {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  border-radius: 36px;\n}\n:host .ListInfo {\n  flex: 1;\n  margin-left: 60px;\n}\n:host .ListName {\n  font-family: \"Lato-Bold\";\n  font-size: 18px;\n  color: #fff;\n}\n:host .ListMeta {\n  flex-direction: row;\n  align-items: center;\n  margin-top: 10px;\n  margin-left: 20px;\n}\n:host .ListMetaText {\n  margin-left: 8px;\n  color: #999591;\n}\n:host .circle {\n  background-color: #aaa;\n  border-radius: 50%;\n  width: 100px;\n  height: 100px;\n  overflow: hidden;\n  position: relative;\n  margin-left: 10px;\n  margin-bottom: 10px;\n}\n:host .circle img {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWdlbmRhL2FnZW5kYS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxrQkFBQTtBQURSO0FBS0k7RUFDSSx5QkFBQTtFQUNBLDZCQUFBO0FBSFI7QUFNSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBSlI7QUFRSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFOUjtBQVNJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBUFI7QUFVSTtFQUNJLE9BQUE7RUFDQSxzQkFBQTtBQVJSO0FBV0k7RUFHSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQVhSO0FBY0k7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtBQVpSO0FBZUk7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUFiUjtBQWtCSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQWhCUjtBQW1CSTtFQUNJLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBakJSO0FBb0JJO0VBQ0ksZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUFsQlI7QUFxQkk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7QUFuQlI7QUFzQkk7RUFDSSwwQkFBQTtFQUFBLHVCQUFBO0VBQUEsa0JBQUE7RUFDQSwyQkFBQTtFQUFBLHdCQUFBO0VBQUEsbUJBQUE7RUFDQSxtQkFBQTtBQXBCUjtBQXVCSTtFQUNJLE9BQUE7RUFDQSxpQkFBQTtBQXJCUjtBQXdCSTtFQUNJLHdCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7QUF0QlI7QUF5Qkk7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQXZCUjtBQTBCSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtBQXhCUjtBQTJCSTtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0FBekJSO0FBNEJNO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtBQTFCUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FnZW5kYS9hZ2VuZGEucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xyXG5cclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICB9XHJcblxyXG5cclxuICAgIGlvbi10b29sYmFyIHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xyXG4gICAgICAgIC0taW9uLWNvbG9yLWJhc2U6IHRyYW5zcGFyZW50O1xyXG4gICAgfVxyXG5cclxuICAgIC5CdXR0b24ge1xyXG4gICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZEO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgXHJcbiAgICAuQnV0dG9uVGV4dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLklucHV0IHtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGhlaWdodDogNDVweDtcclxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5Db250YWluZXIge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgIH1cclxuXHJcbiAgICAuSGVhZGVyIHtcclxuICAgICAgICAvL3BhZGRpbmc6IDMwcHg7XHJcbiAgICAgICAgLy9wYWRkaW5nLXRvcDogMzBweDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2REU4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLkhlYWRlclRpdGxlIHtcclxuICAgICAgICBjb2xvcjogI2Y0ZWRlOTtcclxuICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDI4cHg7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIHRvcDogNTUlO1xyXG4gICAgICAgIGxlZnQ6IDQ1JTtcclxuICAgIH1cclxuXHJcbiAgICAuVXNlck5hbWUge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBmb250LXNpemU6IDIycHg7XHJcbiAgICAgICAgY29sb3I6ICNmNGVkZTk7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogLTE1cHg7XHJcbiAgICB9XHJcblxyXG4gICAgLy8uUHJvZmlsZUJ1dHRvbiB7fVxyXG5cclxuICAgIC5Vc2VyQXZhdGFyIHtcclxuICAgICAgICB3aWR0aDogNTZweDtcclxuICAgICAgICBoZWlnaHQ6IDU2cHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjZmZmO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI4cHg7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5UZXh0IHtcclxuICAgICAgICBmb250LWZhbWlseTogJ0xhdG8tQm9sZCcgO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcclxuICAgICAgICBjb2xvcjogIzEyNkRFODtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdCB7XHJcbiAgICAgICAgcGFkZGluZy10b3A6NTA7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OjIwO1xyXG4gICAgICAgIHBhZGRpbmctcmlnaHQ6MjA7XHJcbiAgICB9XHJcblxyXG4gICAgLkxpc3RDb250YWluZXIge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcclxuICAgICAgICBwYWRkaW5nOiAxNXB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDYuNXB4O1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogNi41cHg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgfVxyXG4gICAgXHJcbiAgICAuTGlzdEF2YXRhciB7XHJcbiAgICAgICAgd2lkdGg6IGZpdC1jb250ZW50O1xyXG4gICAgICAgIGhlaWdodDogZml0LWNvbnRlbnQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMzZweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdEluZm8ge1xyXG4gICAgICAgIGZsZXg6MTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdE5hbWUge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJztcclxuICAgICAgICBmb250LXNpemU6IDE4cHg7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICB9XHJcblxyXG4gICAgLkxpc3RNZXRhIHtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTBweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMjBweDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdE1ldGFUZXh0IHtcclxuICAgICAgICBtYXJnaW4tbGVmdDogOHB4O1xyXG4gICAgICAgIGNvbG9yOiAjOTk5NTkxO1xyXG4gICAgfVxyXG5cclxuICAgIC5jaXJjbGUge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNhYWE7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgIH1cclxuICAgICAgXHJcbiAgICAgIC5jaXJjbGUgaW1nIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgYm90dG9tOiAwO1xyXG4gICAgICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgICB9XHJcbiAgfSJdfQ== */");

/***/ }),

/***/ "./src/app/pages/agenda/agenda.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/agenda/agenda.page.ts ***!
  \*********************************************/
/*! exports provided: AgendaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendaPage", function() { return AgendaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _agendaService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./agendaService */ "./src/app/pages/agenda/agendaService.ts");







let AgendaPage = class AgendaPage {
    constructor(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, router, userService, toastCtrl, plt, agendaService, formBuilder) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.userService = userService;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.agendaService = agendaService;
        this.formBuilder = formBuilder;
        this.agenda = [];
        this.user$ = userService.getUser();
        this.usuario();
    }
    usuario() {
        this.user$.subscribe(profissional => {
            this.profissional = profissional;
            var email = '' + this.profissional.email;
            this.agendaService.findFotoPaciente(email).subscribe(foto => {
                this.foto = foto;
                this.urlFoto = this.foto.url;
            });
        });
    }
    ngOnInit() {
        this.agendaService.findAgendamento().subscribe(agenda => {
            this.agenda = agenda;
            for (var v of this.agenda) {
                var d = new Date(v.data_agendamento);
                var weekday = new Array(7);
                weekday[0] = "Domingo";
                weekday[1] = "Segunda-Feira";
                weekday[2] = "Terça-Feira";
                weekday[3] = "Quarta-Feira";
                weekday[4] = "Quinta-Feira";
                weekday[5] = "Sexta-Feira";
                weekday[6] = "Sábado";
                this.semana = weekday[d.getDay()];
                v.semana = this.semana;
                var h = v.data_agendamento.split('T');
                var hora = h[1].split(':');
                v.hora = hora[0] + ":" + hora[1];
            }
        });
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        this.menuCtrl.close();
        this.usuario();
    }
    consulta(sus) {
        console.log('consulta : ' + sus);
        this.router.navigate(['/consulta/' + sus + '']);
    }
};
AgendaPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _agendaService__WEBPACK_IMPORTED_MODULE_6__["AgendaService"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] }
];
AgendaPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./agenda.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agenda/agenda.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./agenda.page.scss */ "./src/app/pages/agenda/agenda.page.scss")).default]
    })
], AgendaPage);



/***/ })

}]);
//# sourceMappingURL=pages-agenda-agenda-module-es2015.js.map