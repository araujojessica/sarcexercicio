(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-editar-perfil-editar-perfil-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/editar-perfil/editar-perfil.page.html":
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/editar-perfil/editar-perfil.page.html ***!
  \***************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n  <ion-buttons slot=\"start\" size=\"x-small\">\r\n    <ion-back-button color=\"light\" defaultHref=\"agenda\" text=\"\"></ion-back-button>\r\n  </ion-buttons>\r\n</ion-toolbar>\r\n<ion-content>\r\n  <div class=\"Title\">Editar Perfil</div><br>\r\n\r\n  <div class=\"circle\">\r\n    <img src=\"http://159.203.181.9:3000/imgs/{{urlFoto}}\">\r\n  </div>\r\n  <p (click)=\"ap();\"  style=\"text-align: center; margin-top: 4px;\">Editar Foto de Perfil</p>\r\n  <div id = \"ap\" style=\"display: none;\">\r\n    <ion-row class=\"ion-text-wrap\">\r\n      <ion-col>\r\n        <div class = \"edit-email\">\r\n          <ion-item class=\"Input\" style=\"padding: 0px;\">\r\n            <input  type=\"file\" (change)= \"file = $event.target.files[0]\" accept=\"image/*\"><br>\r\n           \r\n        </ion-item>\r\n        \r\n         \r\n          <button class=\"Button\" (click)=\"salvarFoto()\"><p class=\"ButtonText\">SALVAR</p></button>\r\n    \r\n        </div>\r\n      </ion-col>\r\n    </ion-row>\r\n  </div>\r\n  <form [formGroup]= \"editForm\">\r\n    <ion-grid style=\"border: 1px solid lightgray;\" >\r\n      <ion-row>\r\n        <ion-col size=\"3\">Nome</ion-col>\r\n              <ion-col style=\"border-bottom: 1px solid lightgray;\"> \r\n                <input autocomplete=\"off\" name=\"password\" formControlName=\"fullName\"  placeholder = \"{{fullName}}\" style=\"background-color: transparent; border: none; width: 85%;\"/>\r\n              </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"3\">Senha</ion-col>\r\n        <ion-col style=\"border-bottom: 1px solid lightgray;\">\r\n          <input autocomplete=\"off\" name=\"password\" type=\"text\" formControlName=\"password\" placeholder = \"***\" style=\"background-color: transparent; border: none; width: 85%;\"/>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"3\">E-mail</ion-col>\r\n        <ion-col style=\"font-size: 15px; border-bottom: 1px solid lightgray;\">{{user.email}}</ion-col>\r\n      </ion-row>\r\n      <ion-row>\r\n        <ion-col size=\"3\">Telefone</ion-col>\r\n        <ion-col>\r\n          <input autocomplete=\"off\" name=\"password\"  type=\"text\" formControlName=\"telefone\" placeholder = \"{{telefone}}\" style=\"background-color: transparent; border: none; width: 85%;\"/>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </form>\r\n  <button class=\"Button\" (click)=\"salvar()\"><p class=\"ButtonText\">SALVAR</p></button>\r\n\r\n \r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/editar-perfil/editar-perfil-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/pages/editar-perfil/editar-perfil-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: EditarPerfilPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPerfilPageRoutingModule", function() { return EditarPerfilPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _editar_perfil_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./editar-perfil.page */ "./src/app/pages/editar-perfil/editar-perfil.page.ts");




const routes = [
    {
        path: '',
        component: _editar_perfil_page__WEBPACK_IMPORTED_MODULE_3__["EditarPerfilPage"]
    }
];
let EditarPerfilPageRoutingModule = class EditarPerfilPageRoutingModule {
};
EditarPerfilPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], EditarPerfilPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/editar-perfil/editar-perfil.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/editar-perfil/editar-perfil.module.ts ***!
  \*************************************************************/
/*! exports provided: EditarPerfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPerfilPageModule", function() { return EditarPerfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _editar_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./editar-perfil-routing.module */ "./src/app/pages/editar-perfil/editar-perfil-routing.module.ts");
/* harmony import */ var _editar_perfil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./editar-perfil.page */ "./src/app/pages/editar-perfil/editar-perfil.page.ts");







let EditarPerfilPageModule = class EditarPerfilPageModule {
};
EditarPerfilPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _editar_perfil_routing_module__WEBPACK_IMPORTED_MODULE_5__["EditarPerfilPageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"]
        ],
        declarations: [_editar_perfil_page__WEBPACK_IMPORTED_MODULE_6__["EditarPerfilPage"]]
    })
], EditarPerfilPageModule);



/***/ }),

/***/ "./src/app/pages/editar-perfil/editar-perfil.page.scss":
/*!*************************************************************!*\
  !*** ./src/app/pages/editar-perfil/editar-perfil.page.scss ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #fff;\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n}\n:host p {\n  color: #126DE8;\n  padding-left: 10px;\n}\n:host .choose-paciente {\n  width: 100%;\n  padding: 9px;\n  margin-bottom: 10px;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 12px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 10px;\n  background: purple;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .Title {\n  margin-top: 10px;\n  font-size: 20px;\n  text-align: center;\n  color: #126DE8;\n  font-family: \"Lato-Bold\";\n}\n:host .Conteudo {\n  font-size: 20px;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin-bottom: 5px;\n  text-align: center;\n}\n:host .UserName {\n  font-family: \"Lato-Bold\";\n  font-size: 22px;\n  color: #126DE8;\n  text-align: center;\n}\n:host .BackToSignIn {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n:host .CreateAccountButton {\n  position: fixed;\n  width: 100%;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n  display: block;\n}\n:host .CreateAccountButtonText {\n  color: #126DE8;\n  font-size: 18px;\n  font-family: \"Lato-Bold\";\n  margin-left: 10px;\n  align-items: center;\n  text-align: center;\n}\n:host .circle {\n  background-color: #aaa;\n  border-radius: 50%;\n  width: 100px;\n  height: 100px;\n  overflow: hidden;\n  position: relative;\n  margin-left: 10px;\n  margin-bottom: 10px;\n  display: block;\n  margin: auto;\n}\n:host .circle img {\n  position: absolute;\n  bottom: 0;\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvZWRpdGFyLXBlcmZpbC9lZGl0YXItcGVyZmlsLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUFEUjtBQU9JO0VBQ0kseUJBQUE7RUFDQSw2QkFBQTtBQUxSO0FBUUk7RUFDSSxjQUFBO0VBQ0Esa0JBQUE7QUFOUjtBQVNJO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQVBSO0FBVUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBUlI7QUFZSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFWUjtBQWFJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBWFI7QUFjSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQVpSO0FBZUk7RUFDSSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLGVBQUE7RUFFQSxrQkFBQTtBQWRSO0FBaUJJO0VBR0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUFqQlI7QUFvQkk7RUFDSSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSx3QkFBQTtBQWxCUjtBQXFCSTtFQUNJLGVBQUE7RUFDQSxjQUFBO0VBQ0Esd0JBQUE7RUFFQSxrQkFBQTtFQUNBLGtCQUFBO0FBcEJSO0FBdUJJO0VBQ0ksd0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0FBckJSO0FBeUJJO0VBQ0ksa0JBQUE7RUFDQSxPQUFBO0VBQ0EsU0FBQTtFQUNBLFFBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQXZCUjtBQTBCSTtFQUNJLGVBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0Esb0JBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0FBeEJSO0FBMkJJO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSx3QkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQXpCUjtBQTRCSTtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7QUExQlI7QUE2Qk07RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0FBM0JSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvZWRpdGFyLXBlcmZpbC9lZGl0YXItcGVyZmlsLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIjpob3N0e1xyXG5cclxuICAgIGlvbi1jb250ZW50IHtcclxuICAgICAgICAtLWJhY2tncm91bmQ6ICNmZmY7XHJcbiAgICAgICAgZmxleDogMTtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIC8vcGFkZGluZzogMCAxMHB4O1xyXG4gICAgICAgIC8vbWFyZ2luLXRvcDogNzBweDtcclxuICAgICAgICBcclxuICAgIH1cclxuXHJcbiAgICBpb24tdG9vbGJhciB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAtLWlvbi1jb2xvci1iYXNlOiB0cmFuc3BhcmVudDtcclxuICAgIH1cclxuXHJcbiAgICBwIHtcclxuICAgICAgICBjb2xvcjojMTI2REU4OyBcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgICB9XHJcblxyXG4gICAgLmNob29zZS1wYWNpZW50ZXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiA5cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgIH1cclxuICBcclxuICAgIC5CdXR0b24ge1xyXG4gICAgICAgIHdpZHRoOiA2MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA2MHB4OyAvLzUwXHJcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkQ7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA4cHg7IC8vMTVcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgXHJcbiAgICAuQnV0dG9uVGV4dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxMnB4OyAvLzEzXHJcbiAgICB9XHJcbiAgXHJcbiAgICAuSW5wdXQge1xyXG4gICAgICAgIHdpZHRoOiA5MCU7IC8vOTBcclxuICAgICAgICBoZWlnaHQ6IDM4cHg7IC8vNDVcclxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7IC8vNlxyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIH1cclxuXHJcbiAgICAuSW5wdXRTdHlsZSB7XHJcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZTsgXHJcbiAgICAgICAgYm9yZGVyOiAwIG5vbmU7IFxyXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDA7IFxyXG4gICAgICAgIG91dGxpbmU6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLmNvbnRhaW5lciB7XHJcbiAgICAgICAgZmxleDogMTtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTBweDtcclxuICAgICAgICAvL21hcmdpbi10b3A6IDcwcHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogcHVycGxlO1xyXG4gICAgfVxyXG5cclxuICAgIC5IZWFkZXIge1xyXG4gICAgICAgIC8vcGFkZGluZzogMzBweDtcclxuICAgICAgICAvL3BhZGRpbmctdG9wOiAzMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuVGl0bGUge1xyXG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBjb2xvcjogIzEyNkRFODsgLy8jZjRlZGU4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xyXG4gICAgfVxyXG5cclxuICAgIC5Db250ZXVkbyB7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIGNvbG9yOiAjZjRlZGU4O1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLUJvbGQnO1xyXG4gICAgICAgIC8vbWFyZ2luOiAxMHB4IDAgMjBweDtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5Vc2VyTmFtZSB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLUJvbGQnO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICBjb2xvcjogIzEyNkRFODtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgLy9tYXJnaW4tYm90dG9tOiAtMTVweDtcclxuICAgIH1cclxuXHJcbiAgICAuQmFja1RvU2lnbkluIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgcmlnaHQ6MDtcclxuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gICAgICAgIGJvcmRlci10b3Atd2lkdGg6IDFweDtcclxuICAgICAgICBib3JkZXItY29sb3I6ICMyMzIxMjk7XHJcbiAgICAgICAgcGFkZGluZzogMTZweCAwIDI1cHg7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgfVxyXG5cclxuICAgIC5DcmVhdGVBY2NvdW50QnV0dG9uIHtcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICBib3R0b206IDA7XHJcbiAgICAgICAgcmlnaHQ6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItdG9wLXdpZHRoOiAxcHg7XHJcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjMjMyMTI5O1xyXG4gICAgICAgIHBhZGRpbmc6IDE2cHggMCAyNXB4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIH1cclxuICBcclxuICAgIC5DcmVhdGVBY2NvdW50QnV0dG9uVGV4dCB7XHJcbiAgICAgICAgY29sb3I6ICMxMjZERTg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxOHB4OyAvLzE2XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLUJvbGQnO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgfVxyXG5cclxuICAgIC5jaXJjbGUge1xyXG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNhYWE7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG4gICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICBoZWlnaHQ6IDEwMHB4O1xyXG4gICAgICAgIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgICAgICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAuY2lyY2xlIGltZyB7XHJcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgIGJvdHRvbTogMDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgfVxyXG59Il19 */");

/***/ }),

/***/ "./src/app/pages/editar-perfil/editar-perfil.page.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/editar-perfil/editar-perfil.page.ts ***!
  \***********************************************************/
/*! exports provided: EditarPerfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPerfilPage", function() { return EditarPerfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _agenda_agendaService__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../agenda/agendaService */ "./src/app/pages/agenda/agendaService.ts");
/* harmony import */ var _singup_register_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../singup/register.service */ "./src/app/pages/singup/register.service.ts");
/* harmony import */ var _editar_perfilService__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./editar-perfilService */ "./src/app/pages/editar-perfil/editar-perfilService.ts");









let EditarPerfilPage = class EditarPerfilPage {
    constructor(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, router, userService, toastCtrl, plt, formBuilder, editarService, agendaService, signupService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.userService = userService;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.formBuilder = formBuilder;
        this.editarService = editarService;
        this.agendaService = agendaService;
        this.signupService = signupService;
        this.user$ = userService.getUser();
        this.usuario();
    }
    usuario() {
        this.user$.subscribe(user => {
            console.log('user : ' + user.email);
            this.user = user;
            this.email = '' + user.email;
            this.editarService.findProfissional('' + user.email).subscribe(p => {
                this.profissional = p;
                this.fullName = this.profissional.fullName;
                this.telefone = this.profissional.telefone;
            });
            this.agendaService.findFotoPaciente(this.email).subscribe(foto => {
                this.foto = foto;
                this.urlFoto = this.foto.url;
            });
        });
    }
    ngOnInit() {
        this.editForm = this.formBuilder.group({
            fullName: ['', []],
            telefone: ['', []],
            password: ['', []]
        });
    }
    salvar() {
        const newProfissional = this.editForm.getRawValue();
        if (newProfissional.fullName != '') {
            this.editarService.editNome(newProfissional.fullName, this.email).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Usuário editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
                this.router.navigate(['/editar-perfil']);
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (newProfissional.password != '') {
            this.editarService.editPassword(newProfissional.password, this.email).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Usuário editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
                this.router.navigate(['/editar-perfil']);
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
        if (newProfissional.telefone != '') {
            this.editarService.editTelefone(newProfissional.telefone, this.email).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Usuário editado com sucesso!',
                    duration: 4000, position: 'top',
                    color: 'success'
                })).present();
                this.router.navigate(['/editar-perfil']);
            }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
                const toast = yield (yield this.toastCtrl.create({
                    message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                    duration: 4000, position: 'top',
                    color: 'danger'
                })).present();
            }));
        }
    }
    salvarFoto() {
        console.log(this.file);
        let description = this.email;
        let allowComments = true;
        this.signupService.updateFoto(description, allowComments, this.file).subscribe(() => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Foto editada com sucesso!',
                duration: 4000, position: 'top',
                color: 'success'
            })).present();
            this.router.navigate(['/editar-perfil']);
        }), (err) => Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const toast = yield (yield this.toastCtrl.create({
                message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
                duration: 4000, position: 'top',
                color: 'danger'
            })).present();
        }));
    }
    exibirOcultar() {
        this.tipo = !this.tipo;
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        this.menuCtrl.close();
        this.usuario();
    }
    ap() {
        if (document.getElementById("ap").style.display == 'none') {
            document.getElementById("ap").style.display = "block";
        }
        else {
            document.getElementById("ap").style.display = "none";
        }
    }
};
EditarPerfilPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_5__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: _editar_perfilService__WEBPACK_IMPORTED_MODULE_8__["EditarPerfilService"] },
    { type: _agenda_agendaService__WEBPACK_IMPORTED_MODULE_6__["AgendaService"] },
    { type: _singup_register_service__WEBPACK_IMPORTED_MODULE_7__["RegisterService"] }
];
EditarPerfilPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-editar-perfil',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./editar-perfil.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/editar-perfil/editar-perfil.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./editar-perfil.page.scss */ "./src/app/pages/editar-perfil/editar-perfil.page.scss")).default]
    })
], EditarPerfilPage);



/***/ }),

/***/ "./src/app/pages/editar-perfil/editar-perfilService.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/editar-perfil/editar-perfilService.ts ***!
  \*************************************************************/
/*! exports provided: EditarPerfilService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditarPerfilService", function() { return EditarPerfilService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let EditarPerfilService = class EditarPerfilService {
    constructor(http) {
        this.http = http;
    }
    findProfissional(email) {
        return this.http.get(API_URL + '/user/findUser/' + email + '');
    }
    editNome(nome, email) {
        return this.http.get(API_URL + '/user/editNome/' + nome + '/' + email + '');
    }
    editPassword(password, email) {
        return this.http.get(API_URL + '/user/editPassword/' + password + '/' + email + '');
    }
    editTelefone(telefone, email) {
        return this.http.get(API_URL + '/user/editTelefone/' + telefone + '/' + email + '');
    }
    editFoto(description, allowComments, file) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);
            return this.http.post(API_URL + '/photos/update', formData);
        }
    }
};
EditarPerfilService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
EditarPerfilService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], EditarPerfilService);



/***/ })

}]);
//# sourceMappingURL=pages-editar-perfil-editar-perfil-module-es2015.js.map