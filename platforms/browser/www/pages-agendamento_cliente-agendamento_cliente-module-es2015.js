(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-agendamento_cliente-agendamento_cliente-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agendamento_cliente/agendamento_cliente.page.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agendamento_cliente/agendamento_cliente.page.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-toolbar class=\"Header\">\r\n  <ion-buttons slot=\"start\" size=\"x-small\">\r\n    <ion-back-button color=\"light\" defaultHref=\"agenda\" text=\"\"></ion-back-button>\r\n  </ion-buttons>\r\n</ion-toolbar>\r\n\r\n<ion-content>\r\n    \r\n      <div class=\"Text\"><b>Agendamento</b></div><br>\r\n      \r\n      <select class=\"custom-select\" [(ngModel)]=\"pacienteAgenda\" (ngModelChange)=\"pAgenda()\">\r\n        <option [ngValue]=\"undefined\" hidden selected>Escolha o paciente</option>\r\n        <option *ngFor=\"let paciente of paciente\" [ngValue]=\"paciente.sus\">{{paciente.name}}</option>\r\n      </select>\r\n\r\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/agendamento_cliente/agendamento_cliente-routing.module.ts":
/*!*********************************************************************************!*\
  !*** ./src/app/pages/agendamento_cliente/agendamento_cliente-routing.module.ts ***!
  \*********************************************************************************/
/*! exports provided: AgendamentoClientePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoClientePageRoutingModule", function() { return AgendamentoClientePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _agendamento_cliente_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./agendamento_cliente.page */ "./src/app/pages/agendamento_cliente/agendamento_cliente.page.ts");




const routes = [
    {
        path: '',
        component: _agendamento_cliente_page__WEBPACK_IMPORTED_MODULE_3__["AgendamentoClientePage"],
    }
];
let AgendamentoClientePageRoutingModule = class AgendamentoClientePageRoutingModule {
};
AgendamentoClientePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AgendamentoClientePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/agendamento_cliente/agendamento_cliente.module.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/agendamento_cliente/agendamento_cliente.module.ts ***!
  \*************************************************************************/
/*! exports provided: AgendamentoClientePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoClientePageModule", function() { return AgendamentoClientePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _agendamento_cliente_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agendamento_cliente-routing.module */ "./src/app/pages/agendamento_cliente/agendamento_cliente-routing.module.ts");
/* harmony import */ var _agendamento_cliente_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./agendamento_cliente.page */ "./src/app/pages/agendamento_cliente/agendamento_cliente.page.ts");







let AgendamentoClientePageModule = class AgendamentoClientePageModule {
};
AgendamentoClientePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _agendamento_cliente_routing_module__WEBPACK_IMPORTED_MODULE_5__["AgendamentoClientePageRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["ReactiveFormsModule"]
        ],
        declarations: [_agendamento_cliente_page__WEBPACK_IMPORTED_MODULE_6__["AgendamentoClientePage"]]
    })
], AgendamentoClientePageModule);



/***/ }),

/***/ "./src/app/pages/agendamento_cliente/agendamento_cliente.page.scss":
/*!*************************************************************************!*\
  !*** ./src/app/pages/agendamento_cliente/agendamento_cliente.page.scss ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #fff;\n}\n:host ion-toolbar {\n  --background: transparent;\n  --ion-color-base: transparent;\n}\n:host .custom-select {\n  display: block;\n  width: 95%;\n  height: calc(1.5em + .75rem + 2px);\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-size: 1rem;\n  font-weight: 400;\n  line-height: 1.5;\n  color: #495057;\n  border: 1px solid #126DE8;\n  border-radius: 0.75rem;\n  /*-webkit-appearance: auto;\n  -moz-appearance: auto;\n  appearance: auto;*/\n  -webkit-appearance: none;\n  -moz-appearance: none;\n  appearance: auto;\n  text-align: center;\n  margin: auto;\n}\n:host .Button {\n  width: 70%;\n  height: 50px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 15px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 13px;\n}\n:host .Input {\n  width: 90%;\n  height: 45px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 6px;\n  margin-bottom: 8px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .Container {\n  flex: 1;\n  background-color: #fff;\n  align-items: center;\n  justify-content: center;\n  padding: 0 19px;\n  margin-top: 70px;\n}\n:host .Header {\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .HeaderTitle {\n  color: #f4ede9;\n  font-size: 20px;\n  line-height: 28px;\n  margin-top: 10px;\n}\n:host .UserName {\n  font-family: \"Lato-Bold\";\n  font-size: 22px;\n  color: #f4ede9;\n  margin-bottom: -15px;\n}\n:host .UserAvatar {\n  width: 56px;\n  height: 56px;\n  border-color: #fff;\n  border-radius: 28px;\n}\n:host .Text {\n  font-family: \"Lato-Bold\";\n  font-size: 24px;\n  color: #126DE8;\n  text-align: center;\n  margin-top: 20px;\n}\n:host .List {\n  padding-top: 50;\n  padding-left: 20;\n  padding-right: 20;\n}\n:host .ListContainer {\n  background: #126DE8;\n  border-radius: 15px;\n  padding: 15px;\n  margin-bottom: 10px;\n  margin-left: 6.5px;\n  margin-right: 6.5px;\n  flex-direction: row;\n  align-items: center;\n}\n:host .ListAvatar {\n  width: -webkit-fit-content;\n  width: -moz-fit-content;\n  width: fit-content;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content;\n  border-radius: 36px;\n}\n:host .ListInfo {\n  flex: 1;\n  margin-left: 60px;\n}\n:host .ListName {\n  font-family: \"Lato-Bold\";\n  font-size: 18px;\n  color: #fff;\n}\n:host .ListMeta {\n  flex-direction: row;\n  align-items: center;\n  margin-top: 10px;\n  margin-left: 20px;\n}\n:host .ListMetaText {\n  margin-left: 8px;\n  color: #999591;\n}\n:host .choose-paciente {\n  width: 100%;\n  padding: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWdlbmRhbWVudG9fY2xpZW50ZS9hZ2VuZGFtZW50b19jbGllbnRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUNJLGtCQUFBO0FBRFI7QUFJSTtFQUNJLHlCQUFBO0VBQ0EsNkJBQUE7QUFGUjtBQUtJO0VBQ0ksY0FBQTtFQUNBLFVBQUE7RUFDQSxrQ0FBQTtFQUNBLDBDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBRUEseUJBQUE7RUFDQSxzQkFBQTtFQUNBOztvQkFBQTtFQUdBLHdCQUFBO0VBQ0EscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQUpSO0FBT0k7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQUxSO0FBU0k7RUFDSSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBUFI7QUFVSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtBQVJSO0FBV0k7RUFDSSxPQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBVFI7QUFZSTtFQUdJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FBWlI7QUFlSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQWJSO0FBZ0JJO0VBQ0ksd0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FBZFI7QUFtQkk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7QUFqQlI7QUFvQkk7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQWxCUjtBQXFCSTtFQUNJLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBbkJSO0FBc0JJO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBcEJSO0FBdUJJO0VBQ0ksMEJBQUE7RUFBQSx1QkFBQTtFQUFBLGtCQUFBO0VBQ0EsMkJBQUE7RUFBQSx3QkFBQTtFQUFBLG1CQUFBO0VBQ0EsbUJBQUE7QUFyQlI7QUF3Qkk7RUFDSSxPQUFBO0VBQ0EsaUJBQUE7QUF0QlI7QUF5Qkk7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBdkJSO0FBMEJJO0VBQ0ksbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7QUF4QlI7QUEyQkk7RUFDSSxnQkFBQTtFQUNBLGNBQUE7QUF6QlI7QUE0Qkk7RUFDSSxXQUFBO0VBQ0EsYUFBQTtBQTFCUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FnZW5kYW1lbnRvX2NsaWVudGUvYWdlbmRhbWVudG9fY2xpZW50ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyI6aG9zdCB7XHJcblxyXG4gICAgaW9uLWNvbnRlbnQge1xyXG4gICAgICAgIC0tYmFja2dyb3VuZDogI2ZmZjtcclxuICAgIH1cclxuXHJcbiAgICBpb24tdG9vbGJhciB7XHJcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcclxuICAgICAgICAtLWlvbi1jb2xvci1iYXNlOiB0cmFuc3BhcmVudDtcclxuICAgIH1cclxuXHJcbiAgICAuY3VzdG9tLXNlbGVjdCB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgd2lkdGg6IDk1JTtcclxuICAgICAgICBoZWlnaHQ6IGNhbGMoMS41ZW0gKyAuNzVyZW0gKyAycHgpO1xyXG4gICAgICAgIHBhZGRpbmc6IC4zNzVyZW0gMS43NXJlbSAuMzc1cmVtIC43NXJlbTtcclxuICAgICAgICBmb250LXNpemU6IDFyZW07XHJcbiAgICAgICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMS41O1xyXG4gICAgICAgIGNvbG9yOiAjNDk1MDU3O1xyXG4gICAgICAgIC8vdmVydGljYWwtYWxpZ246IG1pZGRsZTtcclxuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMTI2REU4O1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IC43NXJlbTtcclxuICAgICAgICAvKi13ZWJraXQtYXBwZWFyYW5jZTogYXV0bztcclxuICAgICAgICAtbW96LWFwcGVhcmFuY2U6IGF1dG87XHJcbiAgICAgICAgYXBwZWFyYW5jZTogYXV0bzsqL1xyXG4gICAgICAgIC13ZWJraXQtYXBwZWFyYW5jZTogbm9uZTtcclxuICAgICAgICAtbW96LWFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgICAgICAgYXBwZWFyYW5jZTogYXV0bztcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgbWFyZ2luOiBhdXRvO1xyXG4gICAgfVxyXG5cclxuICAgIC5CdXR0b24ge1xyXG4gICAgICAgIHdpZHRoOiA3MCU7XHJcbiAgICAgICAgaGVpZ2h0OiA1MHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZEO1xyXG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICAgICAgbWFyZ2luLXRvcDogMTVweDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6Y2VudGVyO1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gICAgfVxyXG4gIFxyXG4gICAgXHJcbiAgICAuQnV0dG9uVGV4dCB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XHJcbiAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xyXG4gICAgfVxyXG4gIFxyXG4gICAgLklucHV0IHtcclxuICAgICAgICB3aWR0aDogOTAlO1xyXG4gICAgICAgIGhlaWdodDogNDVweDtcclxuICAgICAgICBwYWRkaW5nOiAwIDE2cHg7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiA2cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogOHB4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgICAgICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5Db250YWluZXIge1xyXG4gICAgICAgIGZsZXg6IDE7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcclxuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHBhZGRpbmc6IDAgMTlweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiA3MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5IZWFkZXIge1xyXG4gICAgICAgIC8vcGFkZGluZzogMzBweDtcclxuICAgICAgICAvL3BhZGRpbmctdG9wOiAzMHB4O1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XHJcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIH1cclxuXHJcbiAgICAuSGVhZGVyVGl0bGUge1xyXG4gICAgICAgIGNvbG9yOiAjZjRlZGU5O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcclxuICAgICAgICBsaW5lLWhlaWdodDogMjhweDtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5Vc2VyTmFtZSB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLUJvbGQnO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMjJweDtcclxuICAgICAgICBjb2xvcjogI2Y0ZWRlOTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOiAtMTVweDtcclxuICAgIH1cclxuXHJcbiAgICAvLy5Qcm9maWxlQnV0dG9uIHt9XHJcblxyXG4gICAgLlVzZXJBdmF0YXIge1xyXG4gICAgICAgIHdpZHRoOiA1NnB4O1xyXG4gICAgICAgIGhlaWdodDogNTZweDtcclxuICAgICAgICBib3JkZXItY29sb3I6ICNmZmY7XHJcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjhweDtcclxuICAgIH1cclxuICAgIFxyXG4gICAgLlRleHQge1xyXG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1Cb2xkJyA7XHJcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xyXG4gICAgICAgIGNvbG9yOiAjMTI2REU4O1xyXG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0IHtcclxuICAgICAgICBwYWRkaW5nLXRvcDo1MDtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6MjA7XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDoyMDtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdENvbnRhaW5lciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogIzEyNkRFODtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDE1cHg7XHJcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNi41cHg7XHJcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiA2LjVweDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgICBcclxuICAgIC5MaXN0QXZhdGFyIHtcclxuICAgICAgICB3aWR0aDogZml0LWNvbnRlbnQ7XHJcbiAgICAgICAgaGVpZ2h0OiBmaXQtY29udGVudDtcclxuICAgICAgICBib3JkZXItcmFkaXVzOiAzNnB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0SW5mbyB7XHJcbiAgICAgICAgZmxleDoxO1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA2MHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0TmFtZSB7XHJcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLUJvbGQnO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgIH1cclxuXHJcbiAgICAuTGlzdE1ldGEge1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xyXG4gICAgfVxyXG5cclxuICAgIC5MaXN0TWV0YVRleHQge1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XHJcbiAgICAgICAgY29sb3I6ICM5OTk1OTE7XHJcbiAgICB9XHJcblxyXG4gICAgLmNob29zZS1wYWNpZW50ZXtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBwYWRkaW5nOiAxMHB4O1xyXG4gICAgfVxyXG4gIH0iXX0= */");

/***/ }),

/***/ "./src/app/pages/agendamento_cliente/agendamento_cliente.page.ts":
/*!***********************************************************************!*\
  !*** ./src/app/pages/agendamento_cliente/agendamento_cliente.page.ts ***!
  \***********************************************************************/
/*! exports provided: AgendamentoClientePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoClientePage", function() { return AgendamentoClientePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/user/user.service */ "./src/app/core/user/user.service.ts");
/* harmony import */ var _agendamento_clienteService__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./agendamento_clienteService */ "./src/app/pages/agendamento_cliente/agendamento_clienteService.ts");






let AgendamentoClientePage = class AgendamentoClientePage {
    constructor(navCtrl, menuCtrl, popoverCtrl, alertCtrl, modalCtrl, router, userService, toastCtrl, plt, route, pacienteService) {
        this.navCtrl = navCtrl;
        this.menuCtrl = menuCtrl;
        this.popoverCtrl = popoverCtrl;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.router = router;
        this.userService = userService;
        this.toastCtrl = toastCtrl;
        this.plt = plt;
        this.route = route;
        this.pacienteService = pacienteService;
        this.paciente = [];
        this.user$ = userService.getUser();
    }
    ngOnInit() {
        this.pacienteService.findAllPacientes().subscribe(paciente => {
            console.log('pacientes' + paciente[0].sus);
            this.paciente = paciente;
        });
    }
    pAgenda() {
        this.router.navigate(['/contato/' + this.pacienteAgenda + '']);
    }
    ionViewWillEnter() {
        this.menuCtrl.enable(true);
        this.menuCtrl.close();
    }
    contato(sus) {
    }
};
AgendamentoClientePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["MenuController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_core_user_user_service__WEBPACK_IMPORTED_MODULE_4__["UserService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _agendamento_clienteService__WEBPACK_IMPORTED_MODULE_5__["AgendamentoClienteService"] }
];
AgendamentoClientePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-Agendamento',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./agendamento_cliente.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/agendamento_cliente/agendamento_cliente.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./agendamento_cliente.page.scss */ "./src/app/pages/agendamento_cliente/agendamento_cliente.page.scss")).default]
    })
], AgendamentoClientePage);



/***/ }),

/***/ "./src/app/pages/agendamento_cliente/agendamento_clienteService.ts":
/*!*************************************************************************!*\
  !*** ./src/app/pages/agendamento_cliente/agendamento_clienteService.ts ***!
  \*************************************************************************/
/*! exports provided: AgendamentoClienteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgendamentoClienteService", function() { return AgendamentoClienteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';
let AgendamentoClienteService = class AgendamentoClienteService {
    constructor(http) {
        this.http = http;
    }
    findAllPacientes() {
        return this.http.get(API_URL + '/paciente/users');
    }
};
AgendamentoClienteService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
AgendamentoClienteService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({ providedIn: 'root' })
], AgendamentoClienteService);



/***/ })

}]);
//# sourceMappingURL=pages-agendamento_cliente-agendamento_cliente-module-es2015.js.map