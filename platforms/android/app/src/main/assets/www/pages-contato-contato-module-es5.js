(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-contato-contato-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contato/contato.page.html":
    /*!***************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contato/contato.page.html ***!
      \***************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesContatoContatoPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div class=\"Container\">\n      <div class=\"Header\">\n          <div class=\"HeaderTitle\"><div class=\"UserName\">Contatos Pacientes</div></div>\n          <div class=\"ProfileButton\"></div>\n      </div>\n      <div class=\"List\">\n          <div class=\"ListContainer\">\n              <div class=\"ListAvatar\">\n                  <div class=\"ListMeta\">\n                      <div class=\"ListInfo\">\n                          <div class=\"ListName\">Nome do Paciente</div>\n                      </div>\n                  </div>\n              </div>\n          </div>\n      </div>\n  </div>\n</ion-content>\n";
      /***/
    },

    /***/
    "./src/app/pages/contato/contato-routing.module.ts":
    /*!*********************************************************!*\
      !*** ./src/app/pages/contato/contato-routing.module.ts ***!
      \*********************************************************/

    /*! exports provided: ContatoPageRoutingModule */

    /***/
    function srcAppPagesContatoContatoRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContatoPageRoutingModule", function () {
        return ContatoPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _contato_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./contato.page */
      "./src/app/pages/contato/contato.page.ts");

      var routes = [{
        path: '',
        component: _contato_page__WEBPACK_IMPORTED_MODULE_3__["ContatoPage"]
      }];

      var ContatoPageRoutingModule = function ContatoPageRoutingModule() {
        _classCallCheck(this, ContatoPageRoutingModule);
      };

      ContatoPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], ContatoPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/contato/contato.module.ts":
    /*!*************************************************!*\
      !*** ./src/app/pages/contato/contato.module.ts ***!
      \*************************************************/

    /*! exports provided: ContatoPageModule */

    /***/
    function srcAppPagesContatoContatoModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContatoPageModule", function () {
        return ContatoPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _contato_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./contato.page */
      "./src/app/pages/contato/contato.page.ts");
      /* harmony import */


      var _contato_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./contato-routing.module */
      "./src/app/pages/contato/contato-routing.module.ts");

      var ContatoPageModule = function ContatoPageModule() {
        _classCallCheck(this, ContatoPageModule);
      };

      ContatoPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _contato_routing_module__WEBPACK_IMPORTED_MODULE_6__["ContatoPageRoutingModule"]],
        declarations: [_contato_page__WEBPACK_IMPORTED_MODULE_5__["ContatoPage"]]
      })], ContatoPageModule);
      /***/
    },

    /***/
    "./src/app/pages/contato/contato.page.scss":
    /*!*************************************************!*\
      !*** ./src/app/pages/contato/contato.page.scss ***!
      \*************************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesContatoContatoPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host .Container {\n  flex: 1;\n  background-color: #fff;\n}\n:host .Header {\n  padding: 30px;\n  padding-top: calc(getStatusBarHeight() + 24px);\n  background: #126DE8;\n  flex-direction: row;\n  justify-content: space-between;\n  align-items: center;\n}\n:host .HeaderTitle {\n  color: #f4ede9;\n  font-size: 20px;\n  line-height: 28px;\n}\n:host .UserName {\n  font-family: Lato-Bold;\n  font-size: 24px;\n}\n:host .ProfileButton {\n  color: #fff;\n}\n:host .UserAvatar {\n  width: 56px;\n  height: 56px;\n  border-color: #fff;\n  border-radius: 28px;\n}\n:host .Text {\n  font-family: Lato-Bold;\n  font-size: 24px;\n  color: #126DE8;\n  text-align: center;\n  margin-top: 20px;\n}\n:host .List {\n  padding-top: 50;\n  padding-left: 20;\n  padding-right: 20;\n}\n:host .ListContainer {\n  background: #126DE8;\n  border-radius: 20px;\n  padding: 20px;\n  margin-bottom: 16px;\n  flex-direction: row;\n  align-items: center;\n}\n:host .ListAvatar {\n  width: 72px;\n  height: 72px;\n  border-radius: 36px;\n}\n:host .ListInfo {\n  flex: 1;\n  margin-left: 20px;\n}\n:host .ListName {\n  font-family: \"Lato-Bold\";\n  font-size: 18px;\n  color: #fff;\n}\n:host .ListMeta {\n  flex-direction: row;\n  align-items: center;\n  margin-top: 10px;\n}\n:host .ListMetaText {\n  margin-left: 8px;\n  color: #999591;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGF0by9jb250YXRvLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFSTtFQUNJLE9BQUE7RUFDQSxzQkFBQTtBQURSO0FBSUk7RUFDSSxhQUFBO0VBQ0EsOENBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBQUZSO0FBS0k7RUFDSSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FBSFI7QUFNSTtFQUNJLHNCQUFBO0VBQ0EsZUFBQTtBQUpSO0FBT0k7RUFDSSxXQUFBO0FBTFI7QUFRSTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQU5SO0FBU0k7RUFDSSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtBQVBSO0FBVUk7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQVJSO0FBV0k7RUFDSSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQVRSO0FBWUk7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBVlI7QUFhSTtFQUNJLE9BQUE7RUFDQSxpQkFBQTtBQVhSO0FBY0k7RUFDSSx3QkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBWlI7QUFlSTtFQUNJLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQWJSO0FBZ0JJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0FBZFIiLCJmaWxlIjoic3JjL2FwcC9wYWdlcy9jb250YXRvL2NvbnRhdG8ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3QgeyBcblxuICAgIC5Db250YWluZXIge1xuICAgICAgICBmbGV4OiAxO1xuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIH1cblxuICAgIC5IZWFkZXIge1xuICAgICAgICBwYWRkaW5nOjMwcHg7XG4gICAgICAgIHBhZGRpbmctdG9wOiBjYWxjKGdldFN0YXR1c0JhckhlaWdodCgpICsgMjRweCk7XG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICB9XG5cbiAgICAuSGVhZGVyVGl0bGV7XG4gICAgICAgIGNvbG9yOiAjZjRlZGU5O1xuICAgICAgICBmb250LXNpemU6IDIwcHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyOHB4O1xuICAgIH1cblxuICAgIC5Vc2VyTmFtZXtcbiAgICAgICAgZm9udC1mYW1pbHk6IExhdG8tQm9sZCA7XG4gICAgICAgIGZvbnQtc2l6ZTogMjRweDtcbiAgICB9XG5cbiAgICAuUHJvZmlsZUJ1dHRvbiB7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgIH1cblxuICAgIC5Vc2VyQXZhdGFyIHtcbiAgICAgICAgd2lkdGg6IDU2cHg7XG4gICAgICAgIGhlaWdodDogNTZweDtcbiAgICAgICAgYm9yZGVyLWNvbG9yOiAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAyOHB4O1xuICAgIH1cblxuICAgIC5UZXh0IHtcbiAgICAgICAgZm9udC1mYW1pbHk6IExhdG8tQm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAyNHB4O1xuICAgICAgICBjb2xvcjogIzEyNkRFODtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBtYXJnaW4tdG9wOiAyMHB4O1xuICAgIH1cblxuICAgIC5MaXN0IHtcbiAgICAgICAgcGFkZGluZy10b3A6NTA7XG4gICAgICAgIHBhZGRpbmctbGVmdDoyMDtcbiAgICAgICAgcGFkZGluZy1yaWdodDoyMDtcbiAgICB9XG5cbiAgICAuTGlzdENvbnRhaW5lciB7XG4gICAgICAgIGJhY2tncm91bmQ6ICMxMjZERTg7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gICAgICAgIHBhZGRpbmc6IDIwcHg7XG4gICAgICAgIG1hcmdpbi1ib3R0b206IDE2cHg7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgfVxuXG4gICAgLkxpc3RBdmF0YXIge1xuICAgICAgICB3aWR0aDogNzJweDtcbiAgICAgICAgaGVpZ2h0OiA3MnB4O1xuICAgICAgICBib3JkZXItcmFkaXVzOiAzNnB4O1xuICAgIH1cblxuICAgIC5MaXN0SW5mbyB7XG4gICAgICAgIGZsZXg6MTtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDIwcHg7XG4gICAgfVxuXG4gICAgLkxpc3ROYW1lIHtcbiAgICAgICAgZm9udC1mYW1pbHk6ICdMYXRvLUJvbGQnO1xuICAgICAgICBmb250LXNpemU6IDE4cHg7XG4gICAgICAgIGNvbG9yOiAjZmZmO1xuICAgIH1cblxuICAgIC5MaXN0TWV0YSB7XG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgfVxuXG4gICAgLkxpc3RNZXRhVGV4dCB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA4cHg7XG4gICAgICAgIGNvbG9yOiAjOTk5NTkxO1xuICAgIH1cbn0iXX0= */";
      /***/
    },

    /***/
    "./src/app/pages/contato/contato.page.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/contato/contato.page.ts ***!
      \***********************************************/

    /*! exports provided: ContatoPage */

    /***/
    function srcAppPagesContatoContatoPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "ContatoPage", function () {
        return ContatoPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var ContatoPage = /*#__PURE__*/function () {
        function ContatoPage(router) {
          _classCallCheck(this, ContatoPage);

          this.router = router;
        }

        _createClass(ContatoPage, [{
          key: "teste",
          value: function teste() {
            this.router.navigate(['/agenda']);
          }
        }]);

        return ContatoPage;
      }();

      ContatoPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      ContatoPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./contato.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contato/contato.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./contato.page.scss */
        "./src/app/pages/contato/contato.page.scss"))["default"]]
      })], ContatoPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-contato-contato-module-es5.js.map