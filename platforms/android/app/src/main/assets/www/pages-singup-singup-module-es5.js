(function () {
  function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

  function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

  function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

  (window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-singup-singup-module"], {
    /***/
    "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/singup/singup.page.html":
    /*!*************************************************************************************!*\
      !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/singup/singup.page.html ***!
      \*************************************************************************************/

    /*! exports provided: default */

    /***/
    function node_modulesRawLoaderDistCjsJsSrcAppPagesSingupSingupPageHtml(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = "<ion-content>\n  <div>\n    <img src=\"/sarcex/assets/img/logo.png\" style=\"display: block; margin-left: auto; margin-right: auto; margin-top: 35px;\"/>\n    <div class=\"Title\">Crie sua conta</div>\n\n    <form>\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -3px;\">\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n      </svg>&nbsp;\n      <input name=\"name\" type=\"text\" placeholder=\"Nome Completo\" class=\"InputStyle\"/>\n      </ion-item>\n\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-telephone\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z\"/>\n      </svg>&nbsp;\n      <input name=\"phone\" type=\"phone\" placeholder=\"Telefone\" onkeypress=\"$(this).mask('(00) 90000-0000')\" class=\"InputStyle\"/>\n    </ion-item>\n\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-envelope\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z\"/>\n      </svg>&nbsp;\n      <input name=\"email\" type=\"email\" placeholder=\"E-mail\" class=\"InputStyle\"/>\n    </ion-item>\n\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-lock\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -7px;\">\n        <path fill-rule=\"evenodd\" d=\"M11.5 8h-7a1 1 0 0 0-1 1v5a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V9a1 1 0 0 0-1-1zm-7-1a2 2 0 0 0-2 2v5a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2V9a2 2 0 0 0-2-2h-7zm0-3a3.5 3.5 0 1 1 7 0v3h-1V4a2.5 2.5 0 0 0-5 0v3h-1V4z\"/>\n      </svg>&nbsp;\n      <input  name=\"password\" type=\"password\" placeholder=\"Senha\" class=\"InputStyle\"/>\n    </ion-item>\n    </form>\n\n    <button class=\"Button\" (click)=\"paciente()\"><div class=\"ButtonText\"><p>ENVIAR</p></div></button>\n  </div>\n    <div class=\"BackToSignIn\" (click)=\"login()\">\n      <div class=\"BackToSignInText\">\n        <svg width=\"1.7em\" height=\"1.7em\" viewBox=\"0 0 16 16\" class=\"bi bi-arrow-left-short\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-bottom: -8px;\">\n          <path fill-rule=\"evenodd\" d=\"M12 8a.5.5 0 0 1-.5.5H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5a.5.5 0 0 1 .5.5z\"/>\n        </svg>&nbsp;\n        Voltar para Login\n      </div>\n    </div>\n  \n</ion-content>";
      /***/
    },

    /***/
    "./src/app/pages/singup/singup-routing.module.ts":
    /*!*******************************************************!*\
      !*** ./src/app/pages/singup/singup-routing.module.ts ***!
      \*******************************************************/

    /*! exports provided: SingUpPageRoutingModule */

    /***/
    function srcAppPagesSingupSingupRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SingUpPageRoutingModule", function () {
        return SingUpPageRoutingModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
      /* harmony import */


      var _singup_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! ./singup.page */
      "./src/app/pages/singup/singup.page.ts");

      var routes = [{
        path: '',
        component: _singup_page__WEBPACK_IMPORTED_MODULE_3__["SingUpPage"]
      }];

      var SingUpPageRoutingModule = function SingUpPageRoutingModule() {
        _classCallCheck(this, SingUpPageRoutingModule);
      };

      SingUpPageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
      })], SingUpPageRoutingModule);
      /***/
    },

    /***/
    "./src/app/pages/singup/singup.module.ts":
    /*!***********************************************!*\
      !*** ./src/app/pages/singup/singup.module.ts ***!
      \***********************************************/

    /*! exports provided: SingUpPageModule */

    /***/
    function srcAppPagesSingupSingupModuleTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SingUpPageModule", function () {
        return SingUpPageModule;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/common */
      "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
      /* harmony import */


      var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
      /*! @ionic/angular */
      "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
      /* harmony import */


      var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
      /*! @angular/forms */
      "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
      /* harmony import */


      var _singup_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
      /*! ./singup.page */
      "./src/app/pages/singup/singup.page.ts");
      /* harmony import */


      var _singup_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
      /*! ./singup-routing.module */
      "./src/app/pages/singup/singup-routing.module.ts");

      var SingUpPageModule = function SingUpPageModule() {
        _classCallCheck(this, SingUpPageModule);
      };

      SingUpPageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"], _singup_routing_module__WEBPACK_IMPORTED_MODULE_6__["SingUpPageRoutingModule"]],
        declarations: [_singup_page__WEBPACK_IMPORTED_MODULE_5__["SingUpPage"]]
      })], SingUpPageModule);
      /***/
    },

    /***/
    "./src/app/pages/singup/singup.page.scss":
    /*!***********************************************!*\
      !*** ./src/app/pages/singup/singup.page.scss ***!
      \***********************************************/

    /*! exports provided: default */

    /***/
    function srcAppPagesSingupSingupPageScss(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony default export */


      __webpack_exports__["default"] = ":host ion-content {\n  --background: #126DE8;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px 70px;\n}\n:host .Title {\n  font-size: 24px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Regular\";\n  margin: 5px 0 24px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  padding-left: 5px;\n  padding-bottom: none;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .BackToSignIn {\n  position: absolute;\n  left: 0;\n  bottom: 0;\n  right: 0;\n  background: #fff;\n  border-top-width: 1px;\n  border-color: #232129;\n  padding: 16px 0 25px;\n  justify-content: center;\n  align-items: center;\n  flex-direction: row;\n}\n:host .BackToSignInText {\n  color: #126DE8;\n  font-size: 16px;\n  font-family: \"Lato-Bold\";\n  margin-left: 10px;\n  align-items: center;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvc2luZ3VwL3Npbmd1cC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUk7RUFDSSxxQkFBQTtBQURSO0FBSUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBRlI7QUFNSTtFQUNJLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7QUFKUjtBQU9JO0VBQ0ksT0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxvQkFBQTtBQUxSO0FBUUk7RUFDSSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsMkJBQUE7RUFDQSxrQkFBQTtBQU5SO0FBU0k7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0FBUFI7QUFVSTtFQUNJLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQVJSO0FBV0k7RUFDSSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLG9CQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0FBVFI7QUFZSTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUFWUiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Npbmd1cC9zaW5ndXAucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICAtLWJhY2tncm91bmQ6ICMxMjZERTg7XG4gICAgfVxuXG4gICAgLkJ1dHRvbiB7XG4gICAgICAgIHdpZHRoOiA2MCU7XG4gICAgICAgIGhlaWdodDogNjBweDsgLy81MFxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2RDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogOHB4OyAvLzE1XG4gICAgICAgIGp1c3RpZnktY29udGVudDpjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIH1cbiAgXG4gICAgXG4gICAgLkJ1dHRvblRleHQge1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDsgLy8xM1xuICAgIH1cblxuICAgIC5Db250YWluZXJ7XG4gICAgICAgIGZsZXg6IDE7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgICAgICBwYWRkaW5nOiAwIDMwcHggNzBweDtcbiAgICB9XG5cbiAgICAuVGl0bGUge1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAnTGF0by1SZWd1bGFyJztcbiAgICAgICAgbWFyZ2luOiA1cHggMCAyNHB4O1xuICAgIH1cblxuICAgIC5JbnB1dCB7XG4gICAgICAgIHdpZHRoOiA5MCU7IC8vOTBcbiAgICAgICAgaGVpZ2h0OiAzOHB4OyAvLzQ1XG4gICAgICAgIHBhZGRpbmc6IDAgMTZweDtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiBub25lO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7IC8vNlxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB9XG5cbiAgICAuSW5wdXRTdHlsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7IFxuICAgICAgICBib3JkZXI6IDAgbm9uZTsgXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDA7IFxuICAgICAgICBvdXRsaW5lOiAwO1xuICAgIH1cblxuICAgIC5CYWNrVG9TaWduSW4ge1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGxlZnQ6IDA7XG4gICAgICAgIGJvdHRvbTogMDtcbiAgICAgICAgcmlnaHQ6MDtcbiAgICAgICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICAgICAgYm9yZGVyLXRvcC13aWR0aDogMXB4O1xuICAgICAgICBib3JkZXItY29sb3I6ICMyMzIxMjk7XG4gICAgICAgIHBhZGRpbmc6IDE2cHggMCAyNXB4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICB9XG5cbiAgICAuQmFja1RvU2lnbkluVGV4dCB7IFxuICAgICAgICBjb2xvcjogIzEyNkRFODtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4O1xuICAgICAgICBmb250LWZhbWlseTogJ0xhdG8tQm9sZCc7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgfVxufSJdfQ== */";
      /***/
    },

    /***/
    "./src/app/pages/singup/singup.page.ts":
    /*!*********************************************!*\
      !*** ./src/app/pages/singup/singup.page.ts ***!
      \*********************************************/

    /*! exports provided: SingUpPage */

    /***/
    function srcAppPagesSingupSingupPageTs(module, __webpack_exports__, __webpack_require__) {
      "use strict";

      __webpack_require__.r(__webpack_exports__);
      /* harmony export (binding) */


      __webpack_require__.d(__webpack_exports__, "SingUpPage", function () {
        return SingUpPage;
      });
      /* harmony import */


      var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
      /*! tslib */
      "./node_modules/tslib/tslib.es6.js");
      /* harmony import */


      var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
      /*! @angular/core */
      "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
      /* harmony import */


      var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
      /*! @angular/router */
      "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");

      var SingUpPage = /*#__PURE__*/function () {
        function SingUpPage(router) {
          _classCallCheck(this, SingUpPage);

          this.router = router;
        }

        _createClass(SingUpPage, [{
          key: "paciente",
          value: function paciente() {
            this.router.navigate(['/paciente']);
          }
        }, {
          key: "login",
          value: function login() {
            this.router.navigate(['/home']);
          }
        }]);

        return SingUpPage;
      }();

      SingUpPage.ctorParameters = function () {
        return [{
          type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]
        }];
      };

      SingUpPage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! raw-loader!./singup.page.html */
        "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/singup/singup.page.html"))["default"],
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(
        /*! ./singup.page.scss */
        "./src/app/pages/singup/singup.page.scss"))["default"]]
      })], SingUpPage);
      /***/
    }
  }]);
})();
//# sourceMappingURL=pages-singup-singup-module-es5.js.map