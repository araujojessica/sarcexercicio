(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-paciente-paciente-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/paciente/paciente.page.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/paciente/paciente.page.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-content>\n  <div>\n    <img src=\"/sarcex/assets/img/logo.png\" style=\"display: block; margin-left: auto; margin-right: auto; margin-top: 35px;\"/>\n    <div class=\"Title\">Vamos cadastrar o paciente.</div>\n\n    <form>\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-person\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -3px;\">\n        <path fill-rule=\"evenodd\" d=\"M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z\"/>\n      </svg>&nbsp;\n      <input name=\"name\" type=\"text\" placeholder=\"Nome Completo\" class=\"InputStyle\"/>\n      </ion-item>\n\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-telephone\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color: #126DE8; margin-top: -4px;\">\n        <path fill-rule=\"evenodd\" d=\"M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z\"/>\n      </svg>&nbsp;\n      <input name=\"phone\" type=\"phone\" placeholder=\"Telefone\" onkeypress=\"$(this).mask('(00) 90000-0000')\" class=\"InputStyle\"/>\n    </ion-item>\n\n    <ion-item class=\"Input\" style=\"padding: 0px;\">\n      <svg width=\"1.3em\" height=\"1.3em\" viewBox=\"0 0 16 16\" class=\"bi bi-calendar-event\" fill=\"currentColor\" xmlns=\"http://www.w3.org/2000/svg\" style=\"color:#126DE8; margin-top: -5px; \">\n        <path fill-rule=\"evenodd\" d=\"M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z\"/>\n        <path d=\"M11 6.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5v-1z\"/>\n      </svg>&nbsp;\n      <input name=\"date\" type=\"text\" placeholder=\"Idade ou data de nascimento\" onkeypress=\"$(this).mask('00/00/0000')\" class=\"InputStyleS\"/>\n      </ion-item>\n    </form>\n\n      <div class=\"Conteudo\"><p>Todo paciente Novo tem que passar pelo teste Sarcopenia.</p></div>\n      <button class=\"Button\" (click)=\"info()\"><p class=\"ButtonText\">Teste para diagnóstico</p></button>\n  </div>\n</ion-content>");

/***/ }),

/***/ "./src/app/pages/paciente/paciente-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/paciente/paciente-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: PacientePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacientePageRoutingModule", function() { return PacientePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");
/* harmony import */ var _paciente_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./paciente.page */ "./src/app/pages/paciente/paciente.page.ts");




const routes = [
    {
        path: '',
        component: _paciente_page__WEBPACK_IMPORTED_MODULE_3__["PacientePage"],
    }
];
let PacientePageRoutingModule = class PacientePageRoutingModule {
};
PacientePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PacientePageRoutingModule);



/***/ }),

/***/ "./src/app/pages/paciente/paciente.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/paciente/paciente.module.ts ***!
  \***************************************************/
/*! exports provided: PacientePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacientePageModule", function() { return PacientePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/__ivy_ngcc__/fesm2015/ionic-angular.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/__ivy_ngcc__/fesm2015/forms.js");
/* harmony import */ var _paciente_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./paciente.page */ "./src/app/pages/paciente/paciente.page.ts");
/* harmony import */ var _paciente_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./paciente-routing.module */ "./src/app/pages/paciente/paciente-routing.module.ts");







let PacientePageModule = class PacientePageModule {
};
PacientePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _paciente_routing_module__WEBPACK_IMPORTED_MODULE_6__["PacientePageRoutingModule"]
        ],
        declarations: [_paciente_page__WEBPACK_IMPORTED_MODULE_5__["PacientePage"]]
    })
], PacientePageModule);



/***/ }),

/***/ "./src/app/pages/paciente/paciente.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/paciente/paciente.page.scss ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (":host ion-content {\n  --background: #126DE8;\n}\n:host .Container {\n  flex: 1;\n  align-items: center;\n  justify-content: center;\n  padding: 0 30px;\n}\n:host .Title {\n  font-size: 24px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Regular\";\n  margin: 10px 0 10px;\n}\n:host .Conteudo {\n  font-size: 18px;\n  text-align: center;\n  color: #f4ede8;\n  font-family: \"Lato-Bold\";\n  margin: 10px 0 10px;\n}\n:host .Button {\n  width: 60%;\n  height: 60px;\n  background: #126D;\n  border-radius: 10px;\n  margin-top: 8px;\n  justify-content: center;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .ButtonText {\n  font-family: \"Lato-Bold\";\n  color: #fff;\n  font-size: 16px;\n}\n:host .Input {\n  width: 90%;\n  height: 38px;\n  padding: 0 16px;\n  background: #fff;\n  border-radius: 8px;\n  margin-bottom: 10px;\n  flex-direction: row;\n  align-items: center;\n  display: block;\n  margin-left: auto;\n  margin-right: auto;\n}\n:host .InputStyle {\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n:host .InputStyleS {\n  margin-left: 10px;\n  width: 115%;\n  font-size: large;\n  border: 0 none;\n  box-shadow: 0 0 0 0;\n  outline: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcGFjaWVudGUvcGFjaWVudGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVJO0VBQ0kscUJBQUE7QUFEUjtBQUlJO0VBQ0ksT0FBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxlQUFBO0FBRlI7QUFLSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBSFI7QUFNSTtFQUNJLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSx3QkFBQTtFQUNBLG1CQUFBO0FBSlI7QUFPSTtFQUNJLFVBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFMUjtBQVNJO0VBQ0ksd0JBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQVBSO0FBVUk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFSUjtBQVdJO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0FBVFI7QUFZSTtFQUNJLGlCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtBQVZSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGFjaWVudGUvcGFjaWVudGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiOmhvc3Qge1xuXG4gICAgaW9uLWNvbnRlbnQge1xuICAgICAgICAtLWJhY2tncm91bmQ6ICMxMjZERTg7XG4gICAgfVxuXG4gICAgLkNvbnRhaW5lcntcbiAgICAgICAgZmxleDogMTtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIHBhZGRpbmc6IDAgMzBweDtcbiAgICB9XG5cbiAgICAuVGl0bGUge1xuICAgICAgICBmb250LXNpemU6IDI0cHg7XG4gICAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgICAgY29sb3I6ICNmNGVkZTg7XG4gICAgICAgIGZvbnQtZmFtaWx5OidMYXRvLVJlZ3VsYXInO1xuICAgICAgICBtYXJnaW46IDEwcHggMCAxMHB4O1xuICAgIH1cblxuICAgIC5Db250ZXVkbyB7XG4gICAgICAgIGZvbnQtc2l6ZTogMThweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBjb2xvcjogI2Y0ZWRlODtcbiAgICAgICAgZm9udC1mYW1pbHk6J0xhdG8tQm9sZCc7XG4gICAgICAgIG1hcmdpbjogMTBweCAwIDEwcHg7XG4gICAgfVxuXG4gICAgLkJ1dHRvbiB7XG4gICAgICAgIHdpZHRoOiA2MCU7XG4gICAgICAgIGhlaWdodDogNjBweDsgLy81MFxuICAgICAgICBiYWNrZ3JvdW5kOiAjMTI2RDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICAgICAgbWFyZ2luLXRvcDogOHB4OyAvLzE1XG4gICAgICAgIGp1c3RpZnktY29udGVudDpjZW50ZXI7XG4gICAgICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgICBtYXJnaW4tbGVmdDogYXV0bztcbiAgICAgICAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAgIH1cbiAgXG4gICAgXG4gICAgLkJ1dHRvblRleHQge1xuICAgICAgICBmb250LWZhbWlseTonTGF0by1Cb2xkJztcbiAgICAgICAgY29sb3I6ICNmZmY7XG4gICAgICAgIGZvbnQtc2l6ZTogMTZweDsgLy8xM1xuICAgIH1cbiAgXG4gICAgLklucHV0IHtcbiAgICAgICAgd2lkdGg6IDkwJTsgLy85MFxuICAgICAgICBoZWlnaHQ6IDM4cHg7IC8vNDVcbiAgICAgICAgcGFkZGluZzogMCAxNnB4O1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiA4cHg7IC8vNlxuICAgICAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBkaXNwbGF5OiBibG9jaztcbiAgICAgICAgbWFyZ2luLWxlZnQ6IGF1dG87XG4gICAgICAgIG1hcmdpbi1yaWdodDogYXV0bztcbiAgICB9XG5cbiAgICAuSW5wdXRTdHlsZSB7XG4gICAgICAgIGZvbnQtc2l6ZTogbGFyZ2U7IFxuICAgICAgICBib3JkZXI6IDAgbm9uZTsgXG4gICAgICAgIGJveC1zaGFkb3c6IDAgMCAwIDA7IFxuICAgICAgICBvdXRsaW5lOiAwO1xuICAgIH1cblxuICAgIC5JbnB1dFN0eWxlUyB7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiAxMHB4O1xuICAgICAgICB3aWR0aDogMTE1JTtcbiAgICAgICAgZm9udC1zaXplOiBsYXJnZTsgXG4gICAgICAgIGJvcmRlcjogMCBub25lOyBcbiAgICAgICAgYm94LXNoYWRvdzogMCAwIDAgMDsgXG4gICAgICAgIG91dGxpbmU6IDA7XG4gICAgfVxufVxuIl19 */");

/***/ }),

/***/ "./src/app/pages/paciente/paciente.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/paciente/paciente.page.ts ***!
  \*************************************************/
/*! exports provided: PacientePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PacientePage", function() { return PacientePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/__ivy_ngcc__/fesm2015/router.js");



let PacientePage = class PacientePage {
    constructor(router) {
        this.router = router;
    }
    info() {
        this.router.navigate(['/info']);
    }
};
PacientePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
PacientePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./paciente.page.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/paciente/paciente.page.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./paciente.page.scss */ "./src/app/pages/paciente/paciente.page.scss")).default]
    })
], PacientePage);



/***/ })

}]);
//# sourceMappingURL=pages-paciente-paciente-module-es2015.js.map