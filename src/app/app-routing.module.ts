import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule) },
  { path: 'agenda', loadChildren: () => import('./pages/agenda/agenda.module').then( m => m.AgendaPageModule) },
  { path: 'contato/:sus', loadChildren: () => import('./pages/contato/contato.module').then( m => m.ContatoPageModule) },
  { path: 'agendamento/:sus', loadChildren: () => import('./pages/agendamento/agendamento.module').then( m => m.AgendamentoPageModule) },
  { path: 'agendamento_cliente', loadChildren: () => import('./pages/agendamento_cliente/agendamento_cliente.module').then( m => m.AgendamentoClientePageModule) },
  { path: 'info/:sus', loadChildren: () => import('./pages/info/info.module').then( m => m.InfoPageModule) },
  { path: 'consulta/:sus', loadChildren: () => import('./pages/consulta/consulta.module').then( m => m.ConsultaPageModule) },
  { path: 'paciente', loadChildren: () => import('./pages/paciente/paciente.module').then( m => m.PacientePageModule) },
  { path: 'singup', loadChildren: () => import('./pages/singup/singup.module').then( m => m.SingUpPageModule) },
  { path: 'teste/:sus', loadChildren: () => import('./pages/teste/teste.module').then( m => m.TestePageModule) },
  { path: 'teste2/:sus', loadChildren: () => import('./pages/teste2/teste2.module').then( m => m.Teste2PageModule) },
  { path: 'teste3/:sus', loadChildren: () => import('./pages/teste3/teste3.module').then( m => m.Teste3PageModule) },
  { path: 'teste4/:sus', loadChildren: () => import('./pages/teste4/teste4.module').then( m => m.Teste4PageModule) },
  { path: 'teste5/:sus', loadChildren: () => import('./pages/teste5/teste5.module').then( m => m.Teste5PageModule) },
  { path: 'referencia', loadChildren: () => import('./pages/referencia/referencia.module').then( m => m.ReferenciaPageModule) },
  { path: 'bloco_notas/:sus', loadChildren: () => import('./pages/bloco_notas/bloco_notas.module').then( m => m.BlocoNotasPageModule) },
  { path: 'editar-perfil', loadChildren: () => import('./pages/editar-perfil/editar-perfil.module').then( m => m.EditarPerfilPageModule) },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
