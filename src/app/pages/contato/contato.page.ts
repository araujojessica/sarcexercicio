import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ConsultaService } from '../consulta/consultaService';
import { Paciente } from '../paciente/paciente';
//import { ContatoPaciente } from '../consulta/consultaPaciente';
import { ContatoService } from './contatoService';

@Component({
  selector: 'app-home',
  templateUrl: 'contato.page.html',
  styleUrls: ['contato.page.scss'],
})
export class ContatoPage implements OnInit{

  customAlertOptions: any = {
    header: 'Exercícios MMSS',
    cssClass: 'checkbox',
    translucent: true
  };

  sus:string;
  nomePaciente:string;
  consulta: Paciente[] = [];
  ex:string = '';
  ex1:string = '';
  ex2:string = '';
  ex3:string = '';
  exMMII:string = '';
  exMMII1:string = '';
  exMMII2:string = '';
  excore:string = '';
  primeiro:boolean;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private contatoService: ConsultaService,
    private c: ContatoService
  ) {}

  async ngOnInit(): Promise<void> {
    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de contato : ' + parametros['sus'] );
    })

       const toast = await (await this.toastCtrl.create({
      message: '<b>AVISO!</b><br> É importante a realização de uma avaliação muscular criteriosa individualizada para poder prescrever o tratamento adequado obtendo assim sucesso na resposta às condutas prescritas. Sugestões de Exercícios Obs: De acordo com a avaliação do Profissional de Educação Física utilizar a melhor estrategia para seu paciente.',
      duration: 5000, position: 'middle',
      color: 'danger'
    })).present();
  }

  toggleMMSS() {
    if (document.getElementById("MMSS").style.display == 'none') {
     document.getElementById("MMSS").style.display = "block";
    } else {
     document.getElementById("MMSS").style.display = "none";
    }
  }

  toggleMMII() {
    if (document.getElementById("MMII").style.display == 'none') {
     document.getElementById("MMII").style.display = "block";
    } else {
     document.getElementById("MMII").style.display = "none";
    }
  }

  toggleCore() {
    if (document.getElementById("Core").style.display == 'none') {
     document.getElementById("Core").style.display = "block";
    } else {
     document.getElementById("Core").style.display = "none";
    }
  }

  teste(){
    this.router.navigate(['/agenda']);
  }

  blocoNotas () {
    this.router.navigate(['/bloco_notas/'+this.sus+'']);
  }

  agendamento () {
    this.router.navigate(['/agendamento/'+this.sus+'']);
  }

  exercicio(event) {
    this.ex = event.target.value;
    
   }
   exercicio1(event) {
    this.ex1 = event.target.value;
    
   }

   exercicio2(event) {
    this.ex2 = event.target.value;
    
   }
   exercicio3(event) {
    this.ex3 = event.target.value;
    
   }

   exercicioMMII(event) {
    this.exMMII = event.target.value;
    
   }
   exercicioMMII1(event) {
    this.exMMII1 = event.target.value;
    
   }
   exercicioMMII2(event) {
    this.exMMII2 = event.target.value;
    
   }
   exerciciocore(event) {
    this.excore = event.target.value;
    
   }

   salvarEX(){


    let exercicio_MMSS = this.ex + '#' + this.ex1 + '#' + this.ex2 + '#' + this.ex3;
    let exercicio_MMII = this.exMMII + '#' + this.exMMII1 + '#' + this.exMMII2;
    let exercicio_core = this.excore;

    this.c.signup(exercicio_MMSS,exercicio_MMII,exercicio_core,this.sus).subscribe(
      async () => {
        const toast = await (await this.toastCtrl.create({
          message: 'Exercícios cadastrados com sucesso '+this.sus+'!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        var sus = this.sus;
        this.router.navigate(['/contato/'+sus+''])
      },  
      async err => {
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor contate o adm do app',
          duration: 8000, position: 'top',
          color: 'danger'
        })).present();
      }
    );

   
   
   }

}
