import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Paciente } from "../paciente/paciente";
import { Exercicio } from "./exercicio";

//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class ContatoService{

    constructor(private http: HttpClient){}

    findPaciente(pacienteSus: string) {
        return this.http.get<Paciente[]>(API_URL + '/paciente/'+pacienteSus+'');
     } 

     signup(exercicio_MMSS:string,exercicio_MMII:string,exercicio_core:string,paciente_sus:string){
        return this.http.post(API_URL +  '/exercicio/signup', {exercicio_MMSS,exercicio_MMII,exercicio_core,paciente_sus});
    }

}

   