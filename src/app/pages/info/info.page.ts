import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'info.page.html',
  styleUrls: ['info.page.scss'],
})
export class InfoPage implements OnInit{
  sus:string;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de info : ' + parametros['sus'] );
    })
  }

  teste(){
    console.log('enviando para teste')
    this.router.navigate(['/teste/'+this.sus+'']);
  }

}
