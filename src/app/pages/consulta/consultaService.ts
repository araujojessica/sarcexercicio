import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/core/user/user";
import { Paciente } from "../paciente/paciente";
import { ConsultaPaciente } from "./consultaPaciente";


//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class ConsultaService{

    constructor(private http: HttpClient){}

    findPaciente2(pacienteSus: string) {
        return this.http.get<ConsultaPaciente>(API_URL + '/paciente/consulta/'+pacienteSus+'');
     } 
}

   