import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConsultaPage } from './consulta.page';
import { ConsultaPageRoutingModule } from './consulta-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsultaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ConsultaPage]
})
export class ConsultaPageModule {}
