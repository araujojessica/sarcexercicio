export interface ConsultaPaciente {

    questionario_id: string;
    doenca: string;
    medicamentos: string;
    tabagismo: string;
    atividade: string;
    paciente_id: string;
    questionario_join_date: string;
    paciente_sus: string;
    paciente_name: string;
    paciente_telefone: string;
    paciente_data_nascimento: string;
    paciente_join_date: string;
    bioimpedancia_id: string;
    gordura: string;
    massa_magra: string;
    bioimpedancia_join_date:string;
    antropometrica_id: string;
    antropometrica_resultado: string;
    antropometrica_join_date: string;
    timeup_id: string;
    timeup_resultado: string;
    timeup_join_date: string;
    caminhada_id: string;
    caminhada_resultado: string;
    caminhada_join_date: string;
    anotacao_id:string;
    anotacao: string;
    anotacao_join_date: string;
    exercicio_id: string;
    exercicio_MMSS: string;
    exercicio_MMII: string;
    exercicio_core: string;
    exercicio_join_date: string;
}