import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Paciente } from '../paciente/paciente';
import { ConsultaPaciente } from './consultaPaciente';
import { ConsultaService } from './consultaService';

@Component({
  selector: 'app-home',
  templateUrl: 'consulta.page.html',
  styleUrls: ['consulta.page.scss'],
})
export class ConsultaPage implements OnInit{

  sus:string;
  ex_01:string;
  ex_02:string;
  ex_03:string;
  ex_04:string;
  ex_mmii:string;
  ex_mmii_02:string;
  ex_mmii_03:string;
  ex_core:string;
  doenca:string;
  medicamentos:string;
  tabagismo:string;
  atividade:string;
  massa_magra:string;
  gordura:string;
  consulta: ConsultaPaciente;
  antropometrica_resultado:string;
  timeup_resultado: string;
  caminhada_resultado:string;
  anotacoes:string;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private consultaService: ConsultaService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de contato : ' + parametros['sus'] );
    })
 

    this.consultaService.findPaciente2(this.sus).subscribe(consulta => {
      this.consulta = consulta;
      
      this.doenca = consulta.doenca;
      this.medicamentos = consulta.medicamentos;
      this.tabagismo = consulta.tabagismo;
      this.atividade = consulta.atividade;
      this.massa_magra = consulta.massa_magra;
      this.gordura = consulta.gordura;
      this.antropometrica_resultado = consulta.antropometrica_resultado;
      this.timeup_resultado = consulta.timeup_resultado;
      this.caminhada_resultado = consulta.caminhada_resultado;
      this.anotacoes = consulta.anotacao;
    var s = consulta.exercicio_MMSS.split('#');
       
        if(s[0] == 'undefined'){
          this.ex_01 = '';
        }else{
          this.ex_01 = '. ' + s[0] ;
        }
        if(s[1] == 'undefined'){
          this.ex_02 = '';
        }else{
          this.ex_02 = '. ' + s[1];
        }
        if(s[2] == 'undefined'){
          this.ex_03 = '';
        }else{
          this.ex_03 = '. ' + s[2];
        }
        if(s[3] == 'undefined'){
          this.ex_04 = '';
        }else{
          this.ex_04 = '. ' + s[3];
        }
        
        var n = consulta.exercicio_MMII.split('#');
        if( n[0] == 'undefined'){
          this.ex_mmii= '';
        }else{
          this.ex_mmii= '. ' +  n[0];
        }
        if( n[1] == 'undefined'){
          this.ex_mmii_02= '';
        }else{
          this.ex_mmii_02= '. ' + n[1];
        }
        if( n[2] == 'undefined'){
          this.ex_mmii_03= '';
        }else{
          this.ex_mmii_03= '. ' + n[2];
        }
        
        if(consulta.exercicio_core == undefined){
          this.ex_core = '';
        }else {
          this.ex_core = '. ' + consulta.exercicio_core;
        }
     
       
      
  });

  }
}
