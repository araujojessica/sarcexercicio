import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, MenuController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { AgendaService } from '../agenda/agendaService';
import { Foto } from '../agenda/foto';
import { NewPaciente } from '../paciente/newPaciente';
import { RegisterService } from '../singup/register.service';
import { EditarPerfilService } from './editar-perfilService';
import { NewProfissional } from './NewProfissional';
import { Profissional } from './profissional';

@Component({
  selector: 'app-editar-perfil',
  templateUrl: './editar-perfil.page.html',
  styleUrls: ['./editar-perfil.page.scss'],
})
export class EditarPerfilPage implements OnInit {
  tipo: boolean;
  user$: Observable<User>;
  user: User;
  public editForm: FormGroup;
  profissional: Profissional;
  fullName:string;
  telefone:string;
  email: string;
  foto: Foto;
  urlFoto: string;
  file: File;
  
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    private userService: UserService,
    public toastCtrl: ToastController,
    private plt: Platform,
    private formBuilder: FormBuilder,
    private editarService: EditarPerfilService,
    private agendaService: AgendaService,
    private signupService: RegisterService
  ) {

    this.user$ = userService.getUser();

    this.usuario();


   
  }

  usuario(){
    this.user$.subscribe(user => {
      console.log('user : ' + user.email)
      this.user = user
      this.email = ''+user.email;
      this.editarService.findProfissional(''+user.email).subscribe(p =>{
        this.profissional = p;
        this.fullName = this.profissional.fullName;
        this.telefone = this.profissional.telefone;
})

  this.agendaService.findFotoPaciente(this.email).subscribe(foto => {
    this.foto = foto;
    this.urlFoto = this.foto.url;

})


});
  }
  ngOnInit() {

    this.editForm = this.formBuilder.group({

      fullName: ['', [ ] ],
      telefone: ['', [ ] ], 
      password: ['',[]]
});


  }

  salvar(){

    const newProfissional  = this.editForm.getRawValue() as NewProfissional;
    
    if(newProfissional.fullName != ''){
      this.editarService.editNome(newProfissional.fullName, this.email).subscribe( async () => {
         const toast = await (await this.toastCtrl.create({
          message: 'Usuário editado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/editar-perfil'])
        
      
      },  
      async err => {
        const toast = await (await this.toastCtrl.create({
          message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      });

    }
    if(newProfissional.password != ''){
      this.editarService.editPassword(newProfissional.password, this.email).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Usuário editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       this.router.navigate(['/editar-perfil'])
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }
    if(newProfissional.telefone != '') {
      this.editarService.editTelefone(newProfissional.telefone, this.email).subscribe( async () => {
        const toast = await (await this.toastCtrl.create({
         message: 'Usuário editado com sucesso!',
         duration: 4000, position: 'top',
         color: 'success'
       })).present();
       this.router.navigate(['/editar-perfil'])
       
     
     },  
     async err => {
       const toast = await (await this.toastCtrl.create({
         message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
         duration: 4000, position: 'top',
         color: 'danger'
       })).present();
     });
    }

  }

  salvarFoto(){
    console.log(this.file)
    let description = this.email;
    let allowComments = true;

    this.signupService.updateFoto(description,allowComments, this.file).subscribe(async () => {
      const toast = await (await this.toastCtrl.create({
       message: 'Foto editada com sucesso!',
       duration: 4000, position: 'top',
       color: 'success'
     })).present();
     this.router.navigate(['/editar-perfil'])
     
   
   },  
   async err => {
     const toast = await (await this.toastCtrl.create({
       message: 'Erro ao editar usuário, por favor verifique sua conexão com a internet!',
       duration: 4000, position: 'top',
       color: 'danger'
     })).present();
   });

   
  }

  exibirOcultar(){
    this.tipo = !this.tipo;
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.menuCtrl.close();
    this.usuario();
  }
  ap(){

    if (document.getElementById("ap").style.display == 'none') {
      document.getElementById("ap").style.display = "block";
    } else {
      document.getElementById("ap").style.display = "none";
    }
  }

}
