import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { PacienteAgenda } from "../agendamento_cliente/pacienteAgenda";
import { Profissional } from "./profissional";


//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class EditarPerfilService{

    constructor(private http: HttpClient){}
  
    findProfissional(email: string){

        return this.http.get<Profissional>(API_URL +  '/user/findUser/'+email+'');
    }

    editNome(nome: string, email: string){

        return this.http.get(API_URL +  '/user/editNome/'+nome+'/'+email+'');
    }

    editPassword(password: string, email:string){

        return this.http.get(API_URL +  '/user/editPassword/'+password+'/'+email+'');
    }

    editTelefone(telefone: string, email:string){

        return this.http.get(API_URL +  '/user/editTelefone/'+telefone+'/'+email+'');
    }

    editFoto(description: string, allowComments: boolean, file: File) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);           
            
        return this.http.post(API_URL + '/photos/update', formData);
        }
      }    

    
    

  
     
}

   