import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, MenuController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { AgendamentoClienteService } from './agendamento_clienteService';
import { PacienteAgenda } from './pacienteAgenda';


@Component({
  selector: 'app-Agendamento',
  templateUrl: 'agendamento_cliente.page.html',
  styleUrls: ['agendamento_cliente.page.scss'],
})
export class AgendamentoClientePage implements OnInit {

  user$: Observable<User>;
  paciente: PacienteAgenda[] = [];

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    private userService: UserService,
    public toastCtrl: ToastController,
    private plt: Platform,
    private route: ActivatedRoute,
    private pacienteService: AgendamentoClienteService
  ) {
    this.user$ = userService.getUser();

  }
  pacienteAgenda: string;

 

  ngOnInit(): void {
        this.pacienteService.findAllPacientes().subscribe(paciente => {
          console.log('pacientes' + paciente[0].sus)
          this.paciente = paciente
        });
 
          
  }

  pAgenda(){
    this.router.navigate(['/contato/'+this.pacienteAgenda+''])
   }

 ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.menuCtrl.close();
  }

  contato(sus:string){
    
    
  }
}