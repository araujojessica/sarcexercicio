import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AgendamentoClientePage } from './agendamento_cliente.page';

const routes: Routes = [
  {
    path: '',
    component: AgendamentoClientePage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgendamentoClientePageRoutingModule {}
