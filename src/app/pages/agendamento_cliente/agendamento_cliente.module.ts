import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AgendamentoClientePageRoutingModule } from './agendamento_cliente-routing.module';
import { AgendamentoClientePage } from './agendamento_cliente.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgendamentoClientePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AgendamentoClientePage]
})
export class AgendamentoClientePageModule {}
