import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/core/user/user";
import { PacienteAgenda } from "./pacienteAgenda";

//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class AgendamentoClienteService{

    constructor(private http: HttpClient){}
  
    findAllPacientes(){ 
        return this.http.get<PacienteAgenda[]>(API_URL +  '/paciente/users');
    }
}

   