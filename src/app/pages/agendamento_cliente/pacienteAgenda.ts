import { NumericValueAccessor } from "@ionic/angular";

export interface PacienteAgenda {
    id:number;
    sus: string;
    name:string;
    telefone:string;
    nascimento:string;
    hora: string;
    semana:string;
}