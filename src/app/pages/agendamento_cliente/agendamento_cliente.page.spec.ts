import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { AgendamentoClientePageRoutingModule } from './agendamento_cliente-routing.module';

import { AgendamentoClientePage } from './agendamento_cliente.page';

describe('AgendamentoPage', () => {
  let component: AgendamentoClientePage;
  let fixture: ComponentFixture<AgendamentoClientePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgendamentoClientePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AgendamentoClientePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
