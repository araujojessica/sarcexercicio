import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReferenciaPage } from './referencia.page';

const routes: Routes = [
  {
    path: '',
    component: ReferenciaPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReferenciaPageRoutingModule {}
