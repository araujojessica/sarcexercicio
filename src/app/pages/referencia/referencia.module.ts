import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { ReferenciaPage } from './referencia.page';

import { ReferenciaPageRoutingModule } from './referencia-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReferenciaPageRoutingModule
  ],
  declarations: [ReferenciaPage]
})
export class ReferenciaPageModule {}
