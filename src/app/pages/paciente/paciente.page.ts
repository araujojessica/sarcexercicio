import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { RegisterService } from '../singup/register.service';
import { NewPaciente } from './newPaciente';
import { PacienteNotTakenValidatorService } from './pacienteNotTakenValidatorService';
import { PacienteService } from './pacienteService';

@Component({
  selector: 'app-home',
  templateUrl: 'paciente.page.html',
  styleUrls: ['paciente.page.scss'],
})
export class PacientePage implements OnInit {
  public pacienteForm: FormGroup;
   constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private router: Router,
    private signupService : PacienteService,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder,
    private pacienteNotTakenValidatorService: PacienteNotTakenValidatorService
  ) {}

  ngOnInit(): void {
    this.pacienteForm = this.formBuilder.group({

      telefone: ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(15)
          ]
      ],
      name: ['',
          [
              Validators.required,
              Validators.minLength(2),
              Validators.maxLength(40)
          ]
      ], 

      sus: ['',
          [
              Validators.required,
              Validators.minLength(2),
              Validators.maxLength(40)
          ]//,this.pacienteNotTakenValidatorService.checkUserNameTaken()
      ],                  
      nascimento: ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
      ]
});
  }

  info(){
    const newPaciente  = this.pacienteForm.getRawValue() as NewPaciente;
      
    this.signupService.signup(newPaciente).subscribe(
        
      async () => {

        console.log(newPaciente.sus)
        const toast = await (await this.toastCtrl.create({
          message: 'Paciente cadastrado com sucesso '+newPaciente.sus+'!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        var sus = newPaciente.sus;
        this.router.navigate(['/info/'+sus+''])
      },  
      async err => {
        this.router.navigate(['/home'])
        const toast = await (await this.toastCtrl.create({
          message: 'Paciente já cadastrado. Por favor faça seu login e siga os procedimentos necessários!',
          duration: 8000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
  }

}
