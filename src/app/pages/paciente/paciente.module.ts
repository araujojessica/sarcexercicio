import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PacientePage } from './paciente.page';

import { PacientePageRoutingModule } from './paciente-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PacientePageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PacientePage]
})
export class PacientePageModule {}
