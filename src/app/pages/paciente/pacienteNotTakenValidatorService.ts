import { Injectable } from "@angular/core";

import { AbstractControl } from "@angular/forms";
import {debounceTime, switchMap, map, first} from 'rxjs/operators';
import { PacienteService } from "./pacienteService";
@Injectable({providedIn: 'root' })
export class PacienteNotTakenValidatorService {

    constructor(private signUpService: PacienteService) {}
    
    checkUserNameTaken() {

       
        return (control: AbstractControl) => {
            console.log('entrou no taken' + control.valueChanges)
            return control.valueChanges.pipe(
                debounceTime(300)
            ).pipe(
                switchMap( userName => this.signUpService.checkUserNameTaken(userName))
            ).pipe(
                map( isTaken => isTaken ? {pacienteNameTaken: true} : null)
            ).pipe(
                first()
            );
        };
    }
}
