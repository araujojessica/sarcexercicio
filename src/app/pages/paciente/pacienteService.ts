import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NewPaciente } from "./newPaciente";
import { User } from "src/app/core/user/user";
import { Paciente } from './paciente';


//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class PacienteService{

    constructor(private http: HttpClient){}
   checkUserNameTaken(pacienteSus: string) {
       console.log('dentro do taken: ' + pacienteSus)
       return this.http.get(API_URL + '/paciente/exists/' + pacienteSus);
    } 

    signup(newPaciente : NewPaciente){
        return this.http.post(API_URL +  '/paciente/signup', newPaciente);
    }

    getAll() {
        return this.http.get<Paciente[]>(`/pacientes`);
    }

    getById(id: number) {
        return this.http.get(`/paciente/` + id);
    }

    register(paciente: NewPaciente) {
       
        return this.http.post(`/paciente/register`, paciente);
    }

    update(paciente: Paciente) {
        return this.http.put(`/paciente/` + paciente.id, paciente);
    }

    delete(id: number) {
        return this.http.delete(`/paciente/` + id);
    }


}