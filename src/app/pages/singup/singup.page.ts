import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { NewUser } from './newUser';
import { RegisterService } from './register.service';
import { UserNotTakenValidatorService } from './user-not-taken.validator.service';

@Component({
  selector: 'app-home',
  templateUrl: 'singup.page.html',
  styleUrls: ['singup.page.scss'],
})
export class SingUpPage implements OnInit {
  tipo: boolean;
  public onRegisterForm: FormGroup;
  file: File;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public loadingCtrl: LoadingController,
    private router: Router,
    private userNotTakenValidatorService : UserNotTakenValidatorService,
    private signupService : RegisterService,
    public toastCtrl: ToastController,
    private formBuilder: FormBuilder
  ) {}


  ngOnInit(): void {
    this.onRegisterForm = this.formBuilder.group({

      fullName: ['',
          [
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(40)
          ]
      ],
      userName: ['',
          [
              Validators.required,
              Validators.email
          ],this.userNotTakenValidatorService.checkUserNameTaken()
      ],           
      telefone: ['',[Validators.required]
      ],
      password: ['',
          [
              Validators.required,
              Validators.minLength(1),
              Validators.maxLength(14)
          ]
      ],
      file: ['', Validators.required],
    });
}

  paciente(){

    const newUser  = this.onRegisterForm.getRawValue() as NewUser;
    newUser.email = newUser.userName;
    
    console.log( 'novo usuario : ' + newUser.fullName)

    this.signupService.signup(newUser).subscribe(
        
      async () => {

        
        const description = newUser.email;
        const allowComments = true;
        
        this.signupService.upload(description,allowComments, this.file).subscribe(() => this.router.navigate(['paciente']));
        
        const toast = await (await this.toastCtrl.create({
          message: 'Usuário cadastrado com sucesso! Agora é só fazer o login!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/paciente'])
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Por favor confira se a sua internet está funcionando e tente cadastrar novamente! SarcEx agradece!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );

  
  }

  login(){
    this.router.navigate(['/home']);
  }

  exibirOcultar(){
    this.tipo = !this.tipo;
  }
}
