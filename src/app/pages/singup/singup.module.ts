import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SingUpPage } from './singup.page';

import { SingUpPageRoutingModule } from './singup-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SingUpPageRoutingModule,
    ReactiveFormsModule
    
  ],
  declarations: [SingUpPage]
})
export class SingUpPageModule {}
