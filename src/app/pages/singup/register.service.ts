import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { NewUser } from "./newUser";
import { User } from "src/app/core/user/user";


//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class RegisterService{

    constructor(private http: HttpClient){}
   checkUserNameTaken(userName: string) {
       return this.http.get(API_URL + '/user/exists/' + userName);
    } 

    signup(newUser : NewUser){
        return this.http.post(API_URL +  '/user/signup', newUser);
    }

    getAll() {
        return this.http.get<User[]>(`/users`);
    }

    getById(id: number) {
        return this.http.get(`/users/` + id);
    }

    register(user: NewUser) {
        console.log('entrou no registro')
        return this.http.post(`/users/register`, user);
    }

    update(user: User) {
        return this.http.put(`/users/` + user.id, user);
    }

    delete(id: number) {
        return this.http.delete(`/users/` + id);
    }

    upload(description: string, allowComments: boolean, file: File) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);           
            
        return this.http.post(API_URL + '/photos/upload', formData);
        }
      }    

      updateFoto(description: string, allowComments: boolean, file: File) {
        {
            const formData = new FormData();
            formData.append('description', description);
            formData.append('allowComments', allowComments ? 'true' : 'false');
            formData.append('imageFile', file);           
            
            return this.http.post(API_URL + '/photos/update', formData);
        }
      }    


}