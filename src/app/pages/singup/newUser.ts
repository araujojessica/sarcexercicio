export interface NewUser{
    id: number,
    userName : string,
    fullName : string,
    email : string,
    telefone: string,
    password : string
}