import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SingUpPage } from './singup.page';

const routes: Routes = [
  {
    path: '',
    component: SingUpPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SingUpPageRoutingModule {}
