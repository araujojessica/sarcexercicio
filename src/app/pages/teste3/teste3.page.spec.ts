import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Teste3Page } from './teste3.page';

describe('Teste5Page', () => {
  let component: Teste3Page;
  let fixture: ComponentFixture<Teste3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Teste3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Teste3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
