import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Antropometrica } from './antropometrica';
import { AntropometricaService } from './antropometricaService';

@Component({
  selector: 'app-home',
  templateUrl: 'teste3.page.html',
  styleUrls: ['teste3.page.scss'],
})
export class Teste3Page implements OnInit{

  public antropometricaForm: FormGroup;
  sus:string;
  
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private formBuilder: FormBuilder,
    private antropometricaService: AntropometricaService,
    private toastCtrl: ToastController
  ) {}
  ngOnInit(): void {
    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de antro : ' + parametros['sus'] );
    })
    this.antropometricaForm = this.formBuilder.group({
      'antropometrica_resultado': [null, Validators.compose([Validators.required])]
    });
  }

  teste4(){

    const newAntropometrica  = this.antropometricaForm.getRawValue() as Antropometrica;
    console.log(newAntropometrica.antropometrica_resultado)
    newAntropometrica.paciente_sus = this.sus;
    this.antropometricaService.signup(newAntropometrica).subscribe(
        
      async () => {

        const toast = await (await this.toastCtrl.create({
          message: 'Bioimpedância cadastrado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/teste4/'+this.sus+'']);
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor avise o administrador do app!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
    
  }

}
