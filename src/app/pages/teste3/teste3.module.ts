import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  Teste3Page } from './teste3.page';

import { Teste3PageRoutingModule } from './teste3-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Teste3PageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [Teste3Page]
})
export class Teste3PageModule {}
