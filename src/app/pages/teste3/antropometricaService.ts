import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Antropometrica } from "./antropometrica";

//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class AntropometricaService{

    constructor(private http: HttpClient){}

    signup(newAntropometrica : Antropometrica){
        console.log(newAntropometrica)
        return this.http.post(API_URL +  '/antropometrica/signup', newAntropometrica);
    }

 

}