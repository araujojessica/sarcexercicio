import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/core/user/user";
import { BlocoNotas } from "./bloco_notas";



//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class BlocoNotasService{

    constructor(private http: HttpClient){}
  
    signup(anotacao: string, paciente_sus: string){
        return this.http.post(API_URL +  '/anotacao/signup', {anotacao, paciente_sus});
    }
}

   