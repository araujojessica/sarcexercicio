import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { BlocoNotasPageRoutingModule } from './bloco_notas-routing.module';

import { BlocoNotasPage } from './bloco_notas.page';

describe('BlocoNotasPage', () => {
  let component: BlocoNotasPage;
  let fixture: ComponentFixture<BlocoNotasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlocoNotasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BlocoNotasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
