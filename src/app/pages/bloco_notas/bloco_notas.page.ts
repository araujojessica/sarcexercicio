import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, MenuController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { BlocoNotas } from './bloco_notas';
import { BlocoNotasService } from './bloco_notasService';

@Component({
  selector: 'app-bloco-notas',
  templateUrl: 'bloco_notas.page.html',
  styleUrls: ['bloco_notas.page.scss'],
})
export class BlocoNotasPage implements OnInit {

  user$: Observable<User>;
  sus: string;
  anotacao: string;
  paciente_sus: string;
  formBuilder: any;
  public notasForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    private userService: UserService,
    public toastCtrl: ToastController,
    private plt: Platform,
    private route: ActivatedRoute,
    private anotacaoService: BlocoNotasService
  ) {
    this.user$ = userService.getUser();
  }
  ngOnInit(): void {
    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de info : ' + parametros['sus'] );
    });

    
  }

  anotacoes(){
    
    this.paciente_sus = this.sus;
    console.log(this.anotacao)
    console.log('anotaçoes' + this.sus)

    this.anotacaoService.signup(this.anotacao, this.sus).subscribe(
      async () => {

        console.log(this.sus)
        const toast = await (await this.toastCtrl.create({
          message: 'Paciente cadastrado com sucesso '+this.sus+'!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        var sus = this.sus;
        this.router.navigate(['/contato/'+sus+''])
      },  
      async err => {
        this.router.navigate(['/home'])
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor contate o adm do app',
          duration: 8000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
  }
}