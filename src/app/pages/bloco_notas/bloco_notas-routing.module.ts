import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlocoNotasPage } from './bloco_notas.page';

const routes: Routes = [
  {
    path: '',
    component: BlocoNotasPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlocoNotasPageRoutingModule {}
