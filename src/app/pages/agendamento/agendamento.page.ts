import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, MenuController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { Agendamento } from './agendamento';
import { AgendamentoService } from './agendamentoService';

@Component({
  selector: 'app-Agendamento',
  templateUrl: 'agendamento.page.html',
  styleUrls: ['agendamento.page.scss'],
})
export class AgendamentoPage implements OnInit {

  user$: Observable<User>;
  sus: string;
  data_agendamento: string;
  paciente_sus: string;
  profissional: string;
  data:string;
  hora:string;
  nome:string;
 
  public notasForm: FormGroup;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    private userService: UserService,
    public toastCtrl: ToastController,
    private plt: Platform,
    private route: ActivatedRoute,
    private agendamentoService: AgendamentoService
  ) {
    this.user$ = userService.getUser();
    this.nome = window.localStorage.getItem('nome');

  }

  ngOnInit(): void {
    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de info : ' + parametros['sus'] );
    });

    
    
  }

 ionViewWillEnter() {
    this.menuCtrl.enable(true);
    //this.menuCtrl.close();
  }

  agendamento(){
    
    this.paciente_sus = this.sus;
    var a = this.data.split('T');
    var b = this.hora.split('T');
    var c = b[1].split('.');

    this.data_agendamento = a[0] + 'T' + c[0];

    this.agendamentoService.signup(this.data_agendamento, this.sus, this.profissional).subscribe(
      async () => {

        console.log(this.sus)
        const toast = await (await this.toastCtrl.create({
          message: 'Agendamento cadastrado com sucesso '+this.sus+'!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        var sus = this.sus;
        this.router.navigate(['/contato/'+sus+''])
      },  
      async err => {
        this.router.navigate(['/home'])
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor contate o adm do app',
          duration: 8000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
  }
}