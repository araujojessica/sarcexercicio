export interface Agendamento {

    id: string;
    data: string;
    paciente_sus:string;
    profissional:string;
}