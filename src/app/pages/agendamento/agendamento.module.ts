import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { AgendamentoPageRoutingModule } from './agendamento-routing.module';
import { AgendamentoPage } from './agendamento.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgendamentoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AgendamentoPage]
})
export class AgendamentoPageModule {}
