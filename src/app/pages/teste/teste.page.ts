import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { Teste } from './teste';
import { TesteService } from './testeService';

@Component({
  selector: 'app-home',
  templateUrl: 'teste.page.html',
  styleUrls: ['teste.page.scss'],
})
export class TestePage implements OnInit{
  
  user$: Observable<User>;
  sus:string;
  public testeForm: FormGroup;
  
  constructor(
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private testeService: TesteService,
    private toastCtrl: ToastController,
    private route: ActivatedRoute
  ) {

    this.user$ = userService.getUser();
  }
  ngOnInit(): void {

    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de info : ' + parametros['sus'] );
    })

    this.testeForm = this.formBuilder.group({
      'doencas': [null, Validators.compose([Validators.required])],
      'medicamentos': [null, Validators.compose([Validators.required])],
      'tabagismo': [null, Validators.compose([Validators.required])],
      'atividade': [null, Validators.compose([Validators.required])]
    });
   
  }

  teste2(){

    const newTeste  = this.testeForm.getRawValue() as Teste;
     newTeste.paciente_id = this.sus;
    this.testeService.signup(newTeste).subscribe(
        
      async () => {
        const toast = await (await this.toastCtrl.create({
          message: 'Teste cadastrado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/teste2/'+this.sus+''])
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor avise o administrador do app!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
    
  }

}
