import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Teste } from "./teste";

//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class TesteService{

    constructor(private http: HttpClient){}


    signup(newTeste : Teste){
        console.log(newTeste)
        return this.http.post(API_URL +  '/questionario/signup', newTeste);
    }

 

}