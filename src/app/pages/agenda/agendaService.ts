import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "src/app/core/user/user";
import { Agenda } from "./agenda";
import { PacienteAgenda } from "../agendamento_cliente/pacienteAgenda";
import { getMaxListeners } from "process";
import { Foto } from "./foto";




//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class AgendaService{

    constructor(private http: HttpClient){}
  
    findAgendamento(){

        return this.http.get<Agenda[]>(API_URL +  '/agendamento/hoje');
    }

    findPaciente(pacienteSus: string) {
        return this.http.get<PacienteAgenda[]>(API_URL + '/paciente/'+pacienteSus+'');
     } 
    
     findFotoPaciente(email: string) {
     
        return this.http.get<Foto>(API_URL + '/photos/'+email+'');
     } 

     
}

   