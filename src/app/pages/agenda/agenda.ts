export interface Agenda {
    id:string;
    data_agendamento:string;
    profissional:string;
    paciente_sus:string;
    hora: string;
    semana: string;
}