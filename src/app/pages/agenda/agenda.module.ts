import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AgendaPage } from './agenda.page';

import { AgendaPageRoutingModule } from './agenda-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgendaPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [AgendaPage]
})
export class AgendaPageModule {}
