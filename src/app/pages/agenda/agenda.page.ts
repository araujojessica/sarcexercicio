import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, MenuController, ModalController, NavController, Platform, PopoverController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { PacienteAgenda } from '../agendamento_cliente/pacienteAgenda';
import { Paciente } from '../paciente/paciente';
import { Agenda } from './agenda';
import { AgendaHoje } from './agendaHoje';
import { AgendaService } from './agendaService';
import { Foto } from './foto';

@Component({
  selector: 'app-home',
  templateUrl: 'agenda.page.html',
  styleUrls: ['agenda.page.scss'],
})
export class AgendaPage implements OnInit{
  user$: Observable<User>;
  profissional: User;
  agenda: Agenda[] = [];
  nomePaciente: string;
  semana:string;
  horario:string;
  uploadedFiles: Array < File > ;
  photoForm: FormGroup;
  file: File;
  foto: Foto;
  urlFoto: string;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public popoverCtrl: PopoverController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private router: Router,
    private userService: UserService,
    public toastCtrl: ToastController,
    private plt: Platform,
    private agendaService: AgendaService,
    private formBuilder: FormBuilder
  ) {
    this.user$ = userService.getUser();
    this.usuario();
    
    
  }

usuario(){
  this.user$.subscribe (profissional => {
    this.profissional = profissional;
    var email = '' + this.profissional.email;
    this.agendaService.findFotoPaciente(email).subscribe(foto => {
        this.foto = foto;
        this.urlFoto = this.foto.url;
    
    })
  })
}
  ngOnInit(): void {
    this.agendaService.findAgendamento().subscribe(agenda => {

      this.agenda = agenda

      for (var v of this.agenda) {
            var d = new Date(v.data_agendamento);
            var weekday = new Array(7);
            weekday[0] = "Domingo";
            weekday[1] = "Segunda-Feira";
            weekday[2] = "Terça-Feira";
            weekday[3] = "Quarta-Feira";
            weekday[4] = "Quinta-Feira";
            weekday[5] = "Sexta-Feira";
            weekday[6] = "Sábado";
      
          this.semana  = weekday[d.getDay()];
          v.semana = this.semana;        
          var h = v.data_agendamento.split('T');
          var hora = h[1].split(':')
          v.hora = hora[0] + ":" + hora[1];
      
          
    
      }

      
    });
    
  
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(true);
    this.menuCtrl.close();
    this.usuario();
  }

  consulta(sus: string){
    console.log('consulta : ' + sus)
    this.router.navigate(['/consulta/'+sus+'']);
  }

  
  



}