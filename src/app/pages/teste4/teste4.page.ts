import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Timeup } from './timeup';
import { TimeupService } from './timeupService';

@Component({
  selector: 'app-home',
  templateUrl: 'teste4.page.html',
  styleUrls: ['teste4.page.scss'],
})
export class Teste4Page implements OnInit{

  public timeupForm: FormGroup;
  sus:string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private timeupService: TimeupService,
    private formBuilder: FormBuilder
    
  ) {}
  ngOnInit(): void {
    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de timeup : ' + parametros['sus'] );
    })
    this.timeupForm = this.formBuilder.group({
      'timeup_resultado': [null, Validators.compose([Validators.required])]
    });
  }

  teste5(){
    const newtimeup  = this.timeupForm.getRawValue() as Timeup;
    console.log(newtimeup)
    newtimeup.paciente_sus = this.sus;
    this.timeupService.signup(newtimeup).subscribe(
        
      async () => {

        const toast = await (await this.toastCtrl.create({
          message: 'Timeup cadastrado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/teste5/'+this.sus+'']);
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor avise o administrador do app!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
  }

}
