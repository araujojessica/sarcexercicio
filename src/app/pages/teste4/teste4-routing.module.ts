import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Teste4Page } from './teste4.page';

const routes: Routes = [
  {
    path: '',
    component: Teste4Page,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Teste4PageRoutingModule {}
