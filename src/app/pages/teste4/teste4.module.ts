import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  Teste4Page } from './teste4.page';

import { Teste4PageRoutingModule } from './teste4-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Teste4PageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [Teste4Page]
})
export class Teste4PageModule {}
