import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { Timeup } from "./timeup";

//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class TimeupService{

    constructor(private http: HttpClient){}

    signup(newTimeup : Timeup){
        console.log(newTimeup)
        return this.http.post(API_URL +  '/timeup/signup', newTimeup);
    }

 

}