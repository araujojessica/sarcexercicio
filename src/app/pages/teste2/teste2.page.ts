import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { Bioimpedancia } from './bioimpedancia';
import { BioimpedanciService } from './bioimpedanciaService';

@Component({
  selector: 'app-home',
  templateUrl: 'teste2.page.html',
  styleUrls: ['teste2.page.scss'],
})
export class Teste2Page implements OnInit{

  sus: string;
  public bioimpedanciaForm: FormGroup;
  constructor(
    private router: Router,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private bioimpedanciaService: BioimpedanciService,
    private toastCtrl: ToastController,
    private route:ActivatedRoute
  ) {}

  ngOnInit(): void {

    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de bioo : ' + parametros['sus'] );
    })
    this.bioimpedanciaForm = this.formBuilder.group({
      'paciente_id': [null, Validators.compose([Validators.required])],
      'gordura': [null, Validators.compose([Validators.required])],
      'massa_magra': [null, Validators.compose([Validators.required])]
    });
  }

  teste3(){
    const newBio  = this.bioimpedanciaForm.getRawValue() as Bioimpedancia;
    console.log(newBio)
    newBio.paciente_id = this.sus;
    this.bioimpedanciaService.signup(newBio).subscribe(
        
      async () => {

        const toast = await (await this.toastCtrl.create({
          message: 'Bioimpedância cadastrado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/teste3/'+this.sus+''])
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor avise o administrador do app!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
  }

}
