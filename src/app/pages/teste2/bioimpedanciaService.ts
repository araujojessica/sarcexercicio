import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Bioimpedancia } from "./bioimpedancia";

//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class BioimpedanciService{

    constructor(private http: HttpClient){}

    signup(newBioimpedancia : Bioimpedancia){
        console.log(newBioimpedancia)
        return this.http.post(API_URL +  '/bioimpedancia/signup', newBioimpedancia);
    }

 

}