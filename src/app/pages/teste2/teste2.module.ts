import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  Teste2Page } from './teste2.page';

import { Teste2PageRoutingModule } from './teste2-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Teste2PageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [Teste2Page]
})
export class Teste2PageModule {}
