import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Teste5Page } from './teste5.page';

const routes: Routes = [
  {
    path: '',
    component: Teste5Page,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Teste5PageRoutingModule {}
