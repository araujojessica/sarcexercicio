import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Caminhada } from './caminhada';
import { CaminhadaService } from './caminhadaService';

@Component({
  selector: 'app-home',
  templateUrl: 'teste5.page.html',
  styleUrls: ['teste5.page.scss'],
})
export class Teste5Page implements OnInit{

  public caminhadaForm: FormGroup;
  sus:string; 
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private toastCtrl: ToastController,
    private caminhadaService: CaminhadaService,
    private formBuilder: FormBuilder
  ) {}
  ngOnInit(): void {

    this.route.params.subscribe(parametros=> {
      this.sus =  parametros['sus'];
      console.log('parametro dentro de timeup : ' + parametros['sus'] );
    })
    this.caminhadaForm = this.formBuilder.group({
      'caminhada_resultado': [null, Validators.compose([Validators.required])]
    });
  }

  agenda(){

    const newCaminhada  = this.caminhadaForm.getRawValue() as Caminhada;
    console.log(newCaminhada)
    newCaminhada.paciente_sus = this.sus;
    this.caminhadaService.signup(newCaminhada).subscribe(
        
      async () => {

        const toast = await (await this.toastCtrl.create({
          message: 'Caminhada cadastrado com sucesso!',
          duration: 4000, position: 'top',
          color: 'success'
        })).present();
        this.router.navigate(['/contato/'+this.sus+'']);
      },  
      async err => {
        console.log(err)
        const toast = await (await this.toastCtrl.create({
          message: 'Houve um erro, por favor avise o administrador do app!',
          duration: 4000, position: 'top',
          color: 'danger'
        })).present();
      }
    );
   
  }

}
