import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Teste5Page } from './teste5.page';

describe('Teste5Page', () => {
  let component: Teste5Page;
  let fixture: ComponentFixture<Teste5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Teste5Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Teste5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
