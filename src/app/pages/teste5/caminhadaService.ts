import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Caminhada } from "./caminhada";


//const API_URL = "http://localhost:3000"; 
const API_URL = 'http://159.203.181.9:3000';

@Injectable({providedIn: 'root'})
export class CaminhadaService{

    constructor(private http: HttpClient){}

    signup(newCaminhada : Caminhada){
        console.log(newCaminhada)
        return this.http.post(API_URL +  '/caminhada/signup', newCaminhada);
    }

 

}