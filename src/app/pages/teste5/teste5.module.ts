import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {  Teste5Page } from './teste5.page';

import { Teste5PageRoutingModule } from './teste5-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Teste5PageRoutingModule,ReactiveFormsModule
  ],
  declarations: [Teste5Page]
})
export class Teste5PageModule {}
