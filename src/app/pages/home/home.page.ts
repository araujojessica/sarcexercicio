import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertController, LoadingController, MenuController, NavController, ToastController } from '@ionic/angular';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/core/auth/auth.service';
import { PlatformDectorService } from 'src/app/core/plataform-dector/plataform-dector.service';
import { User } from 'src/app/core/user/user';
import { UserService } from 'src/app/core/user/user.service';
import { RecuperarSenhaService } from './recuperar_senha';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  public onLoginForm: FormGroup;
  users = [];
  nome: string; 
  senha: string;
 
  tipo: boolean;
  @ViewChild('userNameInput') userNameInput: ElementRef<HTMLInputElement>;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
	  private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private recuperarSenha: RecuperarSenhaService,
    private platformDetectorService: PlatformDectorService
  ) {

  }

  ngOnInit(): void {
    this.onLoginForm = this.formBuilder.group({
      'userName': [null, Validators.compose([Validators.required])],
      'password': [null, Validators.compose([Validators.required])]
    });
   
    this.platformDetectorService.isPlatformBrowser();

  
  }

  async forgotPassword(){
    const alert = await this.alertCtrl.create({
      header: 'Esqueceu sua senha?',
      message: 'Informe seu e-mail para enviarmos um novo link de recuperação.',
      inputs: [
        {
          name: 'email',
          type: 'email',
          placeholder: 'E-mail'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: () => {
            console.log('Confirmar Cancelamento?');
          }
        }, {
          text: 'Confirmar',
          handler: async data => {
            console.log(data.email)
            this.recuperarSenha.sendemail(data.email)
            .subscribe(
                async () =>{ 
                  this.navCtrl.navigateRoot('')
                  const toast = await this.toastCtrl.create({
                            
                    message:'Email enviado com sucesso!', 
                    duration:4000, position:'top',
                    color:'success'
                    });
            
                  
                  toast.present();
                },
                async err => {
             
                   const toast = await this.toastCtrl.create({
                            
                            message:'Usuário não cadastrado, por favor cadastre-se no APP!', 
                            duration:4000, position:'top',
                            color:'danger'
                    });
            
                   
                   toast.present();
    
                }
                
            );
            
          }
        }
      ]
    });

    await alert.present();
  }

  singup(){
    this.router.navigate(['/singup']);
  }

  agenda(){
    const userName = this.onLoginForm.get('userName').value;
    const password = this.onLoginForm.get('password').value;
    
   this.authService
        .authenticate(userName, password)
        .subscribe(
          
            () => this.navCtrl.navigateRoot('/agenda')
            ,
            async err => {
              this.onLoginForm.reset();
              this.platformDetectorService.isPlatformBrowser();
              const toast = await this.toastCtrl.create({
                  
                message:'Usuário ou senha incorreto, por favor cadastra-se, ou tente novamente!', 
                duration:4000, position:'top',
                color:'danger'
            });
              this.router.navigate(['/singup']);
       
              toast.present();
            }
        ); 
    //
  }

  exibirOcultar(){
    this.tipo = !this.tipo;
  }

}
