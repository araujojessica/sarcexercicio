import { MenuController} from '@ionic/angular';
import { Component, ViewChildren, QueryList  } from '@angular/core';
import { Platform, NavController, ActionSheetController,IonRouterOutlet } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { Pages } from './interfaces/pages';

import { Router } from '@angular/router';
import { UserService } from 'src/app/core/user/user.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/core/user/user';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})


export class AppComponent {

  lastTimeBackPress = 0;
  timePeriodToExit = 2000;
  user$: Observable<User>;
  @ViewChildren(IonRouterOutlet) routerOutlets: QueryList<IonRouterOutlet>;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public menuCtrl: MenuController,
    public router: Router,
    private userService: UserService,
    private actionSheetCtrl: ActionSheetController,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  agenda(){
    this.router.navigate(['/agenda']);
    this.menuCtrl.toggle();
  }

  paciente(){
    this.router.navigate(['/paciente']);
    this.menuCtrl.toggle();
  }

  agendamento(){
    this.router.navigate(['/agendamento_cliente']);
    this.menuCtrl.toggle();
  }

  referencia(){
    this.router.navigate(['/referencia']);
    this.menuCtrl.toggle();
  }

  editPerfil(){
    this.router.navigate(['/editar-perfil']);
    this.menuCtrl.toggle();
  }

  logout(){
    console.log("entrou em sair")
    this.userService.logout();
    localStorage.removeItem('currentUser');
    this.menuCtrl.close();
    this.router.navigate(['home']);
    navigator['app'].exitApp();
    
  }

  backButtonEvent() {
		
    this.platform.backButton.subscribe(async () => {
        // close action sheet
        try {
            const element = await this.actionSheetCtrl.getTop();
            if (element) {
                element.dismiss();
                return;
            }
        } catch (error) {
        }

        

        this.routerOutlets.forEach((outlet: IonRouterOutlet) => {
            if (outlet && outlet.canGoBack()) {
                outlet.pop();

            } else if (this.router.url === '/home-results') {
                if (new Date().getTime() - this.lastTimeBackPress < this.timePeriodToExit) {
                  this.userService.logout();
                  this.router.navigate(['']);
                    // this.platform.exitApp(); // Exit from app
                    navigator['app'].exitApp(); // work in ionic 4

                } else {
                    
                    this.lastTimeBackPress = new Date().getTime();
                }
            }
        });
    });
}
}